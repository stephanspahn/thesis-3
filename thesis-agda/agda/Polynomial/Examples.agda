module Polynomial.Examples where


open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam
open import Prelude.Container

open import Data.List hiding (sum) renaming (map to List-map)
open import Data.Fin hiding (_+_; lift)

open import Polynomial.IR
open import Polynomial.Initial
open import Polynomial.Composition

-- W-types

module _ (S : Set)(P : S -> Set) where

  cW₀ : {D : Set1} -> Poly D
  cW₀ = sigma (con S) (λ { (lift s) → pi (P s) (λ _ → idPN) })

  -- ordinary W-type: choose D = ⊤
  cW⊤ : IR (Lift ⊤) (Lift ⊤)
  cW⊤ = (cW₀ , λ _ → _)

  W : Set
  W = U {c = cW⊤}

  sup : (s : S) -> (P s -> W) -> W
  sup s f = intro (s , f)

  -- "upgraded" W-type, where decoding component applies T everywhere

  cW : IR Set Set
  cW = cW₀ , (λ { (lift s , Tf) → (x : P s) -> Tf x })

-- A universe closed under sigma- and pi-types and W-types

cUΣΠW : IR Set Set
cUΣΠW = sigma (con Tags)
              (λ { (lift bool) → con ⊤
                 ; (lift sig)  → sigma idPN (λ X → pi X (λ _ → idPN))
                 ; (lift pi)   → sigma idPN (λ X → pi X (λ _ → idPN))
                 ; (lift w)    → sigma idPN (λ X → pi X (λ _ → idPN))
                 }) ,
        (λ { (lift bool , _) → Bool
           ; (lift sig , A , B) → Σ A B
           ; (lift pi , A , B) → (a : A) -> B a
           ; (lift w , A , B) → W A B })
  module TagU where data Tags : Set where
                      bool sig pi w : Tags

UΣΠW : Set
UΣΠW = U {c = cUΣΠW}

TΣΠW : UΣΠW -> Set
TΣΠW x = T x

bool : UΣΠW
bool = intro (TagU.bool , tt)

sig : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
sig a b = intro (TagU.sig , a , b)

pii : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
pii a b = intro (TagU.pi , a , b)

w : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
w a b = intro (TagU.w , a , b)


-- A language of sums and products

sumFin : (n : ℕ) -> (Fin n -> ℕ) -> ℕ
sumFin zero f = 0
sumFin (suc n) f = f (fromℕ n) + sumFin n (f ∘ inject₁)

prodFin : (n : ℕ) -> (Fin n -> ℕ) -> ℕ
prodFin zero f = 1
prodFin (suc n) f = f (fromℕ n) * prodFin n (f ∘ inject₁)

cArith : IR (Lift ℕ) (Lift ℕ)
cArith =
   sigma (con Tags)
         (λ { (lift fin) → con ℕ
            ; (lift sum) → sigma idPN (λ n → pi (Fin (lower n)) (λ _ → idPN))
            ; (lift prod) → sigma idPN (λ n → pi (Fin (lower n)) (λ _ → idPN))
            }) ,
   (λ { (lift fin , n) → n
      ; (lift sum , lift n , f) → lift (sumFin n (lower ∘ f))
      ; (lift prod , lift n , f) → lift (prodFin n (lower ∘ f)) })
  module TagArith where data Tags : Set where
                          fin sum prod : Tags

fac : ℕ -> U {c = cArith}
fac n = intro (TagArith.prod ,
              intro (TagArith.fin , n) ,
              (λ x → intro (TagArith.fin , suc (toℕ x))))

_ : T (fac 5) ≡ lift 120
_ = refl

-- List of arguments for free: composing with W ℕ Fin

cUList : IR Set Set
cUList = cUΣΠW ○ (cW ℕ Fin)

-- concretely, the composed code is as follows:

_ : proj₁ cUList ≡ sigma (con TagU.Tags)
                         (λ { (lift TagU.bool) → con ⊤
                            ; (lift TagU.sig) →
                               sigma (cW₀ ℕ Fin)
                                     (λ { (lift s , Tf) →
                                             pi ((x : Fin s) → Tf x)
                                                (λ _ → (cW₀ ℕ Fin))})
                            ; (lift TagU.pi) →
                               sigma (cW₀ ℕ Fin)
                                     (λ { (lift s , Tf) →
                                             pi ((x : Fin s) → Tf x)
                                                (λ _ → (cW₀ ℕ Fin))})
                            ; (lift TagU.w) →
                               sigma (cW₀ ℕ Fin)
                                     (λ { (lift s , Tf) →
                                             pi ((x : Fin s) → Tf x)
                                                (λ _ → (cW₀ ℕ Fin))})
                            })
_ = cong (sigma (con TagU.Tags)) (ext (λ { (lift TagU.bool) → refl
                                         ; (lift TagU.sig) → refl
                                         ; (lift TagU.pi) → refl
                                         ; (lift TagU.w) → refl
                                         }))

-- for convenience, convert from real lists to container lists

fromL : {A : Set}(T : A -> Set) -> List A -> ⟦ cW ℕ Fin ⟧₀ (A , T)
fromL T [] = 0 , (λ ())
fromL T (x ∷ xs) = let (n , f) = fromL T xs in
                     (suc n , (λ { zero → x ; (suc m) → f m }))

fromL₁ : {A : Set}{T : A -> Set} -> (xs : List A) ->
         ⟦ cW ℕ Fin ⟧₁ (A , T) (fromL T xs) -> foldr (λ u Y → T u × Y) ⊤ xs
fromL₁ [] y = tt
fromL₁ (x ∷ xs) y = y zero , fromL₁ xs (y ∘ suc)

-- The upgraded universe

U' : Set
U' = U {c = cUList}

T' : U' -> Set
T' x = T x

bool' : U'
bool' = intro (TagU.bool , tt)

sig' : (a : List U') -> (b : foldr (λ u Y → T u × Y) ⊤ a  -> List U') -> U'
sig' a b = intro (TagU.sig , fromL T' a , (λ x → fromL T' (b (fromL₁ a x))))

pii' : (a : List U') -> (b : foldr (λ u Y → T u × Y) ⊤ a  -> List U') -> U'
pii' a b = intro (TagU.pi , fromL T' a , (λ x → fromL T' (b (fromL₁ a x))))

w' : (a : List U') -> (b : foldr (λ u Y → T u × Y) ⊤ a  -> List U') -> U'
w' a b = intro (TagU.w , fromL T' a , (λ x → fromL T' (b (fromL₁ a x))))


-- can of course recover the original behaviour (in this case)

sig'' : (a : U') -> (b : T' a -> U') -> U'
sig'' a b = sig' [ a ] (λ { (x , _) → [ b x ] })

pii'' : (a : U') -> (b : T' a -> U') -> U'
pii'' a b = pii' [ a ] (λ { (x , _) → [ b x ] })

w'' : (a : U') -> (b : T' a -> U') -> U'
w'' a b = w' [ a ] (λ { (x , _) → [ b x ] })
