module Polynomial.Intermediary-Systems.Levitation where

open import Prelude.Basic
open import Prelude.Container
open import Prelude.Fam
open import Prelude.Equality
open import Prelude.Equivalences
--open import DS.IR
open import DS.SigmaDelta
open import DS.IR
open import DS.Arg
open import Polynomial.Container.IR
import Relation.Binary.PreorderReasoning as Pre


{- We show that a 'levitation system' where codes are annotated by their
shape (given by a binary tree) can be reduced to such a system with
lower degree. This is a generalization of Dybjer-Setzer's reduction of
the two-level system (SP , Arg) to the one level system DS.

A 'leviation system' is Polynomial IR without pi. Another name would
be 'system with a code for itself'.
-}




{------------------- Irish codes without pi ------------------------}

mutual 
   data LevC (D : Set₁) : Set₁ where 
        bs : Cont {lzero}{lzero} -> LevC D
        it  : (R : LevC D) ->  (LevP R -> LevC D) -> LevC D
 
   LevP : {D : Set₁} -> LevC D -> Set₁
   LevP {D} (bs R)  = (cont R) D
   LevP     (it R f) = Σ (LevP R) (λ x → LevP (f x))

ContLev : {D : Set₁} -> Cont
ContLev {D} = (LevC D , LevP)

Lev : ∀ {a} → Set₁ -> Set a -> Set (a ⊔ lsuc (lzero))
Lev D E = (cont (ContLev {D})) E

-- monadicity

-con : {D : Set₁} → Set → LevC D
-con A = bs (A , λ a → ⊥) 

ηLev : ∀ {a} → {D : Set₁} → {E : Set a} -> E -> Lev D E 
ηLev e = (-con ⊤ , const e)

μLev : ∀ {a} → {D : Set₁} -> {E : Set a} → Lev D (Lev D E) → Lev D E
μLev {a} {D} {E} (R , f) = (Q , α)
                          where Q : LevC D  
                                Q = it R (proj₁ ∘ f) 
                                α : LevP Q → E  
                                α (x , q) = proj₂ (f x) q

-- decoding

mutual
  -ix : {D : Set₁} -> (X : LevC D) -> Fam D -> Set
  -ix (bs (S , P)) (U , T) = (S ◃ P) U
  -ix (it R f) P = Σ[ x ∈ -ix R P ] (-ix (f (-dx R P x)) P)

  -dx : {D : Set₁} -> (X : LevC D) -> (P : Fam D) -> -ix X P -> LevP X
  -dx (bs (S , P)) (U , T) = ◃-map S P T
  -dx (it R f) P (x , p) = -dx R P x , -dx (f (-dx R P x)) P p

-⟦_⟧ : {D E : Set₁} -> Lev D E -> Fam D -> Fam E
-⟦ X , f ⟧ P = Fam-map f (-ix X P , -dx X P)

-⟦_⟧₀ : {D E : Set₁} -> Lev D E -> Fam D -> Set
-⟦ c ⟧₀ X = ind (-⟦ c ⟧ X)

-⟦_⟧₁ : {D E : Set₁} -> ( c : Lev D E) -> (X : Fam D) -> -⟦ c ⟧₀ X → E
-⟦ c ⟧₁ X = fib (-⟦ c ⟧ X)



{------------------------ Annotate Lev by (pairs of) binary trees ---------------------------------}

data bt : Set where
  leaf : bt
  branch : bt → bt → bt

mutual
  data LevtC (D : Set₁): bt → Set₁ where
    bt-bs :  Cont {lzero}{lzero} -> LevtC D leaf
    bt-it : (xx yy : bt) → (R : LevtC D xx) ->  (LevtP R -> LevtC D yy) -> LevtC D (branch xx yy)

  LevtP : {D : Set₁} {xx : bt} → LevtC D xx → Set₁
  LevtP {D = D} (bt-bs R) = (cont R) D
  LevtP (bt-it xx yy Q h) =  Σ (LevtP Q) (λ x → LevtP (h x))

ContLevt : {D : Set₁} → (xx : bt) -> Cont
ContLevt {D} xx = (LevtC D xx , LevtP)

Levt : ∀ {a} → Set₁ -> Set a → (xx : bt) -> Set (a ⊔ lsuc (lzero))
Levt D E xx = (cont (ContLevt {D} xx)) E

Levt→ : ∀ {a} → (D : Set₁) -> {xx : bt} → {E E' : Set a} → (φ : E → E') -> Levt D E xx → Levt D E' xx
Levt→ D {xx = xx} φ = (cont-map (ContLevt {D} xx)) φ

{-
μLevt : {D : Set₁} -> {E : Set₁} → {xx  yy : bt} → Levt D (Levt D E yy) xx → Levt D E (branch xx yy)
μLevt {D} {E} {xx = xx} {yy = yy}(R , f) = (Q , α)
                          where Q : LevtC D  (branch xx yy)
                                Q = bt-it xx yy R (proj₁ ∘ f) 
                                α : LevtP Q → E  
                                α (x , q) = proj₂ (f x) q

-}

μLevt : ∀ {a} → {D : Set₁} -> {E : Set a} → {xx  yy : bt} → Levt D (Levt D E yy) xx → Levt D E (branch xx yy)
μLevt {a} {D} {E} {xx = xx} {yy = yy}(R , f) = (Q , α)
                          where Q : LevtC D  (branch xx yy)
                                Q = bt-it xx yy R (proj₁ ∘ f) 
                                α : LevtP Q → E  
                                α (x , q) = proj₂ (f x) q
                                

-- Reduction

Δ : {D E : Set1} → {xx yy : bt} → {Q : LevtC D xx } -> (h : LevtP Q -> LevtC D yy) -> (x : LevtP Q) -> (f : (LevtP (bt-it xx yy Q h)) -> E) -> Levt D E yy
Δ h x f = (h x , λ q -> f (x , q))

Cod𝒹 :  {D E : Set1} → bt → Set₁
Cod𝒹 {D = D} {E = E} (leaf) = Levt D E leaf
Cod𝒹 {D = D} {E = E} (branch xx yy ) = Levt D (Levt D E yy ) xx

𝒹 : {D E : Set1} → {ww : bt} → Levt D E ww → Cod𝒹 {D = D} {E = E} ww
𝒹 (bt-bs R , f) = (bt-bs R , f)
𝒹 (bt-it xx yy Q h , f) =  ( Q ,  λ x → Δ {Q = Q} h x f)

-- lemma about 𝒹

lemma-𝒹 : {D E : Set1}{xx yy : bt} → (R : LevtC D xx) → (h : LevtP R → LevtC D yy) → (f : LevtP (bt-it xx yy R h) → E) → μLevt (𝒹 (bt-it xx yy R h , f)) ≡ (bt-it xx yy R h , f)
lemma-𝒹 R h f = refl

-- The codomain of 𝒹𝒹 (the iteration of 𝒹)

Cod𝒹𝒹 :  (D E : Set1) → bt → Set₁
Cod𝒹𝒹 D E leaf = Levt D E leaf
Cod𝒹𝒹 D E (branch xx yy) = Cod𝒹𝒹 D (Cod𝒹 {D = D} {E = E} yy) xx

-- Functoriality of the Codomains

Cod𝒹→ : (D : Set₁) → {E E' : Set₁} → {xx : bt} → (φ : E → E') → Cod𝒹 {D = D}{E = E} xx → Cod𝒹 {D = D}{E = E'} xx
Cod𝒹→ D {xx = leaf} φ = Levt→ D φ  
Cod𝒹→ D { xx = (branch xx yy)} φ = Levt→ D (Levt→ D φ)

Cod𝒹𝒹→ : (D : Set₁) → {E E' : Set₁} → {xx : bt} → (φ : E → E') → Cod𝒹𝒹 D E xx → Cod𝒹𝒹 D E' xx
Cod𝒹𝒹→ D {xx = leaf} φ  = Levt→ D φ 
Cod𝒹𝒹→ D {E = E}{E' = E'} {xx = (branch xx yy)} φ = Cod𝒹𝒹→ D {E = Cod𝒹 {D = D}{E = E} yy }{E' = Cod𝒹 {D = D}{E = E'} yy } {xx = xx} (Cod𝒹→ D {E = E }{E' = E' } {xx = yy}  φ)


{-
 Levt D (Levt D E yy) xx → Levt D E (branch xx yy) -- E' = (Levt D E aa)
 Levt D (Levt D (Levt D E aa) yy) xx → Levt D (Levt D E aa) (branch xx yy) = Levt D E (branch (branch xx yy) aa)
-}

lSpl : bt → bt
lSpl leaf = leaf
lSpl (branch xx yy) = branch (lSpl xx) yy

μLevt* : {D : Set₁}{E : Set₁}{xx : bt} → Cod𝒹𝒹 D E xx → Levt D E xx
μLevt* {D} {E} {xx = leaf } c = c
μLevt* {D} {E} {xx = branch xx leaf} c = μLevt {lsuc lzero} {D}{E}{xx}{yy = leaf} (μLevt* {D}{E = Levt D E leaf} {xx} c)
μLevt* {D} {E} {xx = (branch xx (branch aa bb))} = (μLevt {lsuc lzero} {D}{E}{xx}{yy = branch aa bb}) ∘  (μLevt* {D} {E = (Levt D E (branch aa bb))} {xx = xx}) ∘ (Cod𝒹𝒹→ D {E = Levt D (Levt D E bb) aa }{E' = Levt D E (branch aa bb)} {xx = xx}(μLevt {lsuc lzero} {D} {E} {xx = aa} {yy = bb}) )


-- 𝒹𝒹 is the first composite of the translation, the socond being REDU below

𝒹𝒹 : {D E : Set1} → {ww : bt} → Levt D E ww → Cod𝒹𝒹 D E ww
𝒹𝒹 (bt-bs R , f) = (bt-bs R , f)
𝒹𝒹(bt-it xx yy Q h , f) = 𝒹𝒹 (proj₁ (𝒹 (bt-it xx yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it xx yy Q h , f)))x)))

-- lemma about 𝒹𝒹

lemma-𝒹𝒹 : {D E : Set1}{xx : bt} → (c : Levt D E xx) → μLevt* (𝒹𝒹 c) ≡ c
lemma-𝒹𝒹 {xx = leaf} (bt-bs h , f) = refl
lemma-𝒹𝒹 {xx = branch xx yy} (bt-it .xx .yy Q h , f) = begin
                                                        μLevt* (𝒹𝒹 (proj₁ (𝒹 (bt-it xx yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it xx yy Q h , f)))x))))
                                                        ≡⟨ {!!} ⟩
                                                        (bt-it xx yy Q h , f)
                                                        ∎ where open ≡-Reasoning
                               

{-
lemma-𝒹𝒹 : {D E : Set1}{xx yy : bt} → (R : LevtC D xx) → (h : LevtP R → LevtC D yy) → (f : LevtP (bt-it xx yy R h) → E) → μLevt* (𝒹𝒹 (bt-it xx yy R h , f)) ≡ (bt-it xx yy R h , f)
lemma-𝒹𝒹 R h f = refl
-}

-- in leaf Levt and DS't coincide

♯pos : {X : Cont} {D : Set₁} → Arg {D = D}(ι' X) → LevtP (bt-bs X)
♯pos x = x

♭leaf : {D E : Set₁} → Levt D E leaf → DS' D E
♭leaf (bt-bs X , f) = (ι' X , λ x → f (♯pos x) )

--  DS' → Lev

{-
{-# TERMINATING #-}
♯SPLevC : {D E : Set₁} → SP D → LevC D
♯SPLevC (ι' X) = bs X
♯SPLevC{D}{E} (σδ' (Q , h)) = (it (bs Q) (♯SPLevC{D}{E} ∘ h))

♭LevP' : {D E : Set₁}{Q : SP D} → LevP (♯SPLevC{D}{E} Q) → Arg (Q)
♭LevP' {Q = (ι' X)} x = x
♭LevP' {D}{E}{Q = (σδ' (Q , h))} (x , y) = (x , (♭LevP'{D}{E} (h x)) y) 

♯' : {D E : Set₁} → DS' D E →  Lev D E 
♯' (ι' X , f) = (bs X , f)
♯' {D}{E}(σδ' (Q , h) , f) = (♯SPLevC{D}{E} (σδ' (Q , h))  , f ∘ ♭LevP'{D}{E})
-}

{-
♯'bt : {D E : Set₁}{xx : bt} → DS' D E →  Levt D E (branch leaf xx)
♯'bt (ι' X , f) = (bs X , f)
♯'bt (σδ' (Q , h) , f) = ?
-}
-- DS't

DS't : (D E : Set₁) → bt → Set₁
DS't D E leaf = DS' D E
DS't D E (branch xx yy) = DS't D (DS't D E yy) xx

η't : {D E : Set₁} → {xx : bt} → E → DS't D E xx
η't {D}{E}{xx = leaf} e = η e
η't {D}{E}{xx = branch aa bb} x = η't {D = D} {E = DS't D E bb} {xx = aa} (η't {D}{E} {xx = bb} x)

DS't→ : (D : Set₁) → {E E' : Set₁} → {xx : bt} → (φ : E → E') → DS't D E xx → DS't D E' xx
DS't→ D {xx = leaf} φ = DS'-map φ
DS't→ D {xx = branch xx yy} φ = DS't→ D {xx = xx} (DS't→ D {xx = yy} φ)

μₒ : {D E : Set1} →  DS't D (DS't D E leaf) leaf → DS't D E leaf
μₒ x = μ x


MU : {D E : Set1} → {xx : bt} → DS't D E xx → DS't D E leaf
MU {xx = leaf} x = x
MU {D}{E}{xx = (branch leaf yy)} x = μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))x )
MU {D} {E} {xx = (branch (branch aa bb) yy)} x = MU {D}{E} {xx = (branch aa leaf)} ( DS't→ D {E = (DS't D (DS't D E yy ) bb)}{E' = DS't D E leaf}{xx = aa} (MU {D} {E} { xx = (branch bb yy) } )  x)


-- reduction via DS't


redu' : (D : Set₁) → {E : Set₁} → (xx yy : bt) → Levt D (Levt D E yy ) xx  → DS't D(DS't D E yy) xx
redu' D leaf leaf = ♭leaf ∘ (Levt→ D (♭leaf))
redu' D {E} leaf (branch aa bb) = (DS't→ D {E = (Levt D (Levt D E bb) aa)}{E' = (DS't D (DS't D E bb) aa)} {xx = leaf} (redu' D {E} aa bb)) ∘ (DS't→ D {xx = leaf} 𝒹 ) ∘ ( ♭leaf {D}{E = (Levt D E (branch aa bb))})
redu'  D {E} (branch pp qq) (branch aa bb) = (DS't→ D {E = DS't D (Levt D E (branch aa bb))qq}{E' = DS't D (DS't D E (branch aa bb)) qq}{pp} (DS't→ D {E = (Levt D E (branch aa bb))}{E' = DS't D (DS't D E bb) aa}{qq} ((redu' D {E} aa bb) ∘ 𝒹 ))) ∘ (redu' D {E = (Levt D E (branch aa bb))}pp qq) ∘ 𝒹
redu' D {E} (branch aa bb) leaf = (DS't→ D {E = (DS't D (Levt D E leaf) bb) } {E' = (DS't D (DS't D E leaf) bb)} {aa} (DS't→ D {E = (Levt D E leaf)}{E' = DS't D E leaf} {bb} ♭leaf)) ∘  (redu' D {E = (Levt D E leaf)} aa bb) ∘ 𝒹
 
redu : (D : Set₁) → {E : Set₁} → (yy : bt) → Cod𝒹 {D = D} {E = E} yy → DS't D E yy
redu D leaf = ♭leaf
redu D {E = E}(branch xx yy) w = redu' D {E = E} xx yy w
  
Redu : {D E : Set1} → (xx : bt) → Cod𝒹𝒹 D E xx → DS't D E xx
Redu leaf = ♭leaf
Redu {D = D}{E = E}(branch xx yy) = (Redu {D = D} { E = (DS't D E yy)} xx) ∘ (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = xx} (redu D {E = E} yy))

-- Second composite of the translation

REDU : {D E : Set1} → {xx : bt} → Cod𝒹𝒹 D E xx → DS't D E leaf
REDU {D}{E}{xx} Z = MU {xx = xx} ( Redu xx Z)

-- Proposition: There is a translation from annotated 'levitation' codes to DS' D E codes (i.e. to Dybjer-setzer (SP , Arg)

♭Trans : {D E : Set₁}{xx : bt} → Levt D E xx → DS' D E
♭Trans {D}{E}{xx} = REDU {D}{E}{xx} ∘ 𝒹𝒹 {D}{E}{xx}

{-One could complement this by ♯Trans in the other direction and show roundtrips-}

{---------------------------- Semantics ---------------------------------}


-- Semantics of Levt

mutual
  iL : {D : Set₁}{xx : bt} -> (X : LevtC D xx) -> Fam D -> Set
  iL (bt-bs (S , P)) (U , T) = (S ◃ P) U
  iL {D}{xx = branch xx yy} (bt-it .xx .yy R f) P = Σ[ x ∈ iL R P ] (iL (f (dL R P x)) P)

  dL : {D : Set₁}{xx : bt} -> (X : LevtC D xx) -> (P : Fam D) -> iL X P -> LevtP X
  dL (bt-bs (S , P)) (U , T) = ◃-map S P T
  dL {D}{xx = branch xx yy} (bt-it .xx .yy R f) P (x , p) = dL R P x , dL (f (dL R P x)) P p


⟦_⟧L : {D E : Set₁}{xx : bt} -> Levt D E xx -> Fam D -> Fam E
⟦ X , f ⟧L P = Fam-map f (iL X P , dL X P)

{----------------------------------- preservation of semantics -----------------------------------}

lemma-♭leaf : {D E : Set₁} → {P : Fam D} → (c : Levt D E leaf) → ⟦ ♭leaf c ⟧' P ≃ ⟦ c ⟧L P
lemma-♭leaf {P = (U , T)} (bt-bs (S , P) , f) = IdEq {-begin
                                         ⟦ (ι' (S , P) , λ x → f (♯pos x)) ⟧' (U , T)
                                         ≃⟨ IdEq ⟩
                                         ( cont (S , P) U , λ {(x , g) → f (♯pos (x , T ∘ g))})
                                         ≃⟨ IdEq ⟩
                                         ( (S ◃ P) U , f ∘ (◃-map S P T)  )
                                         ∎ where open ≃-Reasoning-}
                
-- Theorem: ♭Trans preserves semantics

Theorem-sem' : {D E : Set₁}{xx : bt} → {X : Fam D} → ( c : Levt D E xx) →  ⟦ ♭Trans c ⟧' X ≃ ⟦ c ⟧L X
Theorem-sem' {D}{E}{leaf}{X} (bt-bs (S , P) , f) = IdEq 
Theorem-sem' {D}{E}{xx = (branch leaf yy)}{X} (bt-it .leaf .yy Q h  , f) = begin
                                           ⟦ (REDU {D}{E}{branch leaf yy}( 𝒹𝒹 {D}{E}{branch leaf yy} (bt-it leaf yy Q h , f))) ⟧' X
                                           ≃⟨ IdEq ⟩
                                           ⟦ (MU {D}{E}{branch leaf yy} ( Redu {D}{E} (branch leaf yy) ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))) ⟧' X
                                           ≃⟨ IdEq ⟩
                                           ⟦ (MU {D}{E}{branch leaf yy} ( Redu {D}{E = (DS't D E yy)} leaf ( (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy  } {xx = leaf} (redu D {E = E} yy))
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))) ⟧' X
                                           ≃⟨ IdEq ⟩
                                           ⟦ (μₒ{D}{E} (
                                                       (DS'-map (MU {D}{E} {xx = yy}))
                                                         (♭leaf (
                                                                  Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = leaf}
                                                                           (redu D {E = E} yy)
                                                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))))) ⟧' X
                                           ≃⟨ IdEq ⟩
                                           ⟦ (μₒ{D}{E} (
                                                       (DS'-map (MU {D}{E} {xx = yy}))
                                                         (♭leaf (
                                                                  Levt→ D  {xx = leaf}
                                                                           (redu D {E = E} yy)
                                                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))))) ⟧' X
                                           ≃⟨ IdEq ⟩
                                           (let DD = ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))  ))
                                           in
                                           ⟦ (μₒ{D}{E} (
                                                       (DS'-map (MU {D}{E} {xx = yy}))
                                                         (♭leaf ( (proj₁  DD , (redu D {E = E} yy) ∘ (proj₂ DD))  )))) ⟧' X)
                                            ≃⟨ IdEq ⟩
                                           (let DD = ( 𝒹𝒹 ( Q , λ x →  (𝒹 ((λ x → Δ {Q = Q} h x f)x))  ))
                                           in
                                           ⟦ (μₒ{D}{E} (
                                                       (DS'-map (MU {D}{E} {xx = yy}))
                                                         (♭leaf (proj₁  DD , (redu D {E = E} yy) ∘ (proj₂ DD)  )))) ⟧' X)
                                           ≃⟨ {!!} ⟩
                                           ( (Σ[ x ∈ iL Q X ](iL (h (dL Q X x)) X)) , (f ∘ (λ{(x , p) → (dL Q X x , dL (h (dL Q X x)) X p)})) )
                                           ≃⟨ IdEq ⟩
                                          (iL (bt-it leaf yy Q h) X , f ∘ dL (bt-it leaf yy Q h) X)
                                                         ∎ where open ≃-Reasoning

Theorem-sem' {D}{E}{xx = (branch (branch aa bb ) yy)}{X} (bt-it .(branch aa bb ) .yy Q h  , f) = {!!}

{-
𝒹 : {D E : Set1} → {ww : bt} → Levt D E ww → Cod𝒹 {D = D} {E = E} ww
𝒹 (bt-bs R , f) = (bt-bs R , f)
𝒹 (bt-it xx yy Q h , f) =  ( Q ,  λ x → Δ {Q = Q} h x f)
-}

{-
♭leaf : {D E : Set₁} → Levt D E leaf → DS' D E
♭leaf (bt-bs X , f) = (ι' X , λ x → f (♯pos x) )
-}

{-
𝒹𝒹 : {D E : Set1} → {ww : bt} → Levt D E ww → Cod𝒹𝒹 D E ww
𝒹𝒹 (bt-bs R , f) = (bt-bs R , f)
𝒹𝒹(bt-it xx yy Q h , f) = 𝒹𝒹 (proj₁ (𝒹 (bt-it xx yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it xx yy Q h , f)))x)))

-}

{-
Levt→ : ∀ {a} → (D : Set₁) -> {xx : bt} → {E E' : Set a} → (φ : E → E') -> Levt D E xx → Levt D E' xx
Levt→ D {xx = xx} φ = (cont-map (ContLevt {D} xx)) φ
-}

{-
Cod𝒹𝒹→ : (D : Set₁) → {E E' : Set₁} → {xx : bt} → (φ : E → E') → Cod𝒹𝒹 D E xx → Cod𝒹𝒹 D E' xx
Cod𝒹𝒹→ D {xx = leaf} φ  = Levt→ D φ 
Cod𝒹𝒹→ D {E = E}{E' = E'} {xx = (branch xx yy)} φ = Cod𝒹𝒹→ D {E = Cod𝒹 {D = D}{E = E} yy }{E' = Cod𝒹 {D = D}{E = E'} yy } {xx = xx} (Cod𝒹→ D {E = E }{E' = E' } {xx = yy}  φ)

-}

Theorem-sem0 : {D E : Set₁}{xx : bt} → {X : Fam D} → ( c : Levt D E xx) →  arg (proj₁(♭Trans c))  X ≌ iL (proj₁ c) X
Theorem-sem0 {D}{E}{leaf}{X} (bt-bs (S , P) , f) = ideq 
Theorem-sem0 {D}{E}{xx = (branch leaf yy)}{X} (bt-it .leaf .yy Q h  , f) = begin
                                           (arg (proj₁ (REDU {D}{E}{branch leaf yy}( 𝒹𝒹 {D}{E}{branch leaf yy} (bt-it leaf yy Q h , f)))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ (MU {D}{E}{branch leaf yy} ( Redu {D}{E} (branch leaf yy) ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ (μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))( Redu {D}{E} (branch leaf yy)
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))) ))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ (μₒ{D}{E} ((DS'-map (MU {D}{E} {xx = yy}))( Redu {D}{E} (branch leaf yy)( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ (μₒ{D}{E} ((DS'-map (MU {D}{E} {xx = yy}))
                                           ((Redu {D = D} { E = (DS't D E yy)} leaf) ((Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = leaf} (redu D {E = E} yy))
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ (μₒ{D}{E} ((DS'-map (MU {D}{E} {xx = yy}))
                                           ((♭leaf) ((Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = leaf} (redu D {E = E} yy))
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))))) X)
                                           --≌⟨ ideq ⟩
                                           --
                                           {-≌⟨ ideq ⟩
                                           (arg (proj₁ (μ0{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))( Redu {D}{E} (branch leaf yy)
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))) ))) X)-}
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ ((MU {D}{E}{branch leaf yy} ( Redu {D}{E = (DS't D E yy)} leaf ( (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy  } {xx = leaf} (redu D {E = E} yy))
                                           (𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))))) X)
                                           ≌⟨ {!!} ⟩
                                           (Σ[ x ∈ iL Q X ](iL (h (dL Q X x)) X))
                                           ≌⟨ ideq ⟩
                                           (iL (bt-it leaf yy Q h) X)
                                                         ∎ where open ≌-Reasoning
Theorem-sem0 {D}{E}{xx = (branch (branch aa bb) yy)}{X} (bt-it .(branch aa bb) .yy Q h  , f) = begin
                                           (arg (proj₁ (REDU {D}{E}{branch (branch aa bb) yy}( 𝒹𝒹 {D}{E}{branch (branch aa bb) yy} (bt-it (branch aa bb) yy Q h , f)))) X)
                                           ≌⟨ ideq ⟩
                                           ( arg (proj₁ (MU {D}{E}{branch (branch aa bb) yy} ( Redu {D}{E} (branch (branch aa bb) yy) ( 𝒹𝒹 (proj₁ (𝒹 (bt-it (branch aa bb) yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it (branch aa bb) yy Q h , f)))x))))))) X)
                                           ≌⟨ ideq ⟩
                                           (arg (proj₁ ((MU {D}{E}{branch (branch aa bb) yy} ( Redu {D}{E = (DS't D E yy)} (branch aa bb) ( (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy  } {xx = (branch aa bb)} (redu D {E = E} yy))
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it (branch aa bb) yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it (branch aa bb) yy Q h , f)))x))))))))) X)
                                           ≌⟨ {!!} ⟩
                                           (Σ[ x ∈ iL Q X ](iL (h (dL Q X x)) X))
                                           ≌⟨ ideq ⟩
                                          (iL (bt-it (branch aa bb) yy Q h) X)
                                                         ∎ where open ≌-Reasoning


{-
((Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = leaf} (redu D {E = E} yy))
                                           ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))
 -}
--♭leaf : {D E : Set₁} → Levt D E leaf → DS' D E
--♭leaf (bt-bs X , f) = (ι' X , λ x → f (♯pos x) )


{-
( Redu {D}{E} (branch leaf yy)( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x)))))
((Redu {D = D} { E = (DS't D E yy)} leaf) ((Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = leaf} (redu D {E = E} yy)) ( 𝒹𝒹 (proj₁ (𝒹 (bt-it leaf yy Q h , f)) , λ x →  (𝒹 ((proj₂ (𝒹 (bt-it leaf yy Q h , f)))x))))))
-}

{-
Redu : {D E : Set1} → (xx : bt) → Cod𝒹𝒹 D E xx → DS't D E xx
Redu leaf = ♭leaf 
Redu {D = D}{E = E}(branch xx yy) = (Redu {D = D} { E = (DS't D E yy)} xx) ∘ (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = xx} (redu D {E = E} yy))
-}

{-
DS't→ : (D : Set₁) → {E E' : Set₁} → {xx : bt} → (φ : E → E') → DS't D E xx → DS't D E' xx
DS't→ D {xx = leaf} φ = DS'-map φ
DS't→ D {xx = branch xx yy} φ = DS't→ D {xx = xx} (DS't→ D {xx = yy} φ)
-}

{-
DS'-map : {D E E' : Set₁} → (E -> E') -> DS' D E -> DS' D E'
DS'-map f (c , α) = (c , (f ∘ α))
-}
{-
MU : {D E : Set1} → {xx : bt} → DS't D E xx → DS't D E leaf
MU {xx = leaf} x = x
MU {D}{E}{xx = (branch leaf yy)} x = (μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))x ))
MU {D} {E} {xx = (branch (branch aa bb) yy)} x = MU {D}{E} {xx = (branch aa leaf)} ( DS't→ D {E = (DS't D (DS't D E yy ) bb)}{E' = DS't D E leaf}{xx = aa} (MU {D} {E} { xx = (branch bb yy) } )  x)
-}
{-
Redu : {D E : Set1} → (xx : bt) → Cod𝒹𝒹 D E xx → DS't D E xx
Redu leaf = ♭leaf 
Redu {D = D}{E = E}(branch xx yy) = (Redu {D = D} { E = (DS't D E yy)} xx) ∘ (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy} {xx = xx} (redu D {E = E} yy))
-}
{-
(REDU {D}{E}{branch xx yy}( 𝒹𝒹 {D}{E}{branch xx yy} (bt-it xx yy Q h , f)))

REDU {D}{E}{branch xx yy} Z = MU {branch xx yy} ( Redu (branch xx yy) Z)

MU : {D E : Set1} → {xx : bt} → DS't D E xx → DS't D E leaf
MU {xx = leaf} x = x
MU {D}{E}{xx = (branch leaf yy)} x = μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))x )
MU {D} {E} {xx = (branch (branch aa bb) yy)} x = MU {D}{E} {xx = (branch aa leaf)} ( DS't→ D {E = (DS't D (DS't D E yy ) bb)}{E' = DS't D E leaf}{xx = aa} (MU {D} {E} { xx = (branch bb yy) } )  x)


-}
Theorem-sem1 : {D E : Set₁}{xx : bt} → {X : Fam D} → ( c : Levt D E xx) → (x : arg (proj₁(♭Trans c))  X) → (((proj₂ (♭Trans c)) ∘ ⟦ (proj₁ (♭Trans c)) ⟧'map X) x) ≡ (((proj₂ c) ∘ (dL (proj₁ c) X)) (Prelude.Equivalences._≌_.left-Set (Theorem-sem0 c) x))
Theorem-sem1 = {!!}

Theorem-sem : {D E : Set₁}{xx : bt} → {X : Fam D} → ( c : Levt D E xx) →  ⟦ ♭Trans c ⟧' X ≃ ⟦ c ⟧L X
Theorem-sem c = Fam-≃ (Theorem-sem0 c) (Theorem-sem1 c)


{-Redu {D = D}{E = E}(branch xx yy) = (Redu {D = D} { E = (DS't D E yy)} xx) ∘ (Cod𝒹𝒹→ D {E = (Cod𝒹 {D} {E} yy)}{E' =  DS't D E yy  } {xx = xx} (redu D {E = E} yy))
-}

-- Lemmas about multiplication and Semantics

-- For Lev

Σ⟦⟧- : {D E : Set₁} → (Fam D) → Fam (Lev D E) → Fam E
Σ⟦⟧- X (U , T) = ⋃ U (λ u → (-⟦ T u ⟧ X))

lemma-Σ⟦⟧- : {D E : Set₁}{P : Fam D} → (c : ( Lev D (Lev D E))) → ((Σ⟦⟧- P (-⟦ c ⟧ P)) ≡ -⟦ (μLev c) ⟧ P)
lemma-Σ⟦⟧- {P = (U , T)}(bs X , f) = refl
lemma-Σ⟦⟧- {P = (U , T)} (it R h , f) = refl

-- For Levt

Σ⟦⟧L : {D E : Set₁}{xx : bt} → (Fam D) → Fam (Levt D E xx) → Fam E
Σ⟦⟧L X (U , T) = ⋃ U (λ u → (⟦ T u ⟧L X))

lemma-Σ⟦⟧L : {D E : Set₁}{xx yy : bt}{P : Fam D} → (c : ( Levt D (Levt D E yy) xx)) → ((Σ⟦⟧L P (⟦ c ⟧L P)) ≡ ⟦ (μLevt c) ⟧L P)
lemma-Σ⟦⟧L {P = (U , T)}(bt-bs X , f) = refl
lemma-Σ⟦⟧L {P = (U , T)} (bt-it xx yy R h , f) = refl

-- For DS

Σ⟦⟧DS : {D E : Set₁} → (Fam D) → Fam (DS.SigmaDelta.DS D E ) → Fam E
Σ⟦⟧DS X (U , T) = ⋃ U (λ u → (DS.SigmaDelta.⟦ T u ⟧ X))
{-
lemma-Σ⟦⟧DS : {D E : Set₁}{P : Fam D} → (c : ( DS.SigmaDelta.DS D (DS.SigmaDelta.DS D E))) → ((Σ⟦⟧DS P (DS.SigmaDelta.⟦ c ⟧ P)) ≃ DS.SigmaDelta.⟦ (DS.SigmaDelta.μDS c) ⟧ P)
lemma-Σ⟦⟧DS {P = (U , T)}(ι e) = begin
                                  (⋃ (DS.SigmaDelta.⟦ ι e ⟧₀ (U , T)) (λ u → (DS.SigmaDelta.⟦ (DS.SigmaDelta.⟦ ι e ⟧₁ (U , T)) u ⟧ (U , T))))
                                  ≃⟨ IdEq ⟩
                                  (⋃ ⊤ (λ u → (DS.SigmaDelta.⟦ e ⟧ (U , T))))
                                  ≃⟨ IdEq ⟩
                                  ((Σ[ a ∈ ⊤ ] ⊤) , (λ { (a , x) → e }))
                                  ≃⟨ Fam-≃ (record{left-Set = λ _ → tt ; right-Set = λ _ → (tt , tt) ; lr-Set = refl ; rl-Set = refl })(λ _ → refl) ⟩
                                  (⊤ , const e)
                                  ≃⟨ ? ⟩
                                  (DS.SigmaDelta.⟦ e ⟧(U , T))
                                 ∎ where open ≃-Reasoning
                                  
lemma-Σ⟦⟧DS {P = (U , T)} (σδ A f) = {!!}
--lemma-Σ⟦⟧DS {P = (U , T)} (δ A f) = {!!}
-}

-- For DS' ... it is more complicated

Σ⟦⟧ : {D E : Set₁} → (Fam D) → Fam (DS' D E ) → Fam E
Σ⟦⟧ X (U , T) = ⋃ U (λ u → (⟦ T u ⟧' X))

lemma-Σ⟦⟧ : {D E : Set₁}{P : Fam D} → (c : ( DS' D (DS' D E))) → ((Σ⟦⟧ P (⟦ c ⟧' P)) ≃ ⟦ (μ c) ⟧' P)
lemma-Σ⟦⟧ {D}{E}{P} (ι' X , f) = IdEq
lemma-Σ⟦⟧ {D}{E}{P = (U , T)} (σδ' (Q , h)  , f) =  begin
                                   (⋃ (ind (⟦ (σδ' (Q , h)  , f) ⟧' (U , T))) (λ u → (⟦ (fib (⟦ (σδ' (Q , h)  , f) ⟧' (U , T))) u ⟧' (U , T))))
                                  {- ≡⟨ refl ⟩
                                   (⋃ (Σ[ x ∈ cont Q U ] arg (h (cont-map Q T x)) (U , T)) (λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' P)))
                                   )-}
                                   ≃⟨ IdEq ⟩
                                   (let A = (Σ[ x ∈ cont Q U ] arg (h (cont-map Q T x)) (U , T))
                                        g = (λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T)))
                                   in
                                   ((Σ[ a ∈ A ] ind (g a)) , (λ { (a , x) → fib (g a) x }))
                                   )
                                   ≃⟨ IdEq ⟩
                                   (let g = (λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T)))
                                   in
                                   ((Σ[ a ∈ (Σ[ x ∈ cont Q U ] (arg (h (cont-map Q T x)) (U , T))) ] ind (g a)) , (λ { (w , c) → fib (g w) c })))
                                   {-≃⟨ assocΣ (cont Q U) ((arg (h (cont-map Q T x)) (U , T))) ((ind (g (x , x')))) ⟩
                                   (let (U , T) = P
                                        g = (λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T)))
                                     -- g (x , y) = (⟦ ( (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)) ⟧' (U , T))

                                   in
                                   (Σ[ x ∈ cont Q U ](Σ[ x' ∈ (arg (h (cont-map Q T x)) (U , T)) ](ind (g (x , x')))) , (λ { (a , c) → fib (g a) c })))-}
                                   {!≃⟨ assocΣFam (cont Q U) ⟩
                                   (let g = (λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T)))
                                    in
                                     ((Σ[ x ∈ cont Q U ](Σ[ x' ∈ (arg (h (cont-map Q T x)) (U , T)) ] (ind (g (x ,  x'))))) , (λ { (a , x) → fib (g a) x })) )!}
                                   ≃⟨ {!!} ⟩
                                   ((Σ[ x ∈ cont Q U ] (arg ((λ x → μ0 (h x) (λ y → f (x , y))) (cont-map Q T x)) (U , T))) ,
                                   (λ {(x , y) → (μ1 (h x) (f ∘ (λ z → x , z)) y)}) ∘ (λ {(x , y) →  cont-map Q T x , (⟦ (λ x → μ0 (h x) (λ y → f (x , y)))  (cont-map Q T x) ⟧'map ( U , T) y)}))
                                   ≃⟨ IdEq ⟩
                                   (let φ = (λ x → μ0 (h x) (λ y → f (x , y)))
                                        ψ = (λ {(x , y) → (μ1 (h x) (f ∘ (λ z → x , z)) y)})
                                    in
                                   ((Σ[ x ∈ cont Q U ] arg (φ (cont-map Q T x)) (U , T)) , ψ ∘ (λ {(x , y) →  cont-map Q T x , (⟦ φ  (cont-map Q T x) ⟧'map ( U , T) y)})))
                                   ≃⟨ IdEq ⟩
                                   (⟦ ((σδ' (Q , (λ x → μ0 (h x) (λ y → f (x , y))))) , (λ {(x , y) → (μ1 (h x) (f ∘ (λ z → x , z)) y)})) ⟧' (U , T))
                                   ≃⟨ IdEq ⟩
                                   (⟦ (μ0 (σδ' (Q , h)) f , μ1 (σδ' (Q , h)) f) ⟧' (U , T))
                                          ∎ where open ≃-Reasoning

{-
(Σ[ x ∈ cont Q U ](Σ[ x' ∈ (arg (h (cont-map Q T x)) (U , T)) ] (ind ((λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T))) ))) ,
                                   (λ { (a , x) → fib ((λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T))) a) x }))
                                   -}

{-
(λ x → (arg (h (cont-map Q T x)) (U , T))) ( λ u → (ind (((⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T))) )))
                                   (λ { (a , x) → fib ((λ u → (⟦ ( λ { (x , y) → (cont-map Q T x , ⟦ h (cont-map Q T x) ⟧'map ( U , T) y)}) u ⟧' (U , T))) a) x })
-}

{-
 arg (σδ' (Q , h)) (U , T)
    = (Σ[ x ∈ cont Q U ] arg (h (cont-map Q T x)) (U , T))
    -}
{-
⟦ σδ' (Q , h) ⟧'map (U , T) 
    = (λ {(x , y) →  cont-map Q T x , (⟦ h  (cont-map Q T x) ⟧'map ( U , T) y)})
    -}

{-
⋃ : {D : Set1} -> (A : Set) -> (A -> Fam D) -> Fam D
⋃ A f = ((Σ[ a ∈ A ] ind (f a)) , (λ { (a , x) → fib (f a) x }))
-}

{-

μ0 : {D E : Set1} -> (c : SP D) -> (α : Arg c -> DS' D E) -> SP D
μ0 (ι' X) α = σδ' (X , (proj₁ ∘ α))
μ0 (σδ' (Q , h)) α =  (σδ' (Q , (λ x → μ0 (h x) (λ y → α (x , y)))))

μ1 : ∀ {D E} c α -> Arg (μ0 {D} {E} c α) -> E
μ1 (ι' X) α (x , y) = proj₂ (α x) y
μ1 (σδ' (Q , h)) α (x , y) = (μ1 (h x) (α ∘ (λ z → x , z)) y)
-}
                                   
{-
μ : {D E : Set1} -> DS' D (DS' D E) -> DS' D E
μ (c , α) = (μ0 c α , μ1 c α)

-}





-- Semantics of DS't

{- DS't D E (branch (branch aa bb) yy) =  DS't D (DS't D (DS't D E yy) bb) aa
Tagt returns the "E" from the unfolded (to DS') DS't -}

Tagt : (D E : Set₁) → bt → Set₁
Tagt D E leaf = E
Tagt D E (branch leaf yy) = (DS't D E yy)
Tagt D E (branch (branch aa bb) yy) = Tagt D (DS't D E (branch bb yy)) aa -- (DS't D (DS't D E yy) bb)
{-All this checks but is presrntly not used
Idx : (aa bb yy : bt) → bt
Idx leaf bb yy = branch bb yy
Idx (branch leaf zz) bb yy = branch zz (branch bb yy)
Idx (branch (branch pp qq) zz) bb yy = Idx  pp (branch qq zz) (branch bb yy)


lemma-Tagt-DS't : {D E : Set₁}(aa bb yy : bt) → Tagt D E (branch (branch aa bb) yy) ≡ DS't D E (Idx aa bb yy)
lemma-Tagt-DS't {D}{E} leaf bb yy  = refl
lemma-Tagt-DS't {D}{E} (branch leaf zz) bb yy  = refl
lemma-Tagt-DS't {D}{E} (branch (branch pp qq) zz) bb yy  = begin
                                                           -- Tagt D E (branch (branch aa bb) yy) -- aa = (branch (branch pp qq) zz)
                                                           --≡⟨ refl ⟩
                                                           Tagt D E (branch (branch (branch (branch pp qq) zz) bb) yy)
                                                          {- ≡⟨ ? ⟩
                                                           Tagt D (DS't D E (branch bb yy)) (branch (branch pp qq) zz) -- E' := (DS't D E (branch bb yy))
                                                           --≡⟨ refl ⟩
                                                           --Tagt D E' (branch (branch pp qq) zz)
                                                           --≡⟨ refl ⟩
                                                           --Tagt D (DS't D E' (branch qq zz))pp-}
                                                           ≡⟨ refl ⟩
                                                           Tagt D (DS't D (DS't D E (branch bb yy))(branch qq zz)) pp
                                                           ≡⟨ refl ⟩
                                                           Tagt D (DS't D E (branch (branch qq zz) (branch bb yy) )) pp
                                                           ≡⟨ refl ⟩
                                                           Tagt D E (branch (branch pp (branch qq zz)) (branch bb yy) )
                                                           ≡⟨ refl ⟩
                                                           Tagt D E (Idx (branch leaf (branch pp (branch qq zz)) ) bb yy)
                                                           ≡⟨ refl ⟩
                                                           Tagt D E (branch (branch pp (branch qq zz)) (branch bb yy))
                                                           ≡⟨ lemma-Tagt-DS't {D}{E} pp (branch qq zz) (branch bb yy) ⟩
                                                           DS't D E (Idx  pp (branch qq zz) (branch bb yy))
                                                           ≡⟨ refl ⟩
                                                           DS't D E (Idx (branch (branch pp qq) zz) bb yy)
                                                           ∎ where open ≡-Reasoning

lemma-Tagt-DS't-leaf :  {D E : Set₁} → Tagt D E leaf ≡ E
lemma-Tagt-DS't-leaf = refl

lemma-Tagt-DS't-lb : {D E : Set₁}(xx : bt) → Tagt D E (branch leaf xx) ≡ DS't D E xx
lemma-Tagt-DS't-lb xx = refl

IDX : bt → bt → bt
IDX  leaf zz = zz
IDX  (branch pp qq) zz = Idx pp qq zz

lemma-Tagt : {D E : Set₁}(xx yy : bt) → Tagt D E (branch xx yy) ≡ DS't D E (IDX xx yy)
lemma-Tagt leaf zz  = refl
lemma-Tagt (branch pp qq) zz = lemma-Tagt-DS't pp qq zz

prefix : {D E : Set₁} → {xx : bt} → Tagt D E xx → DS't D (Tagt D E xx) xx
prefix {D}{E}{xx} x = η't {D = D} {E =  Tagt D E xx} {xx = xx} x

νT : {D E : Set₁} {xx : bt} → Fam (Tagt D E xx) → bt
νT {xx = leaf} X = leaf
νT {xx = (branch leaf yy)} X = yy
νT {xx = (branch (branch aa bb) yy)} X = (branch (branch aa bb) yy)
-}
unfold-DS't : {D E : Set₁}{xx : bt} → DS't D E xx → DS' D (Tagt D E xx)
unfold-DS't {xx = leaf} c = c
unfold-DS't {xx = (branch leaf yy)} c = c
unfold-DS't {xx = branch (branch aa bb) yy} c = unfold-DS't {xx = aa} c

expand-DS't : (D E : Set₁) → (xx : bt) → Set₁
expand-DS't D E leaf = DS't D E leaf
expand-DS't D E (branch xx yy) = expand-DS't D (expand-DS't D E yy) xx

coerce-'t :  {D E : Set₁}{xx : bt} → DS't D E xx → expand-DS't D E xx
coerce-'t {xx = leaf} c = c
coerce-'t {D}{E}{xx = branch xx yy} = (coerce-'t {D}{E = expand-DS't D E yy }{xx = xx}) ∘ (DS't→ D {E = DS't D E yy}{E' = expand-DS't D E yy} {xx = xx} (coerce-'t {D}{E} {xx = yy}))

{- DS't D E (branch yy (branch aa bb)) =  DS't D (DS't D (DS't D E bb) aa) yy-}

-- Semantics of DS't

⟦⟧t : {D E : Set₁}{xx : bt} → DS't D E xx → Fam D → Fam (Tagt D E xx) -- DS't D E xx = Tagt D E (branch leaf xx)
⟦⟧t {D}{E}{xx = leaf} c X = ⟦ c ⟧' X
⟦⟧t {xx = branch leaf yy} c X = ⟦ c ⟧' X
⟦⟧t {xx = branch (branch aa bb) yy} c X  = ⟦ unfold-DS't {xx = branch (branch aa bb) yy} c ⟧' X

νt : {D E : Set₁} → {xx : bt} → DS't D E xx → bt
νt {xx = xx} c = xx
--νt {xx = leaf} c = leaf
--νt {xx = (branch xx yy)} c = branch xx yy

Σ⟦⟧t : {D E : Set₁} → (Fam D) → {xx : bt} → Fam (DS't D E xx) → Fam (Tagt D E xx)
Σ⟦⟧t {D}{E} X {xx} (U , T) = ⋃ {D = (Tagt D E xx)} U (λ u → (⟦⟧t {xx = νt {xx = xx} (T u)} (T u) X))
--Σ⟦⟧t {D}{E} X {xx = leaf} (U , T) = ⋃ U (λ u → (⟦⟧t {xx = νt {xx = leaf} (T u)} (T u) X))
--Σ⟦⟧t {D}{E} X {xx = (branch xx yy)} (U , T) = ⋃ U (λ u → (⟦⟧t {xx = νt {xx = (branch xx yy)} (T u)} (T u) X))

-- do it with Tagt in place of DS't for compatibility with Σ⟦⟧t. Use Tagt D E (branch leaf xx) = DS't D E xx. Also include the case of unary sum
Σ⟦⟧*t : {D E : Set₁} → (Fam D) → {xx : bt} → Fam (Tagt D E xx) → Fam E
Σ⟦⟧*t {D}{E} X {xx = leaf} Y = Y --include unary sum 
Σ⟦⟧*t {D}{E} X {xx = (branch leaf zz)} Y = Σ⟦⟧*t {D}{E} X {xx = zz}  (Σ⟦⟧t {D}{E} X {xx = zz} Y)
Σ⟦⟧*t {D}{E} X {xx = branch (branch leaf qq) yy } Y = Σ⟦⟧*t {D}{E} X {xx = (branch qq yy)}(Σ⟦⟧t {D}{E} X {xx = (branch qq yy)} Y)
Σ⟦⟧*t {D}{E} X {xx = branch (branch (branch aa bb) qq) yy } Y = Σ⟦⟧*t {D}{E} X {xx = branch qq yy} (Σ⟦⟧t {D}{E} X {xx = branch qq yy}( Σ⟦⟧*t {D}{E = DS't D E (branch qq yy)} X {xx = (branch aa bb)} Y))


{-
lemma-⋃ : {D E : Set1} -> {A : Set} -> (H : A -> Fam D) → (s : Fam D → Fam E) →
          s (⋃ A (λ a →  H a)) ≡ (⋃ A (λ a →  (s (H a))))
lemma-⋃ F s = begin
             s ((Σ[ a ∈ A ] ind (H a)) , (λ { (a , x) → fib (H a) x }))
             ≡⟨ ⟩
             (Σ[ a ∈ A ] ind (s(H a)))
-}

{-
xx = (branch leaf yy)
H u = (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P)
-}

lemma-⋃ :  {D E : Set₁}{xx : bt}{P : Fam D}  → (U : Set) →  (H : U → Fam (Tagt D E xx)) → (Σ⟦⟧*t {D}{E} P {xx} ( ⋃ {D = Tagt D E xx} U (λ u → (H u)))) ≡ ( ⋃ U  (λ u → Σ⟦⟧*t {D}{E} P {xx} (H u)))
lemma-⋃ {xx = leaf} U H = refl
lemma-⋃ {D}{E}{xx = (branch leaf zz)}{P} U H =  begin
                                    (Σ⟦⟧*t {D}{E} P {xx = (branch leaf zz)} ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u))))
                                    ≡⟨ refl ⟩
                                    (Σ⟦⟧*t {D}{E} P {zz}  (Σ⟦⟧t {D}{E} P {xx = zz} ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))))
                                    ≡⟨ refl ⟩
                                    (Σ⟦⟧*t {D}{E} P {zz} ( ⋃ {D = (Tagt D E zz)}
                                    (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) -- = "U"
                                    (λ u → (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u)} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u) P))) ) -- = "(λ u → (H u))"
                                    ≡⟨ lemma-⋃ {D}{E}{xx = zz}{P} (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u))))
                                   (λ u → (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u)} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u) P)) ⟩
                                    ( ⋃ (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u))))
                                    (λ u' → (Σ⟦⟧*t {D}{E} P {zz}
                                    (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u')} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u') P))))
                                   {- ≡⟨ refl ⟩
                                    ( ⋃ (Σ[ u ∈ U ](ind (H u)))
                                    (λ u' → (Σ⟦⟧*t {D}{E} P {zz}
                                    (⟦⟧t {xx = νt {xx = zz} ((λ { (a , x) → fib (H a) x }) u')} ((λ { (a , x) → fib (H a) x }) u') P)))) -- (a , x) : (Σ[ u ∈ U ](ind (H u)))-}
                                    ≡⟨ refl ⟩
                                    ( ⋃ (Σ[ u ∈ U ](ind (H u)))
                                    (λ{ (u , x) → (Σ⟦⟧*t {D}{E} P {zz}
                                    (⟦⟧t {xx = νt {xx = zz} ( fib (H u) x )} ( fib (H u) x ) P))}))
                                    ≡⟨ refl ⟩
                                    ((Σ[ q ∈ (Σ[ u ∈ U ](ind (H u))) ]( ind (Σ⟦⟧*t {D}{E} P {zz} -- q=(u,x)
                                    (⟦⟧t {xx = νt {xx = zz} ( fib (H (proj₁ q)) (proj₂ q) )} ( fib (H (proj₁ q)) (proj₂ q) ) P)) )) ,
                                    (λ { (q , u') → fib (Σ⟦⟧*t {D}{E} P {zz}
                                    (⟦⟧t {xx = νt {xx = zz} ( fib (H (proj₁ q)) (proj₂ q) )} ( fib (H (proj₁ q)) (proj₂ q) ) P)) u' }))
                                     ≡⟨ refl ⟩
                                    (let  ST' : (q : (Σ[ u ∈ U ](ind (H u)))) →  Fam (Tagt D E zz)
                                          ST' q = (⟦⟧t {xx = νt {xx = zz} ((fib (H (proj₁ q)))(proj₂ q))} ((fib (H (proj₁ q)))(proj₂ q)) P)
                                    in
                                    ((Σ[ q ∈ (Σ[ u ∈ U ](ind (H u))) ](ind (Σ⟦⟧*t {D}{E} P {zz}  (ST' q)))) ,
                                    (λ { (q , u') → (fib (Σ⟦⟧*t {D}{E} P {zz}  (ST' q))) u' })))
                                    ≡⟨ {!!} ⟩
                                    (let ST : (u : U) → (x : (ind (H u))) → Fam (Tagt D E zz)
                                         ST u x = (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)
                                    in (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x))) ,  (λ { (x , s) → (fib( ST u x)) s }))
                                    )))) ,
                                     (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x)) ) ) , (λ { (x , s) → fib (ST u x) s }))) x })))
                                    ≡⟨ refl ⟩
                                    (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {xx = zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)) ) ) , (λ { (x , u') → fib (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P) u' }))
                                    ))) ,
                                    (λ { (u , w) → fib (Σ⟦⟧*t {D}{E} P {xx = zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)) ) ) , (λ { (x , u') → fib (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P) u' }))) w }))
                                    ≡⟨ refl ⟩
                                    (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {xx = zz}
                                    (⋃ {D = (Tagt D E zz)} (ind (H u)) (λ x → (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)))
                                    ) )) ,
                                    (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {xx = zz}
                                    (⋃ {D = (Tagt D E zz)} (ind (H u)) (λ u' → (⟦⟧t {xx = νt {xx = zz} ((fib (H u))u')} ((fib (H u))u') P)))) x }))
                                    ≡⟨ refl ⟩
                                    ( ⋃ U  (λ u → (Σ⟦⟧*t {D}{E} P {xx = zz}  (⋃ {D = (Tagt D E zz)} (ind (H u)) (λ u' → (⟦⟧t {xx = νt {xx = zz} ((fib (H u))u')} ((fib (H u))u') P))))))
                                    ≡⟨ refl ⟩
                                    ( ⋃ U  (λ u → Σ⟦⟧*t {D}{E} P {xx = zz}  (Σ⟦⟧t {D}{E} P {xx = zz} (H u))))
                                    ≡⟨ refl ⟩
                                    ( ⋃ U  (λ u → Σ⟦⟧*t {D}{E} P {xx = (branch leaf zz)} (H u)))
                                    ∎ where open ≡-Reasoning
lemma-⋃ {xx = (branch (branch xx yy) zz)} U H = {!!}

left-ST'-ST : {D E : Set₁}{zz : bt}{P : Fam D}  → {U : Set} →  {H : U → Fam (Tagt D E (branch leaf zz))} →
                                   ind (let  ST' : (q : (Σ[ u ∈ U ](ind (H u)))) →  Fam (Tagt D E zz)
                                             ST' q = (⟦⟧t {xx = νt {xx = zz} ((fib (H (proj₁ q)))(proj₂ q))} ((fib (H (proj₁ q)))(proj₂ q)) P)
                                    in (
                                    (Σ[ q ∈ (Σ[ u ∈ U ](ind (H u))) ](ind (Σ⟦⟧*t {D}{E} P {zz}  (ST' q)))) ,
                                    (λ { (q , u') → (fib (Σ⟦⟧*t {D}{E} P {zz}  (ST' q))) u' })))
                                    →
                                    ind (let ST : (u : U) → (x : (ind (H u))) → Fam (Tagt D E zz)
                                             ST u x = (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)
                                    in (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x))) ,  (λ { (x , s) → (fib( ST u x)) s }))
                                    )))) ,
                                     (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x)) ) ) , (λ { (x , s) → fib (ST u x) s }))) x })))
left-ST'-ST {zz = leaf} (q , u') = (proj₁ q ,( proj₂ q , u') )
left-ST'-ST {zz = (branch aa bb)} (q , u') = {!!}


right-ST'-ST : {D E : Set₁}{zz : bt}{P : Fam D}  → {U : Set} →  {H : U → Fam (Tagt D E (branch leaf zz))} →
                                    ind (let ST : (u : U) → (x : (ind (H u))) → Fam (Tagt D E zz)
                                             ST u x = (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)
                                    in (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x))) ,  (λ { (x , s) → (fib( ST u x)) s }))
                                    )))) ,
                                     (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x)) ) ) , (λ { (x , s) → fib (ST u x) s }))) x })))
                                    →
                                    ind (let  ST' : (q : (Σ[ u ∈ U ](ind (H u)))) →  Fam (Tagt D E zz)
                                              ST' q = (⟦⟧t {xx = νt {xx = zz} ((fib (H (proj₁ q)))(proj₂ q))} ((fib (H (proj₁ q)))(proj₂ q)) P)
                                    in (
                                    (Σ[ q ∈ (Σ[ u ∈ U ](ind (H u))) ](ind (Σ⟦⟧*t {D}{E} P {zz}  (ST' q)))) ,
                                    (λ { (q , u') → (fib (Σ⟦⟧*t {D}{E} P {zz}  (ST' q))) u' })))
right-ST'-ST {zz = leaf} (u , x) = ((u , proj₁ x) , proj₂ x)
right-ST'-ST {zz = (branch aa bb)} (u , x) = {!!}

{-comment out for the moment
lemma-⋃-≃ :  {D E : Set₁}{xx : bt}{P : Fam D}  → (U : Set) →  (H : U → Fam (Tagt D E xx)) → (Σ⟦⟧*t {D}{E} P {xx} ( ⋃ {D = Tagt D E xx} U (λ u → (H u)))) ≃ ( ⋃ U  (λ u → Σ⟦⟧*t {D}{E} P {xx} (H u)))
lemma-⋃-≃ {xx = leaf} U H = IdEq
lemma-⋃-≃ {D}{E}{xx = (branch leaf zz)}{P} U H =  begin
                                    (Σ⟦⟧*t {D}{E} P {zz} ( ⋃ {D = (Tagt D E zz)}
                                    (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) -- = "U"
                                    (λ u → (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u)} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u) P))) ) -- = "(λ u → (H u))"
                                    ≃⟨ lemma-⋃-≃ {D}{E}{xx = zz}{P} (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u))))
                                    (λ u → (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u)} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u) P)) ⟩
                                    ( ⋃ (ind ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u))))
                                    (λ u' → (Σ⟦⟧*t {D}{E} P {zz}
                                    (⟦⟧t {xx = νt {xx = zz} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u')} ((fib ( ⋃ {D = Tagt D E (branch leaf zz)} U (λ u → (H u)))) u') P))))
                                    ≃⟨ IdEq ⟩
                                    (let  ST' : (q : (Σ[ u ∈ U ](ind (H u)))) →  Fam (Tagt D E zz)
                                          ST' q = (⟦⟧t {xx = νt {xx = zz} ((fib (H (proj₁ q)))(proj₂ q))} ((fib (H (proj₁ q)))(proj₂ q)) P)
                                    in
                                    ((Σ[ q ∈ (Σ[ u ∈ U ](ind (H u))) ](ind (Σ⟦⟧*t {D}{E} P {zz}  (ST' q)))) ,
                                    (λ { (q , u') → (fib (Σ⟦⟧*t {D}{E} P {zz}  (ST' q))) u' })))
                                    ≃⟨ Fam-≃ (left-ST'-ST {D}{E}{zz}) (right-ST'-ST {D}{E}{zz}) {!!} {!!} {!!} ⟩
                                    (let ST : (u : U) → (x : (ind (H u))) → Fam (Tagt D E zz)
                                         ST u x = (⟦⟧t {xx = νt {xx = zz} ((fib (H u))x)} ((fib (H u))x) P)
                                    in (
                                    (Σ[ u ∈ U ](ind (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x))) ,  (λ { (x , s) → (fib( ST u x)) s }))
                                    )))) ,
                                     (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {zz}
                                    (((Σ[ x ∈ (ind (H u)) ](ind (ST u x)) ) ) , (λ { (x , s) → fib (ST u x) s }))) x })))
                                    ≃⟨ IdEq ⟩
                                    ( ⋃ U  (λ u → Σ⟦⟧*t {D}{E} P {xx = (branch leaf zz)} (H u)))
                                    ∎ where open ≃-Reasoning
lemma-⋃-≃ {xx = (branch (branch xx yy) zz)} U H = {!!}
-}

{-Σ⟦⟧t {D}{E} X {xx} (U , T) = ⋃ {D = (Tagt D E xx)} U (λ u → (⟦⟧t {xx = νt {xx = xx} (T u)} (T u) X))-}

{-Σ⟦⟧t {D}{E} X {xx} (U , T) = ⋃ {D = (Tagt D E xx)} U (λ u → (⟦⟧t {xx = νt {xx = xx} (T u)} (T u) X))
-}


{-lemma-Σ⟦⟧*t {xx =  leaf} {yy}{P} c = refl{- begin
                                     (Σ⟦⟧*t {D}{E} P {{!!}} (⟦⟧t {D}{E = (DS't D E yy)} {xx} c P))
                                     ≡⟨ ⟩
                                     ⟦⟧t {D} {E} {{!!}} (MU {D}{E}{xx = branch xx yy} c) P-}
lemma-Σ⟦⟧*t {xx =  (branch aa bb)}{yy} c = {!!}-}

{-comment this out for the moment
lemma-Σ⟦⟧*t : {D E : Set₁}{xx : bt}{P : Fam D} → (c : ( DS't D E xx)) → (Σ⟦⟧*t {D}{E} P {xx} (⟦⟧t {D}{E} {xx} c P)) ≃ (⟦⟧t {D} {E} {xx = leaf} (MU {D}{E}{xx} c) P)
lemma-Σ⟦⟧*t {D}{E}{xx = leaf}{P} c = IdEq
lemma-Σ⟦⟧*t {D}{E}{xx = (branch leaf yy)}{P} c = begin
                (Σ⟦⟧*t {D}{E} P {(branch leaf yy)} (⟦⟧t {D}{E} {(branch leaf yy)} c P)) 
                {-≡⟨ refl ⟩
                (Σ⟦⟧*t {D}{E} P {(branch leaf yy)} (⟦ c ⟧' P) ) -- (⟦⟧t {D}{E} {(branch leaf yy)} c P) = (⟦ c ⟧' P)-}
                ≃⟨ IdEq ⟩
                (Σ⟦⟧*t {D}{E} P {yy}(Σ⟦⟧t {D}{E} P {yy} (⟦⟧t {D}{E} {(branch leaf yy)} c P)))
                ≃⟨ IdEq ⟩
                (Σ⟦⟧*t {D}{E} P {yy} ( ⋃ (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) (λ u → (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P))))
                ≃⟨ lemma-⋃-≃ {D}{E} {yy} {P} (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) (λ u → (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P)) ⟩
                 ( ⋃ (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P))
                 (λ u → (Σ⟦⟧*t {D}{E} P {yy} (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P))))
                 ≃⟨ IdEq ⟩
                 ((Σ[ u ∈ (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) ] ind (Σ⟦⟧*t {D}{E} P {yy} (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P))) ,
                 (λ { (u , x) → fib (Σ⟦⟧*t {D}{E} P {yy} (⟦⟧t {xx = νt {xx = yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u) P)) x }))
                 ≃⟨{! (Fam-≃ (λ {(u , x) → (u , ((ind _≃_.left) (lemma-Σ⟦⟧*t {D}{E}{xx = yy}{P} c) ) x)}) ? {!!} {!!} {!!})!} ⟩ -- "c" = ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)
                 ((Σ[ u ∈ (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) ] ind (⟦⟧t {D} {E} {xx = leaf} (MU {D}{E}{yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)) P)) ,
                 (λ { (u , x) → fib (⟦⟧t {D} {E} {xx = leaf} (MU {D}{E}{yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)) P) x }))
                 ≃⟨ IdEq ⟩
                 ( ⋃ (ind (⟦⟧t {D}{E} {(branch leaf yy)} c P))
                 (λ u → (⟦⟧t {D} {E} {xx = leaf} (MU {D}{E}{yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)) P)))
                {- ≃⟨ IdEq ⟩
                 (Σ⟦⟧t {D}{E} P {leaf} ((ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) , (λ u → (MU {D}{E}{yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u)))))
                  ≃⟨ IdEq ⟩
                 (Σ⟦⟧t {D}{E} P {leaf} (Fam-map ((MU {D}{E}{yy})(⟦⟧t {D}{E} {(branch leaf yy)} c P))))
                ≃⟨ {!!} ⟩  -- show semantics commutes with multiplication or Σ⟦⟧*t commtes with ⋃
                {! (Fam-map (μ-Tagt (Tagt→ D {E = DS't D E yy} {E' = DS't D E leaf} MU {D}{E}{yy}))(⟦⟧t {D} {E = DS't D E yy} {leaf} c))!}-}
                ≃⟨ {!!} ⟩
                (⟦⟧t {D} {E} {xx = leaf} (μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} )) c ))  P)
                ≃⟨ IdEq ⟩
                (⟦⟧t {D} {E} {xx = leaf} (MU {D}{E}{(branch leaf yy)} c) P)
                ∎ where open ≃-Reasoning
lemma-Σ⟦⟧*t {D}{E}{xx = (branch (branch aa bb ) yy)}{P} c = {!!}
-}
{-
MU : {D E : Set1} → {xx : bt} → DS't D E xx → DS't D E leaf
MU {xx = leaf} x = x
MU {D}{E}{xx = (branch leaf yy)} x = (μₒ{D}{E} ( (DS't→ D {E = (DS't D E yy)} {E' = (DS't D E leaf) } {xx = leaf}(MU {D}{E} {xx = yy} ))x ))
MU {D} {E} {xx = (branch (branch aa bb) yy)} x = MU {D}{E} {xx = (branch aa leaf)} ( DS't→ D {E = (DS't D (DS't D E yy ) bb)}{E' = DS't D E leaf}{xx = aa} (MU {D} {E} { xx = (branch bb yy) } )  x)
-}

{-
Σ⟦⟧*t : {D E : Set₁} → (Fam D) → {xx : bt} → Fam (Tagt D E xx) → Fam E
Σ⟦⟧*t {D}{E} X {xx = leaf} Y = Y --include unary sum 
Σ⟦⟧*t {D}{E} X {xx = (branch leaf zz)} Y = Σ⟦⟧*t {D}{E} X {xx = zz}  (Σ⟦⟧t {D}{E} X {xx = zz} Y)
Σ⟦⟧*t {D}{E} X {xx = branch (branch leaf qq) yy } Y = Σ⟦⟧*t {D}{E} X {xx = (branch qq yy)}(Σ⟦⟧t {D}{E} X {xx = (branch qq yy)} Y)
Σ⟦⟧*t {D}{E} X {xx = branch (branch (branch aa bb) qq) yy } Y = Σ⟦⟧*t {D}{E} X {xx = branch qq yy} (Σ⟦⟧t {D}{E} X {xx = branch qq yy}( Σ⟦⟧*t {D}{E = DS't D E (branch qq yy)} X {xx = (branch aa bb)} Y))

-}

{-
Σ⟦⟧t : {D E : Set₁} → (Fam D) → {xx : bt} → Fam (DS't D E xx) → Fam (Tagt D E xx)
Σ⟦⟧t {D}{E} X {xx} (U , T) = ⋃ {D = (Tagt D E xx)} U (λ u → (⟦⟧t {xx = νt {xx = xx} (T u)} (T u) X))
-}

{- ≃⟨ IdEq ⟩
                 (Σ⟦⟧*t {D}{E} P {branch leaf leaf} ((ind (⟦⟧t {D}{E} {(branch leaf yy)} c P)) , (λ u → (MU {D}{E}{yy} ((fib (⟦⟧t {D}{E} {(branch leaf yy)} c P)) u))))) -}

{- ≃⟨ {!!} ⟩
                (⋃ (ind (Σ⟦⟧*t {D}{E} P {(branch leaf yy)} (⟦⟧t {D}{E} {(branch leaf yy)} c P)  ))
                (λ u → (⟦⟧t {xx = νt {xx = (branch leaf yy)} ((fib (Σ⟦⟧*t {D}{E} P {(branch leaf yy)} (⟦⟧t {D}{E} {yy} c P)  )) u)} ((fib (Σ⟦⟧*t {D}{E} P {yy} (⟦⟧t {D}{E} {(branch leaf yy)} c P)  )) u) P) ))
                ≃⟨ ? ⟩
                (⋃ (ind (Σ⟦⟧*t {D}{E} P {yy} (⟦ c ⟧' P)  )) (λ u → (⟦⟧t {xx = νt {xx = yy} ((fib (Σ⟦⟧*t {D}{E} P {yy} (⟦ c ⟧' P)  )) u)} ((fib (Σ⟦⟧*t {D}{E} P {yy} (⟦ c ⟧' P)  )) u) P) ))-}



{-
lemma-Σ⟦⟧t : {D E : Set₁}{xx yy : bt}{P : Fam D} → (c : ( DS't D (DS't D E yy) xx)) → (Σ⟦⟧t {D}{E} P {{!!}} (⟦⟧t {D}{E = (DS't D E yy)} {xx = xx} c P)) ≡ ⟦⟧t {D} {E} {{!!}} (μDS't {D}{E}{xx}{yy} c) P
lemma-Σ⟦⟧t {P} c = refl
-}
{-
μDS't : {D E : Set₁}{xx yy : bt} → ( DS't D (DS't D E yy) xx) → DS't D E yy
μDS't {xx = leaf}{yy} = μ
μDS't {xx = branch aa bb}{yy} = ?
-}

{-this doesn't work since for xx = leaf, (⟦⟧t {D}{E} {xx} c P) : Fam E
lemma-Σ⟦⟧*t :  {D E : Set₁}{xx : bt}{P : Fam D} → (c : ( DS't D E xx)) → (Σ⟦⟧*t {D}{E} P {{!!}} (⟦⟧t {D}{E} {xx} c P)) ≡ ⟦⟧t {D} {E} {{!!}} (MU {D}{E}{xx = branch xx yy} c) P
-}




-- (νT (⟦⟧t {xx = xx} c P))

{- {P = (U , T)}(bt-bs X , f) = refl
lemma-Σ⟦⟧t {P = (U , T)}(bt-it xx yy R h , f) = refl
-}


{-
μ0 : {D E : Set1} -> (c : SP D) -> (f : Arg c -> DS' D E) -> SP D
μ0 (ι' X) f = σδ' (X , (proj₁ ∘ f))
μ0 (σδ' (Q , h)) f =  σδ' (Q , (λ x → μ0 (h x) (λ y → f (x , y))))

μ1 : ∀ {D E} c f -> Arg (μ0 {D} {E} c f) -> E
μ1 (ι' X) f (x , y) = proj₂ (f x) y
μ1 (σδ' (Q , h)) f (x , y) = μ1 (h x) (f ∘ (λ z → x , z)) y

μt : {D E : Set1} -> DS' D (DS' D E) -> DS' D E
μt (c , α) = (μ0 c α , μ1 c α)
-}

{-
lemma-Σ⟦⟧t : {D E : Set₁}{xx yy : t}{P : Fam D} → (c : ( DS't D (DS't D E yy) xx)) → (Σ⟦⟧t P (⟦⟧t c P)) ≡ ⟦⟧t (μ't c) P
lemma-Σ⟦⟧t {P = (U , T)}(bt-bs X , f) = refl
lemma-Σ⟦⟧t {P = (U , T)}(bt-it xx yy R h , f) = refl
-}


{-lemma-Σ⟦⟧ : {D E : Set₁}{P : Fam D} → (c : ( Levt D (Levt D E yy) xx)) → ((Σ⟦⟧ P (⟦ c ⟧L P)) ≡ -⟦ (-μIR c) ⟧L P)
lemma-Σ⟦⟧ {P = (U , T)} (-eta X , f) = begin
                                       ⋃ (cont X U) (λ x → -⟦ f (((cont-map X) T) x) ⟧ (U , T))
                                       ≡⟨ refl ⟩
                                       -⟦ (-μIR (-eta X , f)) ⟧ (U , T)
                                       ∎ where open ≡-Reasoning
lemma-Σ⟦⟧ {P = (U , T)} (-mu R h , f) = refl
-}
-- ⟦_⟧'t : {D E : Set₁}{xx : bt} → DS't D E xx → Fam D → Fam E

{-
data SP (D : Set₁) : Set₁ where
  ι'  : (X : Cont{lzero}{lzero}) → SP D
  σδ' : LCFam D (SP D) → SP D

Arg : {D : Set₁} → (SP D) → Set₁
Arg {D = D} (ι' X) = cont X D
Arg {D = D} (σδ' (Q , h)) = Σ[ x ∈ cont Q D ] (Arg (h x))

DS' : (D E : Set₁) → Set₁
DS' D E = Σ[ Q ∈ SP D ] (Arg Q -> E)
-}
-------------------------------------------------------------------------------
{- Therorem
The purpose of the translation 𝒹𝒹 is that it can be used to
translate its values further to DS codes in a way preserving
semantics. This can happen by defining a map

(bt , Cod𝒹𝒹 D E) -> DS D E

that (dependently) factors though

μDS^n : (DS D (DS D (....DS D E)...)) → DS D E

the other factor is an equivalence since Levt D E xx (of which Cod𝒹𝒹
is an iteration of) is equivalent to DS D E (via the two level version
DS' D E of DS).
-}
{- As explanation of the name 'levitation' we mention that the
   constructor base-lev of the following definition of the full
   hierarchy of systems LE D E n generated by 'levitation' functions
   as a cummulativity map: the systems LE D E n are all strucurally
   the same except for their differing degrees n. The above system
   Levt D E is the truncation of LE D E to degree n=1 with an
   annotation added for the purpose needed there. One can show (see below) that
   each system LE D E (suc n) contains a code for the system LE D E n.-}

mutual
  data LECont (D : Set₁): ℕ → Set₁ where
    base : (n : ℕ ) → Cont {lzero}{lzero} -> LECont D n
    base-lev : (n : ℕ ) → LECont D n -> LECont D (suc n)
    iter-lev : (n : ℕ) → (R : LECont D n) ->  (LEPos R -> LECont D n) -> LECont D n

  LEPos : {D : Set₁} {n : ℕ} → LECont D n → Set₁
  LEPos {D = D} (base n R) = (cont R) D
  LEPos {D = D} {n = (suc n)}(base-lev .n R) =  LEPos {D = D} {n = n} R
  LEPos (iter-lev n Q h) =  Σ (LEPos Q) (λ x → LEPos (h x))

ContLE : {D : Set₁} → (n : ℕ) -> Cont
ContLE {D} n = (LECont D n , LEPos)

LE : ∀ {a} → Set₁ -> Set a → (n : ℕ) -> Set (a ⊔ lsuc (lzero))
LE D E n = (cont (ContLE {D} n)) E


-- A code for LE D E (suc n) in LE D E n

-- assume names for the constructors of level n
{-
data ConstrNames : ℕ → Set where
  base- : ( n : ℕ) → ConstrNames n
  base-lev- : ( n : ℕ) → ConstrNames n
  iter-lev- : ( n : ℕ) → ConstrNames n
-}

{-
data ConstrNames : Set where
  base-n  base-lev-n iter-lev-n : ConstrNames

data U : Set where

El : U -> Set
El ()

mk-cont : Set → Cont {lzero}{lzero}
mk-cont A = (A , λ a  →  ⊥)

-- (CodeOfLECont-n , PosOfLE-n) is a code for LECont D n (doesn't depend on D)

module _ {n : ℕ} where

  CodeOfLECont-n : LECont Set (suc n) -- not Set1, because we are starting at U
  CodeOfLECont-n = iter-lev (suc n) (base (suc n) (mk-cont (ConstrNames)))
                  (λ { (base-n , *) → (base (suc n) (mk-cont ⊤)) ;
                       (base-lev-n , *) → base-lev n (base n (mk-cont U) ) ;
                       (iter-lev-n , *) → iter-lev (suc n) (base (suc n) (mk-cont U)) (λ X → base (suc n) (mk-cont U))})
                       
  PosOfLE-n : (D : Set) -> LEPos CodeOfLECont-n -> Set
  PosOfLE-n D ((base-n , *)  , y) = D
  PosOfLE-n D ((base-lev-n , *)  , A) = cont (mk-cont U) D
  PosOfLE-n D (( iter-lev-n , *) , (X , Y)) = Σ[ x ∈ X ] (Y x)

-}

{-

-- choice of constructors
data Tags : Set where
  idi' con' sigma' pi' : Tags

-- a (fake, doesn't matter) universe below Set₀
data U : Set where

El : U -> Set
El ()

-- code for Irish D (doesn't depend on D)
irishDesc : Irish Set -- not Set1, because we are starting at U
irishDesc = sigma (con Tags)
                  (λ { (lift idi') → con ⊤ ;
                       (lift con') → con U ;
                       (lift sigma') → sigma idi (λ InfoS → pi InfoS (λ _ → idi)) ;
                       (lift pi') → sigma (con U) (λ A → pi (El (lower A)) (λ _ → idi)) })

infoDesc : (D : Set) -> Info irishDesc -> Set
infoDesc D (lift idi'   , y) = D
infoDesc D (lift con'   , lift A) = Lift (El A)
infoDesc D (lift sigma' , (InfoS , InfoF)) = Σ[ x ∈ InfoS ] (InfoF x)
infoDesc D (lift pi'    , (lift A , InfoF)) = (x : El A) -> InfoF x

-}


-- cuts

{-
Cod♯Cont : (D : Set₁) → SP D → Set₁
Cod♯Cont D (ι' X) = LevtC D leaf
Cod♯Cont D (σδ'(X , h)) = LevtC D (branch leaf leaf)

mutual
  ♯Cont-bs : {D : Set₁} → Cont → LevtC D leaf
  ♯Cont-bs  X = bt-bs X
  
  ♯Cont-it : {D : Set₁} → ( Q : LCFam D (SP D) → LevtC D (branch leaf leaf)
  ♯Cont-it (X , h) = bt-it leaf leaf (bt-bs X)(♯Cont ∘ h ∘ ♭Pos)

  ♯Cont : {D : Set₁} → (Q : SP D) → Cod♯Cont D Q
  ♯Cont (ι' X) = ♯Cont-bs X
  ♯Cont ( σδ' (X , h)) = ♯Cont-it (X , h)

  ♭Cont-bs : {D : Set₁} → LevtC D xx → SP D
  ♭Cont-bs (bt-bs X) = ι' X

  ♭Cont-it : {D : Set₁} → {xx : bt} → LevtC D (branch zero xx) → SP D
  ♭Cont-it (bt-it (bt-bs X) h) = σδ' (X , ♭Cont ∘ h)

  RightNest : {D : Set₁} → (xx : bt) → Set₁
  RightNest D xx = LevtC D leaf
  RightNest D xx = LevtC D (branch zero leaf)
  
  ♭Cont : {D : Set₁} → {xx : bt} → RightNest D xx → SP D
  ♭Cont
  
  Cod♯Pos : {D : Set₁} → (Q : SP D) → Set₁
  Cod♯Pos (ι' X) = LevtP (♯Cont (ι' X))
  Cod♯Pos (σδ'(X , h)) = LevtP (♯Cont (σδ'(X , h)))
   
  ♯Pos : {D : Set₁} → {Q : SP D} → Arg Q →  Cod♯Pos Q
  --♯Pos w = w
  ♯Pos {Q = ι' X} w = w
  ♯Pos {Q = σδ'(X , h)} w = w
  
  ♭Pos : {D : Set₁} → {X : Cont{lzero}{lzero}} → LevtP (bt-bs X)  → Arg (ι' X)
  ♭Pos x = x 
-}
