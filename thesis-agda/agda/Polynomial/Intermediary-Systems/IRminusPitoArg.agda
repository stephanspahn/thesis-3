module Polynomial.Intermediary-Systems.IRminusPitoArg where

open import Prelude.Basic
open import Prelude.Container
open import Prelude.Fam
open import Prelude.Equality
open import DS.Arg
open import Polynomial.Container.IR
open import Polynomial.IR
--import Polynomial.Container.IR as Cont
open import Polynomial.Intermediary-Systems.IRminusPi

{- Try to translate IRminusPi to Arg where Arg is here not
Dybjer-Setzer's (SP,Arg) but has the container base case.

Notice that the equivalence of Polynomial codes and Container codes
does not restrict to those systems without pi since pi is used to
separate the eta base case -}

♭♭Cont :  {D : Set₁} → -IRCont D → Cont{lzero}{lzero}
♭♭Cont (-eta X) = X
♭♭Cont (-mu R f) = ♭♭Cont R

{-mutual

  ♭-Cont : {D : Set₁} -> -IRCont D -> SP D
  ♭-Cont (-eta X) = ι X
  ♭-Cont (-mu R f) = σδ (♭♭Cont R) (λ z → ♭-Cont (f (♭Pos R z)))-}

  
