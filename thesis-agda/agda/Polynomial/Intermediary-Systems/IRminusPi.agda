module Polynomial.Intermediary-Systems.IRminusPi where

open import Prelude.Basic
open import Prelude.Container
open import Prelude.Fam
open import Prelude.Equality
--open import DS.IR
open import DS.SigmaDelta
open import DS.Arg
open import Polynomial.Container.IR

{-This section is of rather scholastic interest. We are going to show that a complicated system of codes we have no intrinsic interest in is equivalent to an easier system we already know. What we nevertheless can learn from it is that we can use the fact that monad multiplication has a retraction to invert the related constructor for mu.-}

-- Irish codes without pi

mutual 
   data -IRCont (D : Set₁) : Set₁ where 
        -eta : Cont {lzero}{lzero} -> -IRCont D
        -mu  : (R : -IRCont D) ->  (-IRPos R -> -IRCont D) -> -IRCont D
 
   -IRPos : {D : Set₁} -> -IRCont D -> Set₁
   -IRPos {D} (-eta R)  = (cont R) D
   -IRPos     (-mu R f) = Σ (-IRPos R) (λ x → -IRPos (f x))

-ContIR : {D : Set₁} -> Cont
-ContIR {D} = (-IRCont D , -IRPos)

-IR : ∀ {a} → Set₁ -> Set a -> Set (a ⊔ lsuc (lzero))
-IR D E = (cont (-ContIR {D})) E

{-
-- a uniformly graded version

{-this is a simpler version and one should be able to reduce the
degree in this special case since now all path legth are equal which
was the obstacle in case of the other systems-}

mutual 
   data LeviCν (D : Set₁) : ℕ → Set₁ where
        bs : Cont {lzero}{lzero} -> LeviCν D zero
        it  : {n m : ℕ} -> (R : LeviCν D n) ->  (LeviPν R -> LeviCν D m) -> LeviCν D (suc n + m)
 
   LeviPν : {D : Set₁} → {n : ℕ} -> LeviCν D n -> Set₁
   LeviPν {D} (bs R)  = (cont R) D
   LeviPν     (it R f) = Σ (LeviPν R) (λ x → LeviPν (f x))

ContLevν : {D : Set₁} → {n : ℕ} -> Cont
ContLevν {D}{n = n} = (LeviCν D n , LeviPν)

--Leviν : ∀ {a} → Set₁ -> Set a → {n : ℕ} -> Set (a ⊔ lsuc (lzero))
Leviν : Set₁ -> Set₁ → (n : ℕ) -> Set₁
Leviν D E n = (cont (ContLevν {D} {n = n})) E
-}

-- monadicity

-con : {D : Set₁} → Set → -IRCont D
-con A = -eta (A , λ a → ⊥) 

-ηIR : ∀ {a} → {D : Set₁} → {E : Set a} -> E -> -IR D E 
-ηIR e = (-con ⊤ , const e)

-μIR : ∀ {a} → {D : Set₁} -> {E : Set a} → -IR D (-IR D E) → -IR D E
-μIR {a} {D} {E} (R , f) = (Q , α)
                          where Q : -IRCont D  
                                Q = -mu R (proj₁ ∘ f) 
                                α : -IRPos Q → E  
                                α (x , q) = proj₂ (f x) q

-- decoding

mutual
  -ix : {D : Set₁} -> (X : -IRCont D) -> Fam D -> Set
  -ix (-eta (S , P)) (U , T) = (S ◃ P) U
  -ix (-mu R f) P = Σ[ x ∈ -ix R P ] (-ix (f (-dx R P x)) P)

  -dx : {D : Set₁} -> (X : -IRCont D) -> (P : Fam D) -> -ix X P -> -IRPos X
  -dx (-eta (S , P)) (U , T) = ◃-map S P T
  -dx (-mu R f) P (x , p) = -dx R P x , -dx (f (-dx R P x)) P p

-⟦_⟧ : {D E : Set₁} -> -IR D E -> Fam D -> Fam E
-⟦ X , f ⟧ P = Fam-map f (-ix X P , -dx X P)

-⟦_⟧₀ : {D E : Set₁} -> -IR D E -> Fam D -> Set
-⟦ c ⟧₀ X = ind (-⟦ c ⟧ X)

-⟦_⟧₁ : {D E : Set₁} -> ( c : -IR D E) -> (X : Fam D) -> -⟦ c ⟧₀ X → E
-⟦ c ⟧₁ X = fib (-⟦ c ⟧ X)

-- -IR is a subsystem of IR in the following sense

mutual

  ♯Cont : {D : Set₁} -> -IRCont D -> IRCont D
  ♯Cont (-eta X) = eta X
  ♯Cont (-mu R f) = mu (♯Cont R) (λ z → ♯Cont (f (♭Pos R z)))

  ♯Pos : {D : Set₁} -> (Q : -IRCont D) -> (-IRPos Q) -> IRPos (♯Cont Q)
  ♯Pos (-eta K) x = x
  ♯Pos (-mu R f) (x , z) = (♯Pos R x , subst (λ y → IRPos (♯Cont (f y))) (♭♯Pos R x) (♯Pos (f x) z))
  
 -- ♭Cont : {D : Set₁} -> IRCont D -> -IRCont D
 -- ♭Cont (eta X) = -eta X
 -- ♭Cont (mu R f) = -mu (♭Cont R) (♭Cont ∘ f ∘ ♯Pos)

  ♭Pos : {D : Set₁} -> (Q : -IRCont D) -> IRPos (♯Cont Q) -> -IRPos Q
  ♭Pos (-eta K) x = x
  ♭Pos (-mu R f) (x , z) = (♭Pos R x , (♭Pos (f (♭Pos R x))) z)

  ♭♯Pos : {D : Set₁} -> (Q : -IRCont D) -> (w : -IRPos Q) -> (w ≡ ((♭Pos Q) ((♯Pos Q) w)))
  ♭♯Pos (-eta K) w = refl
  ♭♯Pos (-mu R f) (x , z) = Σ-≡ (♭♯Pos R x) (q)
        where q = begin
                  subst (λ x → -IRPos (f x)) (♭♯Pos R x) z
                  ≡⟨ subst→≡ f -IRPos (♭♯Pos R x) {z = z} {(♭Pos (f x) (♯Pos (f x) z))} (♭♯Pos (f x) z)  ⟩
                  subst -IRPos (cong f (♭♯Pos R x)) (♭Pos (f x) (♯Pos (f x) z))
                  ≡⟨ cong₂dd (♭Pos) (cong f (♭♯Pos R x)) (sym (subst-cong {B = IRPos ∘ ♯Cont} {f} (♭♯Pos R x))) ⟩
                  ♭Pos (f (♭Pos R (♯Pos R x))) (subst (λ y → IRPos (♯Cont (f y))) (♭♯Pos R x) (♯Pos (f x) z))
                  ∎ where open ≡-Reasoning

-IRtoIR : {D E : Set₁} -> -IR D E -> IR D E
-IRtoIR (Q , h) = (♯Cont Q , h ∘ ♭Pos Q)

{- subsequently we will give a translation from -IR to DS
  more precisely we will give a map
       red : (c : -IR D E) → DS^(ν c) D E
where  DS^n D E is the n-th iteration DS D (DS D(...DS DE)...)
-}

-- the basic idea is to reduce -mu nestings until we end in an eta which easily can be translated into a DS code
-- the essence of this idea is given by the following map producing codes from IRCont and IRPos ingredients

-Δ : {D E : Set1} -> {R : -IRCont D} -> (h : -IRPos R -> -IRCont D) -> (x : -IRPos R) -> (f : (-IRPos (-mu R h)) -> E) -> -IR D E
-Δ h x f = (h x , λ q -> f (x , q))

-- 𝒹 is the action on codes induced by Δ

-𝒹 : {D E : Set1} → -IR D E -> -IR D (-IR D E)
-𝒹 (-eta X , f) = (-eta X , -ηIR ∘ f)
-𝒹 (-mu R h , f) = (R , λ x → -Δ {R = R} h x f)

-- Properties of -𝒹

-- -𝒹 is a retraction of -μIR restricted to -eta codes
-- this lemma is not meant to hold for -eta

lemma-𝒹 : {D E : Set1} → (R : -IRCont D) → (h : -IRPos R → -IRCont D) → (f : (-IRPos (-mu R h)) → E) → -μIR (-𝒹 (-mu R h , f)) ≡ (-mu R h , f)
lemma-𝒹 R h f = begin
                         -μIR (R , λ x → -Δ {R = R} h  x f)
                         ≡⟨ refl ⟩
                         ( -mu R (proj₁ ∘ ( λ x → -Δ {R = R} h  x f)) , λ {(x , q) → proj₂ ( -Δ {R = R} h x f) q} )
                         ≡⟨ refl ⟩
                         (-mu R h , f)
                         ∎ where open ≡-Reasoning


{- Collect some lemmas about how -𝒹 relates to decoding, multiplication, and unit

-IR D (IR D E) <-------> -IR D E
    I                     I
    I⟦-⟧UT                I⟦ ⟧UT
    I                     I
Fam (-IR D E) <-------> Fam (E)
-}


⋃Fam-IR : {D E : Set₁} → (Fam D) → Fam (-IR D E) → Fam E
⋃Fam-IR X (U , T) = ⋃ U (λ u → (-⟦ T u ⟧ X))

lemma-⟦⋃μ⟧ : {D E : Set₁}{P : Fam D} → (c : ( -IR D (-IR D E))) → ((⋃Fam-IR P (-⟦ c ⟧ P)) ≡ -⟦ (-μIR c) ⟧ P)
lemma-⟦⋃μ⟧ {P = (U , T)} (-eta X , f) = begin
                                       ⋃ (cont X U) (λ x → -⟦ f (((cont-map X) T) x) ⟧ (U , T))
                                       ≡⟨ refl ⟩
                                       -⟦ (-μIR (-eta X , f)) ⟧ (U , T)
                                       ∎ where open ≡-Reasoning
lemma-⟦⋃μ⟧  {P = (U , T)} (-mu R h , f) = refl

-- lemma-⟦⋃μ⟧{P = (U , T)} ≡ Fam-map ∘ (λ x → ⟦ ⟧(U , T))

-- this lemma holds only for -mu
-- lemma-𝒹 implies that lemma-⟦-d⟧' is a special case of lemma-⟦⋃μ⟧

lemma-⟦-d⟧' : {D E : Set₁} → (R : -IRCont D) → (h : -IRPos R → -IRCont D) → (f : (-IRPos (-mu R h)) → E) → {P : Fam D} → (⋃ ( -⟦ -𝒹 (-mu R h , f) ⟧₀ P )( λ x → -⟦ ( -⟦ -𝒹 (-mu R h , f) ⟧₁ P) x ⟧ P)) ≡ -⟦ (-mu R h , f) ⟧ P
lemma-⟦-d⟧' {D = D}{E = E} R h f {P = P}  = begin
                                                  ⋃ ( -⟦ -𝒹 (-mu R h , f) ⟧₀ P )( λ x → -⟦ ( -⟦ -𝒹 (-mu R h , f) ⟧₁ P) x ⟧ P)
                                                  ≡⟨ refl ⟩
                                                  ⋃ ( -⟦ (R , λ x → -Δ {R = R} h x f) ⟧₀ P )( λ x → -⟦ ( -⟦ (R , λ y → -Δ {R = R} h y f) ⟧₁ P) x ⟧ P)
                                                  ≡⟨ refl ⟩
                                                  ⋃ (-ix R P )( λ x → ( -⟦ -Δ {R = R} h (-dx R P x) f ⟧ P ))
                                                  ≡⟨ refl ⟩
                                                  ⋃ (-ix R P)(λ x → ( -⟦ ( h (-dx R P x) , ( λ q → f ((-dx R P x) , q))) ⟧ P ))
                                                  ≡⟨ refl ⟩
                                                  ⋃ (-ix R P)(λ x → ( -ix (h (-dx R P x)) P ,  λ p → f ((-dx R P x) , -dx (h (-dx R P x)) P p)))
                                                  ≡⟨ refl ⟩
                                                  (( Σ[ x ∈ (-ix R P) ]( -ix (h (-dx R P x )) P )) ,  (λ { (x , p) →( f ((-dx R P x) , -dx (h (-dx R P x)) P p)) }) )
                                                  ≡⟨ refl ⟩
                                                  (-ix (-mu R h) P , f ∘ (-dx (-mu R h) P) )
                                                  ≡⟨ refl ⟩
                                                  -⟦ (-mu R h , f) ⟧ P
                                                  ∎ where open ≡-Reasoning


-- this lemma holds only for -eta
lemma-⟦-d⟧ : {D E : Set₁} → (X : Cont{lzero}{lzero}) → (f : (cont X D) → E) → {P : Fam D} →  -⟦  -𝒹 (-eta X , f) ⟧ P ≡ (Fam-map (-ηIR)) (-⟦ (-eta X , f) ⟧ P)
lemma-⟦-d⟧ X f { P = (U , T)} = begin
                          -⟦ -eta X , -ηIR ∘ f ⟧ (U , T)
                          ≡⟨ refl ⟩
                          (-ix (-eta X)  (U , T) , -ηIR ∘ f ∘ -dx (-eta X) (U , T))
                          ≡⟨ refl ⟩
                          (cont X U , -ηIR ∘ f ∘  cont-map X T )
                          ≡⟨ refl ⟩
                          (Fam-map (-ηIR))(cont X U , f ∘ cont-map X T)
                          ≡⟨ refl ⟩
                          (cont X U , -ηIR ∘ f ∘ cont-map X T)
                          ∎ where open ≡-Reasoning

open import Data.Nat

-- a function counting nestings of mu


νₒ : {D : Set1} → -IRCont D → ℕ
νₒ (-eta X) = ℕ.zero
νₒ (-mu R f0) = ℕ.suc (νₒ R)

ν : {D E : Set1} → (c : -IR D E) → ℕ
ν (Q , f) = νₒ Q

-- the definition of  IR^n D E


mutual

  -IRfromZero^ : (n : ℕ) → Set₁ -> Set₁ -> Set₁
  -IRfromZero^ zero D E = E
  -IRfromZero^ (suc n) D E = (-IR^ n D E)
  
  -IR^ : (n : ℕ) → Set₁ → Set₁ → Set₁
  -IR^ n D E = -IR D (-IRfromZero^ n D E)

-IRᴺ : (D : Set₁) → (E : Set₁) -> Set₁
-IRᴺ D E = Σ ℕ (λ n → -IR^ n D E)

-μIR^ : {D : Set₁} -> {E : Set₁} → (n : ℕ) → -IR^ (ℕ.suc n) D E → -IR^ n D E
-μIR^ zero K = -μIR K
-μIR^ (ℕ.suc n) K = -μIR K

-- iterating -𝒹


-𝒹^ : {D E : Set1}(n : ℕ) → -IR^ n  D E -> -IR^ (suc n) D E
-𝒹^ n (-eta X , f) = (-eta X , -ηIR ∘ f)
-𝒹^ n (-mu R f0 , f1) = (R , λ x0 → -Δ {R = R} f0 x0 f1)


-- relating (SP , Arg) to -IR

{- The constructors
 -eta : Cont {lzero}{lzero} -> -IRCont D
 -mu  : (R : -IRCont D) ->  (-IRPos R -> -IRCont D) -> -IRCont D

-IRPos     (-mu R f) = Σ (-IRPos R) (λ x → -IRPos (f x))

can be recognised as a way to nest polynomials on the level of codes: comparing to 

σδ : (Q : Cont)(cont Q D → SP D) → SP D

Arg {D = D} (σδ (Q , h)) = Σ[ x ∈ cont Q D ] (Arg (h x))

Above we have seen a way to un-nest -mu codes. This raises the question wheher we can translate -IR to (SP , Arg) by keeping track of the degrees of the nesting.

we define
-}

-- -IR codes annotated by the degree of their nesting

mutual
  data SP+ (D : Set₁) : (n : ℕ) → Set₁ where
    ι+  : (X : Cont{lzero}{lzero}) → SP+ D zero
    σδ+ : (n : ℕ)(Q : SP+ D n) → ((Arg+ n {D = D} Q) → (Σ[ n ∈ ℕ ](SP+ D n))) → (SP+ D (suc n))

  Arg+ : (n : ℕ) → {D : Set₁} → (SP+ D n) → Set₁
  Arg+ zero {D = D} (ι+ X) = cont X D
  Arg+ (suc n) {D = D} (σδ+ .n Q h) = Σ[ x ∈ Arg+ n {D = D} Q ] (Arg+ (proj₁ (h x)) {D = D} (proj₂ (h x)))

DS+ : (D E : Set₁)(n : ℕ) → Set₁
DS+ D E n = Σ[ Q ∈ SP+ D n ] (Arg+ n Q -> E)


Inι+ : {D E : Set₁} → E → DS+ D E zero
Inι+ e = (ι+ (⊤ , λ _ → ⊥ ) , λ _ → e)

mutual
  ♭Cont+ : {D : Set₁} → (n : ℕ) → SP+ D n → -IRCont D
  ♭Cont+ zero (ι+ X) = -eta X
  ♭Cont+ (suc n) (σδ+ .n Q h) = -mu (♭Cont+ n Q)(λ z → (( ♭Cont+ (proj₁ (( h  (♯Pos+ n Q z)))) (proj₂ (( h  (♯Pos+ n Q z)))))))
  
  ♯Pos+ : {D : Set₁} → (n : ℕ) → (Q : SP+ D n) → (-IRPos (♭Cont+ n Q)) → Arg+ n {D = D} Q
  ♯Pos+ zero (ι+ X) x = x 
  ♯Pos+ (suc n) (σδ+ .n Q h) (z , q) = ( ♯Pos+ n Q z ,( ♯Pos+ (proj₁ ( h (♯Pos+ n Q z))) (proj₂ ( h (♯Pos+ n Q z)))) q)
  
DS+to-IR : {D E : Set₁} → (n : ℕ) → (DS+ D E n) → -IR D E
DS+to-IR zero (ι+ X , h) = (-eta X , h)
DS+to-IR (suc n)(σδ+ .n Q h , f) =  ((♭Cont+ (suc n)(σδ+ n Q h)) , f ∘ ♯Pos+ (suc n) (σδ+ n Q h))

DS+to-IR0 : {D : Set₁} → (n : ℕ) → (SP+ D n) → -IRCont D
DS+to-IR0 zero (ι+ X ) = (-eta X)
DS+to-IR0 (suc n)(σδ+ .n Q h) = (♭Cont+ (suc n)(σδ+ n Q h))

mutual
  ♯Cont+ : {D : Set₁}(c : -IRCont D) → SP+ D (νₒ c)
  ♯Cont+ (-eta X) = ι+ X
  --♯Cont+ (-mu Q h) = σδ+ (νₒ Q)(♯Cont+ Q) ((λ z → (νₒ z , ♯Cont+ z)) ∘ h ∘ ♭Pos+ Q)
  ♯Cont+ (-mu Q h) = σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))

  ♭Pos+ : {D : Set₁} → (Q : -IRCont D) → (Arg+ (νₒ Q) {D = D} (♯Cont+ Q)) → -IRPos Q
  ♭Pos+ (-eta X) x = x
  ♭Pos+ (-mu Q h) (z , q) = (♭Pos+ Q z , (♭Pos+ (h (♭Pos+ Q z)))q)

-IRtoDS+ : {D E : Set₁} → (c : -IR D E) → DS+ D E (ν c)
-IRtoDS+ (-eta Q , f) = (ι+ Q , f)
-IRtoDS+ (-mu Q h , f) = (♯Cont+ (-mu Q h) , f ∘ (♭Pos+  (-mu Q h)))

-IRtoDS+0 : {D : Set₁} → (Q : -IRCont D) → SP+ D (νₒ Q)
-IRtoDS+0 (-eta Q) = (ι+ Q)
-IRtoDS+0 (-mu Q h) = (♯Cont+ (-mu Q h))

mutual
  ♭♯Cont+ : {D : Set₁} → (Q : -IRCont D) → (♭Cont+  (νₒ Q) (♯Cont+ Q) ) ≡ Q
  ♭♯Cont+ (-eta X) = refl
  ♭♯Cont+ (-mu Q h) = begin
                      (♭Cont+ (νₒ (-mu Q h)) (σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))))
                      ≡⟨ refl ⟩
                      (-mu (♭Cont+ (νₒ Q) (♯Cont+ Q))(λ z → ( ♭Cont+ (νₒ(h((♭Pos+ Q)(♯Pos+ (νₒ Q) (♯Cont+ Q) z))))(♯Cont+ (h((♭Pos+ Q)(♯Pos+ (νₒ Q)(♯Cont+ Q) z)))))))
                      ≡⟨ cong (-mu (♭Cont+ (νₒ Q) (♯Cont+ Q))) (ext (λ z → ♭♯Cont+ (h((♭Pos+ Q)(♯Pos+ (νₒ Q) (♯Cont+ Q) z))) )) ⟩
                      (-mu (♭Cont+ (νₒ Q) (♯Cont+ Q))(λ z → (h((♭Pos+ Q)(♯Pos+ (νₒ Q)(♯Cont+ Q) z)))))
                      ≡⟨ cong₂d -mu (♭♯Cont+ Q) (♭♯Pos+ Q h) ⟩
                      (-mu Q h)
                      ∎ where open ≡-Reasoning

  ♭♯Pos+ :  {D : Set₁} → (Q : -IRCont D) → (h : -IRPos Q → -IRCont D) → --(z : (-IRPos (♭Cont+ (νₒ  Q) (♯Cont+ Q)))) →
                            (subst (λ R → (-IRPos R → -IRCont D)) (♭♯Cont+ Q)  (λ z → (h((♭Pos+ Q)(♯Pos+ (νₒ Q)(♯Cont+ Q) z))))) ≡ h --since ♭Cont+ (νₒ Q) (♯Cont+ Q) != Q...  z has type -IRPos Q
  ♭♯Pos+ (-eta X) h = refl
  ♭♯Pos+ {D = D} (-mu Q h) g = begin
                               (subst (λ O → (-IRPos O → -IRCont D)) (♭♯Cont+ (-mu Q h))  (λ z → (g((♭Pos+ (-mu Q h))(♯Pos+ (νₒ (-mu Q h))(♯Cont+ (-mu Q h)) z)))))
                               ≡⟨ {!(cong₂d (subst (λ O → (-IRPos O → -IRCont D))) {!!} {!!})!} ⟩
                               {!((subst (λ O → (-IRPos O → -IRCont D))
                               (trans (cong (-mu (♭Cont+ (νₒ Q)
                               (♯Cont+ Q))) (ext (λ z → ♭♯Cont+
                               (h((♭Pos+ Q)(♯Pos+ (νₒ Q) (♯Cont+ Q)
                               z))) )))( cong₂d -mu (♭♯Cont+ Q)
                               (♭♯Pos+ Q h)))(λ z → (g(((♭Pos+ Q
                               (proj₁ (♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ
                               Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) ,
                               ♯Cont+ (h((♭Pos+ Q)q))))) z)) , (♭Pos+
                               (h (♭Pos+ Q (proj₁ (♯Pos+ (νₒ (-mu Q
                               h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q →
                               (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+
                               Q)q))))) z)))))(proj₂ (♯Pos+ (νₒ (-mu Q
                               h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q →
                               (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+
                               Q)q))))) z)))))))))!}
                               ≡⟨ {!!} ⟩
                               g
                               ∎ where open ≡-Reasoning

--(subst (λ O → (-IRPos O → -IRCont D)) (♭♯Cont+ (-mu Q h))  (λ z → (g((♭Pos+ (-mu Q h))(♯Pos+ (νₒ (-mu Q h))(♯Cont+ (-mu Q h)) z)))))

{-
((subst (λ O → (-IRPos O → -IRCont D))
                               (trans (cong (-mu (♭Cont+ (νₒ Q)
                               (♯Cont+ Q))) (ext (λ z → ♭♯Cont+
                               (h((♭Pos+ Q)(♯Pos+ (νₒ Q) (♯Cont+ Q)
                               z))) )))( cong₂d -mu (♭♯Cont+ Q)
                               (♭♯Pos+ Q h)))((λ z → (g((λ {(x , q) →
                               (♭Pos+ Q x , (♭Pos+ (h (♭Pos+ Q
                               x)))q)})(♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ
                               Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) ,
                               ♯Cont+ (h((♭Pos+ Q)q))))) z)))) )))
-}
                               
{-coposing the functions in the last line
(λ z → (g((λ {(x , q) → (♭Pos+ Q x , (♭Pos+ (h (♭Pos+ Q x)))q)})(♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z))))

(λ z → (g((λ {w → (♭Pos+ Q (proj₁ w) , (♭Pos+ (h (♭Pos+ Q (proj₁ w))))(proj₂ w))})(♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z))))

(λ z → (g(((♭Pos+ Q (proj₁ (♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z)) , (♭Pos+ (h (♭Pos+ Q (proj₁ (♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z)))))(proj₂ (♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z)))))))
-}

{-  
(λ z → (g((♭Pos+ (-mu Q h))(♯Pos+ (νₒ (-mu Q h))(♯Cont+ (-mu Q h)) z))))

(λ z → (g((λ {(x , q) → (♭Pos+ Q x , (♭Pos+ (h (♭Pos+ Q x)))q)})(♯Pos+ (νₒ (-mu Q h))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) z))))

♯Cont+ (-mu Q h) = (σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q)))))

♭Pos+ (-mu Q h) = (λ { (z , q) → (♭Pos+ Q z , (♭Pos+ (h (♭Pos+ Q z)))q)})
-}
{-subst B p u ≡ v
B = (λ R → (-IRPos R → -IRCont D))
u = (λ z → (h((♭Pos+ Q)(♯Pos+ (νₒ Q)(♯Cont+ Q) z))))
p = ♭♯Cont+ Q
v = h
-}
{-
n= (νₒ Q)
Q=(♯Cont+ Q)
h=(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))
 -}
 
♭♯Cont+-mu : {D : Set₁} → (Q : -IRCont D) → (h : ((-IRPos Q) → -IRCont D) ) → (♭Cont+(suc(νₒ Q))(σδ+ (νₒ Q) (♯Cont+ Q) (λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q)))))) ≡ (-mu Q h)
♭♯Cont+-mu (-eta X) h = begin
                   (♭Cont+(suc zero)(σδ+ zero (♯Cont+ (-eta X)) (λ q → (νₒ(h((♭Pos+ (-eta X))q)) , ♯Cont+ (h((♭Pos+ (-eta X))q))))))
                   ≡⟨ refl ⟩
                   ( -mu (♭Cont+ zero (♯Cont+ (-eta X)))(λ z → ((♭Cont+ (νₒ(h((♭Pos+ (-eta X))(♯Pos+ zero (♯Cont+ (-eta X)) z)))) (♯Cont+ (h((♭Pos+ (-eta X))    (♯Pos+ zero (♯Cont+ (-eta X)) z)       )))) )))
                   ≡⟨ refl ⟩
                   ( -mu (♭Cont+ zero (ι+ X))           (λ z → ((♭Cont+  (νₒ(h((♭Pos+ (-eta X))(♯Pos+ zero ( (ι+ X)) z))))        (♯Cont+ (h((♭Pos+ (-eta X))    (♯Pos+ zero  (ι+ X) z)))) ))))
                   ≡⟨ refl ⟩
                   ( -mu (♭Cont+ zero (ι+ X))  (λ z → ((♭Cont+  (νₒ(h z)) (♯Cont+ (h z)) ))))
                   ≡⟨ refl ⟩
                   ( -mu (-eta X) (λ z → ((♭Cont+  (νₒ(h z)) (♯Cont+ (h z)) ))))
                   ≡⟨ cong( -mu (-eta X)) (ext(λ z → ♭♯Cont+ (h z))) ⟩
                   (-mu (-eta X) h)
                   ∎ where open ≡-Reasoning
♭♯Cont+-mu (-mu R g) h = {!!}

round-+0 : {D : Set₁} → (Q : -IRCont D) → DS+to-IR0(νₒ Q)(-IRtoDS+0 (Q)) ≡ Q
round-+0 (-eta Q) = refl
round-+0 (-mu Q h) = begin
                      DS+to-IR0(νₒ  (-mu Q h))(♯Cont+ (-mu Q h))
                      ≡⟨ refl ⟩
                      DS+to-IR0(νₒ  (-mu Q h)) ( σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q)))))
                      ≡⟨ refl ⟩
                      (♭Cont+(νₒ (-mu Q h))(σδ+ (νₒ Q) (♯Cont+ Q) (λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))))
                      ≡⟨ refl ⟩
                       (♭Cont+(suc(νₒ Q))(σδ+ (νₒ Q) (♯Cont+ Q) (λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))))
                      ≡⟨ ♭♯Cont+-mu Q h ⟩
                      (-mu Q h)
                     ∎ where open ≡-Reasoning

round-+ : {D E : Set₁} → (c : -IR D E) → DS+to-IR(ν c)(-IRtoDS+ (c)) ≡ c
round-+ (-eta Q , f) = refl
round-+ {D = D}{E = E}(-mu Q h , f) = begin
                         DS+to-IR(ν ((-mu Q h , f)))(♯Cont+ (-mu Q h) , f ∘ (♭Pos+  (-mu Q h)))
                         ≡⟨ refl ⟩
                         DS+to-IR(ν ((-mu Q h , f)))((σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q))))) , f ∘ (λ {(z , q) → (♭Pos+ Q z , (♭Pos+ (h (♭Pos+ Q z)))q)}))
                         ≡⟨ refl ⟩
                         (♭Cont+ (ν ((-mu Q h , f)))((σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q)))))) ,
                         (f ∘ (λ {(z , q) → (♭Pos+ Q z , (♭Pos+ (h (♭Pos+ Q z)))q)})) ∘ (♯Pos+ (ν ((-mu Q h , f)))(σδ+ (νₒ Q)(♯Cont+ Q)(λ q → (νₒ(h((♭Pos+ Q)q)) , ♯Cont+ (h((♭Pos+ Q)q)))))))
                         ≡⟨ refl ⟩
                        ( -mu (♭Cont+ (νₒ Q) (♯Cont+ Q))
                        (λ z → ((
                         ♭Cont+ (νₒ(h((♭Pos+ Q)(♯Pos+ (νₒ Q) (♯Cont+
                         Q) z)))) (♯Cont+ {D = D} (h((♭Pos+ {D = D}
                         Q)(♯Pos+ {D = D} (νₒ {D = D} Q) (♯Cont+ {D =
                         D} Q) z))))))) ,
                           (λ w → f ( (♭Pos+ Q (proj₁ ((♯Pos+
                                (ν ((-mu Q h , f)))(σδ+ (νₒ Q)(♯Cont+
                                Q)(λ r → (νₒ(h((♭Pos+ Q)r)) , ♯Cont+
                                (h((♭Pos+ Q)r)))))) w)) , (♭Pos+ (h
                                (♭Pos+ Q (proj₁ ((♯Pos+ (ν ((-mu Q h ,
                                f)))(σδ+ (νₒ Q)(♯Cont+ Q)(λ r →
                                (νₒ(h((♭Pos+ Q)r)) , ♯Cont+ (h((♭Pos+
                                Q)r)))))) w)))))(proj₂ ((♯Pos+ (ν ((-mu
                                Q h , f)))(σδ+ (νₒ Q)(♯Cont+ Q)(λ r →
                                (νₒ(h((♭Pos+ Q)r)) , ♯Cont+ (h((♭Pos+
                                Q)r)))))) w))) )))
                         ≡⟨ Σ-≡ (round-+0 (-mu Q h)) {!!}  ⟩
                         (-mu Q h , f)
                         ∎ where open ≡-Reasoning

checkdeg' : {D : Set₁} → (n : ℕ) → (Q : SP+ D n) → νₒ (♭Cont+ n Q) ≡ n
checkdeg' zero (ι+ X) = refl
checkdeg' (suc n) (σδ+ .n Q h) = begin
                                  νₒ (♭Cont+ (suc n) (σδ+ n Q h))
                                  ≡⟨ refl ⟩
                                  νₒ (-mu (♭Cont+ n Q)(λ z → (( ♭Cont+ (proj₁ (( h  (♯Pos+ n Q z)))) (proj₂ (( h  (♯Pos+ n Q z))))))))
                                  ≡⟨ refl ⟩
                                  suc (νₒ (♭Cont+ n Q))
                                  ≡⟨ cong suc (checkdeg' n Q) ⟩
                                  (suc n)
                                 ∎ where open ≡-Reasoning


checkdeg  : {D E : Set₁} → {n : ℕ} → ( c : DS+ D E (suc n)) → (ν ( -𝒹 (DS+to-IR  (suc n) c))) ≡ n
checkdeg (σδ+ n Q h , f ) = checkdeg' n Q

-- Translation DS+ to Dybjer-Setzer's (SP , Arg)

atzero+' : {D E : Set₁} → DS+ D E zero → DS' D E
atzero+' (ι+ X , f) = ι' X , f

𝒹+' : {D E : Set₁} → {n : ℕ} → ( c : DS+ D E (suc n)) → DS+ D (-IR D E) (ν ( -𝒹 (DS+to-IR  (suc n) c))) -- the intended codomain is  DS+ D (-IR D E) n
𝒹+' {n = n} c = -IRtoDS+ ( -𝒹 (DS+to-IR  (suc n) c))

𝒹+'' : {D E : Set₁} → {n : ℕ} → ( c : DS+ D E (suc n)) → DS+ D (-IR D E) n
𝒹+'' {D = D}{E = E}{n = n} c = subst (λ m → DS+ D (-IR D E) m ) (checkdeg c)( -IRtoDS+ ( -𝒹 (DS+to-IR  (suc n) c)))

DS+⃗ : { D E E' : Set₁} → {n : ℕ} → (E → E') → DS+ D E n → DS+ D E' n
DS+⃗ {D = D} {n = n} h = cont-map (SP+ D n , Arg+ n) h

𝒹+ : { D E : Set₁} → {n : ℕ} → DS+ D E (suc n) → DS+ D (Σ[ m ∈ ℕ ] (DS+ D E m)) n
𝒹+ = (DS+⃗ (λ x → ((ν x) , -IRtoDS+ x) )) ∘ 𝒹+''

DΣ : (D E : Set₁) → (n l : ℕ) → Set₁
DΣ D E zero l = DS+ D (Σ[ m ∈ ℕ ] (DS+ D E m)) l
DΣ D E (suc n) l = DS+ D (Σ[ m ∈ ℕ ](DΣ D E n m) ) l

iter𝒹+ : { D E : Set₁} → {n : ℕ} →  DS+ D E n → DΣ D E n zero
iter𝒹+ {n = zero} (Q , f) = (Q , λ x → (zero , (Inι+ (f x))))
iter𝒹+ { n = (suc n)} (Q , f) = {!!}


-- try to not keep the index

mutual
  data SP∞ (D : Set₁) : Set₁ where
    ι∞  : (X : Cont{lzero}{lzero}) → SP∞ D
    σδ∞ : (Q : SP∞ D) → ((Arg∞ {D = D} Q) → SP∞ D) → SP∞ D

  Arg∞ : {D : Set₁} → (SP∞ D) → Set₁
  Arg∞ {D = D} (ι∞ X) = cont X D
  Arg∞ {D = D} (σδ∞ Q h) = Σ[ x ∈ Arg∞ {D = D} Q ] (Arg∞ {D = D} (h x))

data DS∞ (D E : Set₁) : Set₁ where
    ds : ( Q : SP∞ D) → (Arg∞ Q → DS∞ D E) → DS∞ D E

{-
DS+⃗ : { D E : Set₁} → {n : ℕ} → ( F : E → Set₁) → (h : (e : E) → F e) → (Y : DS+ D E n) → (x : Arg+ n (proj₁ Y)) → DS+ D (F ((proj₂ Y) x)) n
DS+⃗ F h (Q , f) x = {!(Q , λ x → h (f x))!}
-}
{-
DS+⃗ : { D E : Set₁} → {n : ℕ} → {F : (Σ[ Q ∈ SP+ D n ] (Arg+ n Q)) → Set a} → (H : ( Y : (Σ[ Q ∈ SP+ D n ] (Arg+ n Q)) → (F Y) ))
→ DS+ D E (suc n) → ((Σ[ Q ∈ SP+ D n ] (Arg+ n Q)) →  DS+ D F(Q , x) (suc n))
DS+⃗ (Q , f) H (Q , x) = (Q , λ x → H(f x))
-}
{-
DS+⃗ : {D E : Set₁} → (F : E → Set₁) → {n : ℕ} → DS+ D E n → DS D (F e) n
DS+⃗ F = ?
-}

{-
_ i→ _ : ∀ (A A' : X → Set a) → Set a
A i→ A' = ∏ X (λ x → (A x) → (A' x))
-}

{-
♭SP : SP+ D n → SP D

♯Arg : Arg {D = D} → Arg+ n {D = D}
-}

atzero'+ : {D E : Set₁} → DS' D E → DS+ D E zero
atzero'+ (ι' X , f) = (ι+ X , f)
atzero'+ (σδ' X ,  f) = {!(σδ+ ((ι+ X) (λ x → (zero ♯SP x)   ,  f)!}

-- better not: direct comparison with DS
-- DS+ D E (suc n) → DS+ D (DS D E) n

atzero+DS : {D E : Set₁} → DS+ D E zero → DS D E
atzero+DS (ι+ X , f) = σδ X (ι ∘ f)

atzeroDS+ : {D E : Set₁} → DS D E → DS+ D (DS D E) zero
atzeroDS+ (ι e) = ι+ (⊤ , λ _ → ⊥) , ι ∘ (λ _ → e)
atzeroDS+ (σδ X f) = ι+ X , f

-- atzeroDS+' : {D E : Set₁} → DS D E → DS+ D E zero
-- atzeroDS+' (ι e) = ι+ (⊤ , λ _ → ⊥) , (λ _ → e)
-- atzeroDS+' (σδ X f) = ι+ X , f ∘ atzeroDS+'


{-

-- should be atzero : {D E : Set₁} → DS+ D E zero ≡ DS D E


mutual
  arg+ : {D : Set1} -> SP+ D -> Fam D -> Set
  arg+ (ι X) (U , T) = cont X U
  arg+ (σδ (Q , h)) (U , T)
    = Σ[ x ∈ cont Q U ] arg (h (cont-map Q T x)) (U , T)

  ⟦_⟧map : ∀ {D} -> (c : SP D)(Z : Fam D) -> arg c Z -> Arg c
  ⟦ ι X ⟧map (U , T) (x , g) = x , T ∘ g
  ⟦ σδ (Q , h) ⟧map (U , T) (x , y)
    = cont-map Q T x , ⟦ h _ ⟧map ( U , T) y

⟦_⟧ : {D E : Set1} -> DS+ D E -> Fam D -> Fam  E
⟦ c , α ⟧ Z = arg c Z , α ∘ ⟦ c ⟧map Z
-}













-- old

-- Informal proof idea to assign to a code  (c : -IR D E) a code (c' : DE^ (m( c)) D E)
-- where m(c) is a number that may depend on c.
-- two possibilities:

-- 1.keeping track of the number m(c)
-- ultimately this should give a function
-- Red : (c : -IR D E) -> DE^ (ν c) D E
{- in the iteration -𝒹^ we however have to consider also codes (Q, f) : IR^ n D E
 where we need a hereditary mu-nesting degree νν (Q , f) since f(x) can again have (nested) mu
 
νν : IR^ n D E → ℕ
νν (Q , f) = ν(-𝒹^ n (Q , f)) + maximum (z : Pos ( -𝒹^ n (Q , f)))(νν (proj₂ -𝒹^ n (Q , f)  z))

since -𝒹 does not produce any new mu
this (lred and red) terminate: We end with a code (eta X , f) :  where all f(x) are
again eta codes (eta X', f') where all f'(x') are again eta codes with
the same property ad finitum (since all branches have finite length).
lred replaces then all (eta X , ...) by σδ (X , ...). red treats the second components 
-}

{-

lred : {D E : Set₁}(n : ℕ)(c : -IR^ n D E) → DS D (IR^ (n + (ν c)) D E)
lred n (eta X , f) = σδ (X , -ηIR ∘ f)
lred n (mu (R , h) , f) = σδ (proj₁ (lred (ν (-𝒹^ (ν(-mu R h , f)) (-mu R h , f)))
                                          (-𝒹^ (ν(-mu R h , f)) (-mu R h , f))) ,
                             proj₂  (-𝒹^ (ν(-mu R h , f)) (-mu R h , f)))

red : {D E : Set₁}(n m : ℕ)(DS^ n D (IR^ m D E)) → (DS^ (suc n) D (IR^ n D E)
-}

-- 2. don't care about m(c) and using DSℕ :=  union (over all n : ℕ ) of all codes in DS^n D E
{-
-- this should return codes that have only eta codes in every branch


redIR : (n : ℕ)(c : -IR^n D E) → IR^(suc n)  D E
redIR zero (-eta X, f) = (-eta X, f)
redIR (suc n) (-eta X, f) = (proj₁ -𝒹 (-eta X, f) , λ x → redIR (suc n) (proj₁ -𝒹 (-eta X, f)))
redIR n (-mu(R , h), f) = redIR (suc n)(proj₁ -𝒹 -mu(R , h) ,
                          λ X → redIR (ν)(( proj₂ -mu(R , h))X) )(( proj₂ -mu(R , h))X) )

red : (c : -IR D E) → DS^(ν c) D E
red (-eta X , f) = σδ (X , ι ∘ f)
red (-mu(R , h) , f) = red ( proj₁ redIR (ν  (-mu(R , h) , f) (-mu(R , h) , f ),
                             proj₂ redIR (ν  (-mu(R , h) , f) (-mu(R , h) , f )


Eta := union (over all n : ℕ ) of all codes in -IR^n D E that have only eta in each branch

DSℕ :=  union (over all n : ℕ ) of all codes in DS^n D E

Red: Eta → DSℕ
Red (-eta X , f) = σδ (X , Red ∘ f)
-}

{-
-- we probably do not need

-μIRᴺ : {D : Set₁} -> {E : Set₁} → (n : ℕ) -> -IR^ n D E → -IR D E
-μIRᴺ ℕ.zero K = K
-μIRᴺ {D = D} {E = E} (ℕ.suc n) K = -μIRᴺ {D = D} {E = E} n (-μIR^ n K)


-IRd : (D : Set₁) → (E : {Q : -IRCont D}(x : -IRPos Q) → Set₁) → Set₁
-IRd D E = Σ[ Q ∈ -IRCont D ] ((x : -IRPos Q) → (E {Q = Q} x))

dTond : {D : Set₁} → {E : {Q : -IRCont D}(x : -IRPos Q) → Set₁} →  -IRd D (λ {Q} → E {Q}) → Σ[ Q ∈ -IRCont D ] (-IRPos Q → Σ[ x ∈ -IRPos Q ] (E {Q = Q} x)) --IR D (Σ (-IRPos) E)
dTond (Q , d) = (Q , λ x → (x , d x))
-}
-- The following intended def does not check since the map f in a code (Q , f) must not be a dependent function.
-- -IR D (λ _ →  -IR^ (ℕ.suc (ν c)) D E )

--φ : {D : Set₁} → (Q : -IRCont D) → (x : -IRPos Q) → Set₁
--φ Q x = -IR^ (ν (h x)) D E

{-
-IR^ (ℕ.suc (ν c)) D E
=
-IR D IR^ (ν c) D E
=
-IRd D (λ _ →  IR^ (ν c) D E)
-}

{-
-𝒹Cont : {D : Set₁} → -IRCont D → -IRCont D
-𝒹Cont (-eta H) = (-eta H)
-𝒹Cont (-mu R f) = R


-- Σ[ Q ∈ -IRCont D ] ((x : -IRPos Q) → (E {Q = Q} x))
-𝒹p' : {D : Set1} →  (c : -IR D E) -> -IRd D (λ Qx → -IR^ (ν ((proj₂ c) x)) D E --φ
-𝒹p' (-eta X , f) = (-eta X , -ηIR ∘ f) -- -IRd D (λ _ → -IR^ (ν (-eta X , f)) D E
-𝒹p' (-mu R f0 , f1) = (R , λ x0 → -𝒹p' (-Δ {R = R} f0 x0 f1))

-}
{-
-𝒹' : {D E : Set1} →  (c : -IR D E) -> -IR^ (ℕ.suc (ν c)) D E
-𝒹' (-eta X , f) = (-eta X , -ηIR ∘ f)
-𝒹' (-mu R f0 , f1) = (R , λ x0 → -𝒹' (-Δ {R = R} f0 x0 f1))
-}
