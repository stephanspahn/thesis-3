module Prelude.Equivalences where

-- Equivalences in Fam D

open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam

open _Fam⇒_

{------ The type of equivalences of Families -----------------------}

record _≃_ {D : Set1}(P Q : Fam D) : Set1 where
  field
    left  : P Fam⇒ Q
    right : Q Fam⇒ P
    leftright : left Fam∘ right ≡ FamId _
    rightleft : right Fam∘ left ≡ FamId _
    -- naive definition assuming all types are sets for now; these are
    -- logically equivalent to real equivalences anyway
open _≃_

{-------------------------------------------------------}


{------ The type of equivalences of Sets -----------------------}

module _ {a} where
  record _≌_ (L R : Set a) : Set a where
    field
      left-Set  : L → R
      right-Set : R → L
      lr-Set : left-Set ∘ right-Set ≡ id
      rl-Set : right-Set ∘ left-Set ≡ id
open _≌_

{------ What is an equivalence of Families, exactly? (using UIP) ---}

module _ {D : Set1}{P Q : Fam D} where

  Fam-≃ : (y : ((ind P) ≌ (ind Q))) ->
          (left1 : (x : ind P) → fib P x ≡ fib Q (left-Set y x)) ->
           P ≃ Q
  Fam-≃ y left1 =
    record { left = (left-Set y) , left1
           ; right = (right-Set y) , right1
           ; leftright = ⇒-≡ (lr-Set y) (ext (λ x → UIP _ _))
           ; rightleft = ⇒-≡ (rl-Set y) (ext (λ x → UIP _ _))
           }
           where right1 : (x : ind Q) → fib Q x ≡ fib P ((right-Set y) x)
                 right1 x = trans (cong (fib Q) (sym (happly (lr-Set y) x)))
                                  (sym (left1 ((right-Set y) x)))

{-------------------------------------------------------}

{------ ≌ is an equivalence relation -------------------}

ideq : {A : Set} -> A ≌ A
left-Set ideq = id
right-Set ideq = id
lr-Set ideq = refl
rl-Set ideq = refl


symeq : {A B : Set} -> A ≌ B -> B ≌ A
left-Set (symeq p) = right-Set p
right-Set (symeq p) = left-Set p
lr-Set (symeq p) = rl-Set p
rl-Set (symeq p) = lr-Set p

transeq : {A B C : Set} -> A ≌ B -> B ≌ C -> A ≌ C
left-Set (transeq p q) = left-Set q ∘ left-Set p
right-Set (transeq p q) = right-Set p ∘ right-Set q
lr-Set (transeq p q) = ext (λ x → trans
                                           (cong (left-Set q)
                                                 (happly (lr-Set p)
                                                         (right-Set q x)))
                                           (happly (lr-Set q) x))
rl-Set (transeq p q) = ext (λ x → trans
                                           (cong (right-Set p)
                                                 (happly (rl-Set q)
                                                         (left-Set p x)))
                                           (happly (rl-Set p) x))

{-------------------------------------------------------}


{------ ≃ is an equivalence relation -------------------}


IdEq : ∀ {D} {A : Fam D} -> A ≃ A
left IdEq = FamId _
right IdEq = FamId _
leftright IdEq = refl
rightleft IdEq = refl

symEq : ∀ {D} {A B : Fam D} -> A ≃ B -> B ≃ A
left (symEq p) = right p
right (symEq p) = left p
leftright (symEq p) = rightleft p
rightleft (symEq p) = leftright p

transEq : ∀ {D} {A B C : Fam D} -> A ≃ B -> B ≃ C -> A ≃ C
left (transEq p q) = left q Fam∘ left p
right (transEq p q) = right p Fam∘ right q
leftright (transEq p q)
  = comp-is-id-ext
     (left q Fam∘ left p)
     (right p Fam∘ right q)
     (λ x → trans (sym (happly (cong indmor (leftright q)) x))
                  (cong (indmor (left q))
                        (sym (happly (cong indmor (leftright p))
                                     (indmor (right q) x)))))
     (λ y → UIP _ _) -- TODO: this should be doable
rightleft (transEq p q)
  = comp-is-id-ext
     (right p Fam∘ right q)
     (left q Fam∘ left p)
     (λ x → trans (sym (happly (cong indmor (rightleft p)) x))
                  (cong (indmor (right p))
                        (sym (happly (cong indmor (rightleft q))
                                     (indmor (left p) x)))))
     (λ y → UIP _ _) -- TODO: this should be doable

{-------------------------------------------------------}

{----- Constructions closed under ≃ --------------------}

-- coproducts
+-≃ : ∀ {D}{A B A' B' : Fam D} -> A ≃ A' -> B ≃ B' ->
        (A Fam+ B) ≃ (A' Fam+ B')
left (+-≃ p q) = (⊎-map (indmor (left p)) ((indmor (left q))) ,
                  (λ { (inj₁ x) → indmor= (left p) x ;
                       (inj₂ y) → indmor= (left q) y }))
right (+-≃ p q) = (⊎-map (indmor (right p)) ((indmor (right q))) ,
                  (λ { (inj₁ x) → indmor= (right p) x ;
                       (inj₂ y) → indmor= (right q) y }))
leftright (+-≃ p q)
 = comp-is-id-ext
     (left (+-≃ p q))
     (right (+-≃ p q))
     (λ {(inj₁ x) → cong inj₁
                         (sym (happly (cong indmor (leftright p)) x)) ;
         (inj₂ y) → cong inj₂
                         (sym (happly (cong indmor (leftright q)) y)) })
                         (λ y → UIP _ _) -- TODO: again, shouldn't be hard
rightleft (+-≃ p q)
 = comp-is-id-ext
     (right (+-≃ p q))
     (left (+-≃ p q))
     (λ {(inj₁ x) → cong inj₁
                         (sym (happly (cong indmor (rightleft p)) x)) ;
         (inj₂ y) → cong inj₂
                         (sym (happly (cong indmor (rightleft q)) y)) })
                         (λ y → UIP _ _) -- TODO: again, shouldn't be hard

{-------------------------------------------------------}

module ≃-Reasoning {D : Set₁} where

  infix  3 _∎
  infixr 2 _≃⟨_⟩_
  infix  1 begin_

  begin_ : ∀{x y : Fam D} → x ≃ y → x ≃ y
  begin_ x≃y = x≃y

  _≃⟨_⟩_ : ∀ (x {y z} : Fam D) → x ≃ y → y ≃ z → x ≃ z
  _ ≃⟨ x≃y ⟩ y≃z = transEq x≃y y≃z

  _∎ : ∀ (x : Fam D) → x ≃ x
  _∎ _ = IdEq

≡to≃ : {D : Set₁}{X Y : Fam D} → X ≡ Y → X ≃ Y
≡to≃ refl = IdEq

subst-Fam : ∀ {a}{A : Set a}{x₁ x₂ : A}{D : Set₁} → (P : A → Fam D) →
           x₁ ≡ x₂ → ((P x₁) Fam⇒ (P x₂))
subst-Fam {x₁ = x₁} P refl = FamId (P x₁)

{-------------------------------------------------------}

module ≌-Reasoning where

  infix  3 _∎
  infixr 2 _≌⟨_⟩_
  infix  1 begin_

  begin_ : ∀{x y : Set} → x ≌ y → x ≌ y
  begin_ x≌y = x≌y

  _≌⟨_⟩_ : ∀ (x {y z} : Set) → x ≌ y → y ≌ z → x ≌ z
  _ ≌⟨ x≌y ⟩ y≌z = transeq x≌y y≌z

  _∎ : ∀ (x : Set) → x ≌ x
  _∎ _ = ideq

≡to≌ : {X Y : Set} → X ≡ Y → X ≌ Y
≡to≌ refl = ideq

{-
subst-Set : ∀ {a}{A : Set a}{x₁ x₂ : A} → (P : A → Set) →
           x₁ ≡ x₂ → ((P x₁) → (P x₂))
subst-Set {x₁ = x₁} P refl = id (P x₁)
-}


{-------------Some Equivalences ----------------------}


assocΣ : (A : Set) → {B : A → Set} → {C : (a : A) → ( a' : (B a)) → Set} →
         ((Σ[ w ∈ (Σ A B) ](C(proj₁ w)(proj₂ w))) ≌ (Σ[ a ∈  A ] (Σ[ a' ∈ ( B a) ] (C a a'))))
assocΣ A = record {left-Set = λ {((a , a') , c) → (a , (a' , c))}
                      ;right-Set = λ {(a , (a' , c)) → ((a , a') , c)}
                      ; lr-Set = refl
                      ; rl-Set = refl}


assocΣFam : (A : Set) → {B : A → Set} → {C : (a : A) → ( a' : (B a)) → Set} → {T : (Σ[ w ∈ (Σ A B) ](C(proj₁ w)(proj₂ w))) → Set} →
         (((Σ[ w ∈ (Σ A B) ](C(proj₁ w)(proj₂ w))), T ) ≃ ((Σ[ a ∈  A ] (Σ[ a' ∈ ( B a) ] (C a a'))) , λ {(a , (a' , z)) → (T (( a , a') , z))}))
assocΣFam A =  Fam-≃ (assocΣ A) (λ x → refl)


