module DS.Examples where


open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam
open import Prelude.Container

open import Data.Fin hiding (_+_; lift)

open import DS.IR
open import DS.Initial


-- W-types

module _ (S : Set)(P : S -> Set) where

  -- "upgraded" W-type where decoding component applies T everywhere

  cW : DS Set Set
  cW = σ S (λ s → δ (P s) (λ Tf → ι ((x : P s) -> Tf x)))


  W : Set
  W = U {c = cW}

  sup : (s : S) -> (P s -> W) -> W
  sup s f = intro (s , (f , _))

-- A universe closed under sigma- and pi-, and W-types

cUΣΠW : DS Set Set
cUΣΠW =
  σ Tags
    (λ { bool → ι Bool
       ; sig → δ ⊤ (λ X → δ (X _) (λ Y → ι (Σ (X _) Y)))
       ; pi → δ ⊤ (λ X → δ (X _) (λ Y → ι ((x : X _) -> Y x)))
       ; w → δ ⊤ (λ X → δ (X _) (λ Y → ι (W (X _) Y)))
       })
  module TagU where data Tags : Set where
                      bool sig pi w : Tags

UΣΠW : Set
UΣΠW = U {c = cUΣΠW}

TΣΠW : UΣΠW -> Set
TΣΠW x = T x

bool : UΣΠW
bool = intro (TagU.bool , tt)

sig : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
sig a b = intro (TagU.sig , (λ _ → a) , b , tt)

pi' : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
pi' a b = intro (TagU.pi , (λ _ → a) , b , tt)

w : (a : UΣΠW) -> (b : TΣΠW a -> UΣΠW) -> UΣΠW
w a b = intro (TagU.w , (λ _ → a) , b , tt)

-- A language of sums and products

sumFin : (n : ℕ) -> (Fin n -> ℕ) -> ℕ
sumFin zero f = 0
sumFin (suc n) f = f (fromℕ n) + sumFin n (f ∘ inject₁)

prodFin : (n : ℕ) -> (Fin n -> ℕ) -> ℕ
prodFin zero f = 1
prodFin (suc n) f = f (fromℕ n) * prodFin n (f ∘ inject₁)

cArith : DS (Lift ℕ) (Lift ℕ)
cArith =
  σ Tags
    (λ { fin → σ ℕ (λ n → ι (lift n))
       ; sum → δ ⊤
                 (λ n → δ (Fin (lower (n _)))
                          (λ f → ι (lift (sumFin (lower (n _)) (lower ∘ f)))))
       ; prod → δ ⊤
                  (λ n → δ (Fin (lower (n _)))
                           (λ f → ι (lift (prodFin (lower (n _)) (lower ∘ f)))))
        })
  module TagArith where data Tags : Set where
                          fin sum prod : Tags

fac : ℕ -> U {c = cArith}
fac n = intro (TagArith.prod ,
              (λ _ → intro (TagArith.fin , n , tt)) ,
              (λ x → intro (TagArith.fin , (suc (toℕ x)) , tt)) , tt)

Tfac5 : T (fac 5) ≡ lift 120
Tfac5 = refl

-- Using the upgraded W-type

-- since the container (ℕ ◃ Fin) X ≃ List X, we have
-- ⟦ cW ℕ Fin ⟧₀ (X , T) ≃ (List X , (x1 , ... , xn) ↦ (T x1) × ... × (T xn))

test0 : (T : ℕ -> Set) -> ⟦ cW ℕ Fin ⟧₀ (ℕ , T)
test0 T = (3 , (λ { zero             → 2
                  ; (suc zero)       → 5
                  ; (suc (suc zero)) → 7
                  ; (suc (suc (suc ()))) })
             , tt)

test1 : {T : ℕ -> Set} ->
        ⟦ cW ℕ Fin ⟧₁ (ℕ , T) (test0 T) -> T 2 × T 5 × T 7
test1 l = l (# 0) , l (# 1) , l (# 2)



{-
-- Lists
-- L : DS Set Set such that  ⟦ L ⟧ (U , T) = (List U , (x1 , ... , x_n) ↦ (T x1) x ... (T xn))

open import DS.iotaProd

l : {E : Set₁} → ℕ → DS E E --→ DS D E
l zero = δ ⊤ λ d → ι (d tt)
l (suc n) = δ ⊤ λ d → ((l n) ^ (d tt))

L : {E : Set₁} → DS E E
L = σ ℕ λ n → (l n)

{-
l 0 = ι tt
l 1 = δ ⊤ λ d → ι (d tt ) -- the identity functor
l 2 = δ ⊤ λ d → δ ⊤ λ d' → ι ((d tt) × (d' tt)) -- (U , T) ↦ (U , T)^2 pointwise binary product
l 3 = δ ⊤ λ d → δ ⊤ λ d' → δ ⊤ λ d'' →  ι ((d tt) × (d' tt) × (d'' tt)) -- (U,T) ↦ (U,T)^3 pointwise 3-fold product
...
-}

-- ⟦ L(S,V) ⟧ : (U . T) ↦ (cont (S , V) U , (s,f) ↦ (Π (P s) f) ) where
-- e.g. (S , V) = (ℕ , Fin)

open import DS.DSPi

-- does not type check
-- Lc : {E : Set₁} → Cont → Fam E → Fam E
-- Lc (S , P) = DS.DSPi.⟦ σ S (λ s → δ (P s) (λ f → π (P s) λ x → ι (f x))) ⟧

-- σ S λ s → δ (P s) λ f → ι (Π (P s ) f)
-- or σ S λ s → δ (P s) λ f → ι (Σ (P s ) f)


-}
