{-# OPTIONS --without-K #-}
module DS.IRdecodingFunctorial where

open import Prelude.Basic
open import Prelude.Fam
open import Prelude.Equality

open import DS.IR


mutual
  ⟦_⟧₀-map : {D E : Set1} -> (c : DS D E) -> {Z Z' : Fam D} -> (Z Fam⇒ Z') ->
             ⟦ c ⟧₀ Z -> ⟦ c ⟧₀ Z'
  ⟦ ι e ⟧₀-map g _ = _
  ⟦ σ A f ⟧₀-map g (a , x) = a , ⟦ f a ⟧₀-map g x
  ⟦ δ A F ⟧₀-map {Z} {Z'} (g , p) (h , x) = (g ∘ h , subst (λ z → ⟦ F z ⟧₀ Z') (ext (p ∘ h)) (⟦ F _ ⟧₀-map (g , p) x))

  ⟦_⟧₁-map : {D E : Set1} -> (c : DS D E) -> {Z Z' : Fam D} ->
             (f : Z Fam⇒ Z') -> (x : ⟦ c ⟧₀ Z) ->
             ⟦ c ⟧₁ Z x ≡ ⟦ c ⟧₁ Z' (⟦ c ⟧₀-map f x)
  ⟦ ι e ⟧₁-map g _ = refl
  ⟦ σ A f ⟧₁-map g (a , x) = ⟦ f a ⟧₁-map g x
  ⟦ δ A F ⟧₁-map {Z} {Z'} (g , p) (h , x) rewrite ext (p ∘ h) = ⟦ F _ ⟧₁-map (g , p) x

⟦_⟧-map : {D E : Set1} -> (c : DS D E) -> {Z Z' : Fam D} -> (Z Fam⇒ Z') ->
          ⟦ c ⟧ Z Fam⇒ ⟦ c ⟧ Z'
⟦ c ⟧-map f = (⟦ c ⟧₀-map f , ⟦ c ⟧₁-map f)
