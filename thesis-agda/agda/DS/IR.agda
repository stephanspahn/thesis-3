module DS.IR where

open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam
open import Prelude.Equivalences


{- Dybjer-Setzer codes -}
data DS (D : Set1) (E : Set1) : Set1 where
   ι : E →  DS D E
   σ : (A : Set) → (A → DS D E) → DS D E
   δ : (A : Set) → ((A → D) → DS D E) → DS D E

⟦_⟧₀ : {D E : Set1} -> DS D E -> Fam D -> Set
⟦ ι e ⟧₀ X = ⊤
⟦ σ A f ⟧₀ X = Σ[ a ∈ A ] ⟦ f a ⟧₀ X
⟦ δ A F ⟧₀ (U , T) = Σ[ g ∈ (A -> U) ] ⟦ F (T ∘ g) ⟧₀ (U , T)

⟦_⟧₁ : {D E : Set1} -> (c : DS D E) -> (F : Fam D) -> ⟦ c ⟧₀ F -> E
⟦ ι e ⟧₁ X _ = e
⟦ σ A f ⟧₁ X (a , x) = ⟦ f a ⟧₁ X x
⟦ δ A F ⟧₁ (U , T) (g , x) = ⟦ F (T ∘ g) ⟧₁ (U , T) x

⟦_⟧ : {D E : Set1} -> DS D E -> Fam D -> Fam E
⟦ c ⟧ X = ⟦ c ⟧₀ X , ⟦ c ⟧₁ X

-- DS D is functorial

DS-map : {D E E' : Set₁} -> (E -> E') -> DS D E  -> DS D E'
DS-map h (ι e) = ι (h e)
DS-map h (σ A f) = σ A (λ a → DS-map h (f a))
DS-map h (δ A F) = δ A (λ g → DS-map h (F g))

-- and monadic

ηDS : {I O : Set₁} → O → DS I O
ηDS o = ι o

μDS : {I O : Set₁} → DS I (DS I O) → DS I O
μDS (ι o) = o
μDS (σ A f) = σ A λ a → μDS (f a)
μDS (δ A F) = δ A λ h → μDS (F h)

-- and thus has a bind operation

_DS->>=_ : {D E E' : Set₁} → DS D E → (E → DS D E') → DS D E'
c DS->>= h = μDS (DS-map h c)

-- functoriality and semantics
{- no time for this lemma atm but it should be true
mutual
  lemma-⟦map⟧₀ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → ⟦ DS-map h c ⟧₀ P ≡ ⟦ c ⟧₀ P
  lemma-⟦map⟧₀ (ι e) h = refl
  lemma-⟦map⟧₀ {C = C} (σ A f) {P = P} h = cong (Σ A)(ext (λ a → (lemma-⟦map⟧₀ (f a) h)))
                                       --trans (trans refl (cong² Σ (ext (λ a → (lemma-⟦map⟧₀ (f a) h))))) refl
                                       {-begin
                                     ⟦ σ A (λ a → DS-map h (f a)) ⟧₀ P
                                     ≡⟨ refl ⟩
                                     (Σ[ a ∈ A ](⟦ DS-map h (f a) ⟧₀ P))
                                     ≡⟨ (cong² Σ (ext (λ a → (lemma-⟦map⟧₀ (f a) h)))) ⟩
                                     (Σ[ a ∈ A ](⟦ (f a) ⟧₀ P))
                                     ≡⟨ refl ⟩
                                     ⟦ σ A f ⟧₀ P
                                   ∎ where open ≡-Reasoning-}
  lemma-⟦map⟧₀ (δ A F){P = P} h = cong (Σ (A → (ind P))) (ext (λ G → (lemma-⟦map⟧₀  (F ((fib P) ∘ G)) h)))
                                   -- trans (trans refl  (cong² Σ (ext (λ G → (lemma-⟦map⟧₀  (F ((fib P) ∘ G)) h))))) refl
                                   {-begin
                                   ⟦ δ A (λ g → DS-map h (F g)) ⟧₀ P
                                   ≡⟨ refl ⟩
                                   (Σ[ G ∈ (A → ind P) ](⟦ DS-map h (F ((fib P) ∘ G)) ⟧₀ P))
                                   ≡⟨ (cong² Σ (ext (λ G → (lemma-⟦map⟧₀  (F ((fib P) ∘ G)) h)))) ⟩
                                   (Σ[ G ∈ (A → ind P) ](⟦ F ((fib P) ∘ G) ⟧₀ P))
                                   ≡⟨ refl ⟩
                                   ⟦ (δ A F) ⟧₀ P
                                    ∎ where open ≡-Reasoning-}

  lemma-⟦map⟧₁ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → (x : ⟦ DS-map h c ⟧₀ P) → (⟦  DS-map h c ⟧₁ P) x  ≡ h ((⟦ c ⟧₁ P) (subst id (lemma-⟦map⟧₀ c h) x))
  lemma-⟦map⟧₁ (ι e) h x = refl 
  lemma-⟦map⟧₁ (σ A f) {P = P} h (a , q) = begin
                                            ( (⟦ (DS-map h (f a)) ⟧₁ P) q)
                                           ≡⟨(lemma-⟦map⟧₁ (f a) h q)⟩
                                           ( h ((⟦ (f a) ⟧₁ P) (subst id (lemma-⟦map⟧₀ (f a) h) q)))
                                
                                          ≡⟨ (cong h (cong₂d (λ x y → ⟦ f x ⟧₁ P y)
                                                      (part1 extIH)
                                                           (trans
                                                           (cong (λ w → subst (λ z → ⟦ f z ⟧₀ P)
                                                                              (part1 extIH) (subst id w q))
                                                                 (sym (ext-β (λ x → lemma-⟦map⟧₀ (f x) h) a)))
                                                           (part2 extIH))
                                                           ))⟩
                                                          where extIH = (ext (λ x → lemma-⟦map⟧₀ (f x) h))
                                                         part1 : (p : _) -> a ≡ proj₁ (subst id (cong (Σ A) p) (a , q))
                                                         part1 p = trans
                                                                    (sym (subst-const p))
                                                                    (trans (subst-natural proj₁ p)
                                                                           (cong proj₁ (subst-cong p)))
                                                         part2 : (p : _) -> subst (λ z → ⟦ f z ⟧₀ P)
                                                                                  (part1 p)
                                                                                  (subst id (happly p a) q)
                                                                             ≡ proj₂ (subst id (cong (Σ A) p) (a , q))
                                                         part2 p = trans
                                                                    (trans
                                                                      (subst-trans (sym (subst-const p)) _)
                                                                      (trans
                                                                         (subst-trans (subst-natural proj₁ p) _)
                                                                         (trans
                                                                           (sym (subst-cong {B = λ z → ⟦ f z ⟧₀ P} {f = proj₁} (subst-cong p)))
                                                                           (cong
                                                                             (subst (λ z → ⟦ f (proj₁ z) ⟧₀ P)
                                                                                    (subst-cong p))
                                                                             (trans {!!} (sym (subst-proj₂ p)))))))
                                                                    (congd proj₂ (subst-cong p))
                                                                    {!- begin
                                                                             (( ⟦ (DS-map h  (σ A f)) ⟧₁ P) (a , q))
                                                                             ≡⟨ refl ⟩
                                                                             (⟦ σ A (λ a → (DS-map h (f a))) ⟧₁ P (a , q))
                                                                              ≡⟨ refl ⟩
                                                                             ( (⟦ (DS-map h (f a)) ⟧₁ P) q)
                                                                             ≡⟨ lemma-⟦map⟧₁ (f a) h q ⟩
                                                                             ( h ((⟦ (f a) ⟧₁ P) (subst id (lemma-⟦map⟧₀ (f a) h) q)))
                                                                             ≡⟨ refl ⟩
                                                                              (h ((⟦ (σ A f) ⟧₁ P) ( a  , (subst id (lemma-⟦map⟧₀ (f a) h) q))))
                                                                             ≡⟨ ? ⟩
                                                                              (h ((⟦ (σ A f) ⟧₁ P)( (subst proj₁ (lemma-⟦map⟧₀ (σ A f) h) a) ,  (subst proj₂ (lemma-⟦map⟧₀ (σ A f) h) q )  )))
                                                                             ≡⟨ cong (h ∘ (⟦ (σ A f) ⟧₁ P)) (sym Σ-≡ (subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h)) {!Q!}) ⟩
                                                                             (h ((⟦ (σ A f) ⟧₁ P) ( proj₁ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q)) , proj₂ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q)))))
                                                                             ≡⟨ refl ⟩
                                                                             (h ((⟦ (σ A f) ⟧₁ P) (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q))))
                                                                           ∎ where open ≡-Reasoning-!}
  lemma-⟦map⟧₁ (δ A F) h x = {!!}
  
  lemma-⟦map⟧ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → ⟦ DS-map h c ⟧ P ≡ Fam-map h (⟦ c ⟧ P)
  lemma-⟦map⟧ c h = Fam-≡ (lemma-⟦map⟧₀ c h)(λ x → lemma-⟦map⟧₁ c h x )
  -}
{-
where Q : subst (λ w → ⟦ f w ⟧₀ P) (subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h)) ( proj₂ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q))) ≡ (subst id (lemma-⟦map⟧₀ (σ A f) h) q)
Q = begin
 subst (λ w → ⟦ f w ⟧₀ P) (subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h)) ( proj₂ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q)))
 ≡⟨ cong (subst (λ w → ⟦ f w ⟧₀ P) (subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h))) (sym ( subst-natural proj₂ (lemma-⟦map⟧₀ (σ A f) h)))⟩
 subst (λ w → ⟦ f w ⟧₀ P) (subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h)) (subst id (lemma-⟦map⟧₀ (σ A f) h) q)
≡⟨Q' ⟩
(subst id (lemma-⟦map⟧₀ (σ A f) h) q)
-}


--(subst-natural proj₂ (lemma-⟦map⟧₀ (σ A f) h)) 
--proj₂ (lemma-⟦map⟧₀ (σ A f) h) (a , q) ≡ ((lemma-⟦map⟧₀ (f a) h) q)
{-

 (subst (proj₁ ∘ id) (lemma-⟦map⟧₀ (σ A f) h) a
≡⟨ subst-natural proj₁ (lemma-⟦map⟧₀ (σ A f) h) ⟩
 proj₁ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q))

 (subst (proj₂ ∘ id) (lemma-⟦map⟧₀ (σ A f) h) q
≡⟨ subst-natural proj₂ (lemma-⟦map⟧₀ (σ A f) h) ⟩
 proj₂ (subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q))
-} 


{-
(subst id (lemma-⟦map⟧₀ (σ A f) h) (a , q))
≡⟨ refl ⟩ -- by definition
(subst id (cong² Σ (ext (λ a → (lemma-⟦map⟧₀ (f a) h)))) (a , q))
≡⟨ refl ⟩
(a ,  (subst id (lemma-⟦map⟧₀ (f a) h) q))
-}



{-
                                    {! ≡⟨{! Σ-≡(cong₂d Σ refl (lemma-⟦map⟧₀ (f a) h))() !}⟩
                                    (( Σ[ a ∈ A ](ind ( Fam-map h (⟦ (f a) ⟧ P)))) , λ  {( a , q) → (h( fib (⟦ (f a) ⟧ P))q)} )
                                    ≡⟨ ? ⟩
                                    Fam-map h (⋃ A (λ a → ⟦ (f a) ⟧ P))!}
-}

{- Use the equality-of-families operators in Prelude.Fam instead

lemma-⟦map⟧₀ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → ⟦  DS-map h c ⟧₀ P ≡ (⟦ c ⟧₀ P)

lemma-⟦map⟧₁ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → ⟦  DS-map h c ⟧₁ P ≡ h ∘ (⟦ c ⟧₁ P)

lemma-⟦map⟧ : {C E E' : Set₁} → (c : DS C E) → {P : Fam C} → (h : E → E') → ⟦  DS-map h c ⟧ P ≡ Fam-map h (⟦ c ⟧ P)
lemma-⟦map⟧ c h  = Σ-≡ (lemma-⟦map⟧₀ c h) q
   where q : subst S (lemma-⟦map⟧₀ c h) ⟦ DS-map h c ⟧₁ P ≡ h ∘ (⟦ c ⟧₁ P)

     where S: {D : Set₁} → Set → Set₁
           S A = (A → D)

lemma-⟦map⟧ (ι e) h = refl
lemma-⟦map⟧ (σ A f){P = P} h = begin
                                 ⟦ σ A ( λ a → DS-map h (f a)) ⟧ P
                                 ≡⟨ refl ⟩
                                 ⋃ A (λ a → ⟦ DS-map h (f a) ⟧ P)
                                 ≡⟨ refl ⟩
                                 ( (Σ[ a ∈ A ](⟦ DS-map h (f a) ⟧₀ P)) , λ {(a , q) → (( ⟦ DS-map h (f a) ⟧₁ P) q) })
                                 ≡⟨ Σ-≡(cong₂d Σ refl (lemma-⟦map⟧ (f a) h))() ⟩
                                 (( Σ[ a ∈ A ](ind ( Fam-map h (⟦ (f a) ⟧ P)))) , λ {( a , q) → (h( fib ( ⟦ (f a) ⟧ P))q)} )
                                  ≡⟨ refl ⟩
                                 Fam-map h (⋃ A (λ a → ⟦ (f a) ⟧ P))
                                 ≡⟨ refl ⟩
                                 Fam-map h (⟦ σ A f ⟧ P)
                               ∎ where open ≡-Reasoning
lemma-⟦map⟧ (δ A f){P = P} h = ?
-}                                 

{-
_≺_ : {D E : Set₁} → DS D E → DS D E → Set₁
c ≺ (ι e) = ⊥
c ≺ (σ A f) = ∃[ a ]((f a) ≡ c)
c ≺ (δ A F) = ∃[ X ](F X  ≡ c))

_≺≺_ : {D E : Set₁} → DS D E → DS D E → Set₁
c ≺≺ (ι e) = ⊥
c ≺≺ (σ A f) = ∃[ a ](c ≺≺ (f a))
c ≺≺ (δ A F) = ∃[ X ] (c ≺≺ (F X))
-}
