module DS.iotaProd where

-- product of leaves with a iota (needed for family lists)

open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Equivalences
open import Prelude.Fam

open import DS.IR

postulate
  _&_ : {E : Set₁} → E → E → E
  
_^_ : {D E : Set₁} → DS D E → E → DS D E
ι e ^ s = ι (e & s)
σ A f ^ s = σ A λ a → ((f a) ^ s)
δ A F ^ s = δ A λ h → ((F h) ^ s)

