{-# OPTIONS --allow-unsolved-metas #-}
module Misc.CwF-RG-Fam where

open import Misc.RG-Fam
open import Prelude.Basic
open import Data.Unit
open import Data.Empty
open import Data.Bool hiding (T)
open import Data.Nat
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality
open import Prelude.Equality

open ≡-Reasoning


-- The CwF of reflexive graph-families

-- The unit reflexive graph family over Γ, types, terms

⊤_ : {Γ : RG} -> FamRG Γ
⊤_ = FamRG[ (λ _ → ⊤) , (λ _ → ⊤) , (λ _ → id) , (λ _ → id) , (λ _ → id) , (λ _ → refl) , (λ _ → refl) ]



--⊤⇒ : (Γ Δ : RG) → FamRG⇒ (⊤_ {Γ = Γ}) (⊤_ {Γ = Δ })
--⊤⇒ Γ Δ = FamRG⇒[ (λ x → (λ y → y)) ,  (λ x → (λ y → y)) ,  (λ x → refl) ,  (λ x → refl) ,  (λ x → refl) ]

Ty : (Γ : RG) → Set₁
Ty Γ = FamRG Γ

Tm :  (Γ : RG) → (A : Ty Γ) → Set₁
Tm Γ A = FamRG⇒ Γ ⊤_ A


ID : (Γ : RG) → Ty Γ
ID Γ = FamRG[ (λ x → o Γ) ,  (λ x → r Γ) ,  (λ x → ref Γ) ,  (λ x → src Γ) ,  (λ x → tgt Γ) ,
              (λ x → ext (λ y → trans (ul Γ y) (sym (subst-const {B = o Γ} (sym (ul Γ x)))))) ,
              (λ x → ext (λ y → trans (ur Γ y) (sym (subst-const {B = o Γ} (sym (ur Γ x)))))) ]
              
-- substitution of types, and -of terms

-- here 𝒪 (A [ f ]Ty) x etc is replaced by its definition

subst3 : {Γ Δ : RG} → {A : FamRG Δ} → {x : o Γ} → {f : HomRG Γ Δ} → (𝒪 A (src Δ (ref Δ ((𝒻0 f) x)))) ≡ (𝒪 A  ((𝒻0 f) (src Γ (ref Γ x))))
subst3 {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f} = sym (trans (cong (𝒪 A) (sym ((𝒻src f) (ref Γ x)))) (cong (𝒪 A) (cong (src Δ) (((𝒻ref f) x) ))))

subst3r : {Γ Δ : RG} → {A : FamRG Δ} → {x : o Γ} → {f : HomRG Γ Δ} → (𝒪 A (tgt Δ (ref Δ ((𝒻0 f) x)))) ≡ (𝒪 A  ((𝒻0 f) (tgt Γ (ref Γ x))))
subst3r {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f} = sym (trans (cong (𝒪 A) (sym ((𝒻tgt f) (ref Γ x)))) (cong (𝒪 A) (cong (tgt Δ) (((𝒻ref f) x) ))))

subst-cd : ∀{l}{A B B' : Set l} → B ≡ B' → (A → B) → (A → B')
subst-cd {A = A} q α = subst (λ X → (A → X)) q α

{-comment this out for the moment because of the holes
substTy : {Γ Δ : RG} → Ty Δ → (HomRG Γ Δ ) → Ty Γ
substTy {Γ = Γ} {Δ = Δ} A f = FamRG[ (𝒪 A) ∘ (𝒻0 f) ,
  (𝓡 A) ∘ (𝒻1 f) ,
  (λ x → ((subst (𝓡 A) (sym ((𝒻ref f) x))) ∘ ((bref A) ((𝒻0 f) x)))) ,
  (λ y → (subst (𝒪 A) (((𝒻src f) y))) ∘ ((bsrc A)((𝒻1 f) y))) ,
  (λ y → (subst (𝒪 A) ((𝒻tgt f) y)) ∘ (btgt A)((𝒻1 f) y)) ,
  (λ x →  {!ap {lzero} {(𝒪 A ((𝒻0 f) x)) → (𝒪 A (src Δ (ref Δ ((𝒻0 f)x))))}{(𝒪 A ((𝒻0 f) x)) → (𝒪 A ((𝒻0 f) (src Γ (ref Γ x))))} {(bsrc A (ref Δ ((𝒻0 f) x))) ∘ (bref A ((𝒻0 f)x))}{subst (𝒪 A) (sym (ul Δ ((𝒻0 f) x )))}
  (subst-cd {lzero} {𝒪 A ((𝒻0 f) x)} {𝒪 A (src Δ (ref Δ ((𝒻0 f) x)))} {𝒪 A ((𝒻0 f) (src Γ (ref Γ x)))} (subst3 {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f}) ) (ptul A ((𝒻0 f) x))!}) ,
  {!λ x →  ap {lzero} {(𝒪 A ((𝒻0 f) x)) → (𝒪 A (src Δ (ref Δ ((𝒻0 f)x))))}{(𝒪 A ((𝒻0 f) x)) → (𝒪 A ((𝒻0 f) (src Γ (ref Γ x))))} {bsrc (ref Γ x)} {subst 𝒪 (sym ul Γ x)}
  (subst-cd {lzero}{𝒪 A ((𝒻0 f) x)} {𝒪 A (src Δ (ref Δ ((𝒻0 f)x)))} {𝒪 A ((𝒻0 f) (src Γ (ref Γ x)))} (subst3r {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f})) (ptur A ((𝒻0 f) x))!} ]
-}

--famifyHom : {Γ Δ : RG} → (f : HomRG Γ Δ) → FamRG⇒ Γ (ID Γ) (substTy (ID Δ) f)
--famifyHom f = {!!}

--famify⊤ : {Γ Δ : RG} → (f : HomRG Γ Δ) → FamRG⇒ Γ (⊤_ {Γ}) (substTy ⊤_{Δ} f)
--famify⊤ = ?

{-one could also take pointwise equality of reflexive graph families-}
--exch0⊤_ : {Γ Δ : RG} → {x : o Γ} → (f : HomRG Γ Δ ) → (𝒪 (⊤_{Γ = Γ}) x) ≡ (𝒪 (⊤_{Δ = Δ}) ((𝒻0 f) x))
--exch0⊤_ f = refl

{-comment this out for the moment because of the holes
substTm : {Γ Δ : RG} → {A : Ty Δ} → Tm Δ A → (f : HomRG Γ Δ) → Tm Γ (substTy A f )
substTm {Γ = Γ} {Δ = Δ} {A = A} M f = FamRG⇒[ (λ x → ((m𝒪 M ((𝒻0 f) x)))) ,
                                              (λ y → ((m𝓡 M ((𝒻1 f) y)))) ,
                               {!(λ x → (subst (λ X → (X ≡ ((bref (substTy {Γ = Γ}{Δ = Δ} A f) x) ∘ (m𝒪 M ((𝒻0 f)x)))))(ext (λ z → (cong-app (cong (m𝓡 M) ((𝒻ref f) x))((bref ⊤_ {Δ = Δ}((𝒻0 f) x))z) )))(mref M ((𝒻0 f) x))))!} ,
                              {! (λ y → (subst (λ Y → (Y ≡ (bsrc (substTy {Γ = Γ} {Δ = Δ} A f) y) ∘ (m𝓡 M ((𝒻1 f)y)))))(ext (λ z → (cong-app (cong (m𝒪 M) ((𝒻src f) y))((bsrc ⊤_ {Δ = Δ}((𝒻1 f) y)) z))))(msrc M ((𝒻1 f) y)))!} ,
                              {! (λ y → (subst (λ Y → (Y ≡ (btgt (substTy {Γ = Γ} {Δ = Δ} A f) y) ∘ (m𝓡 M ((𝒻1 f)y)))))(ext (λ z → (cong-app (cong (m𝒪 M) ((𝒻tgt f) y))((bsrc ⊤_ {Δ = Δ}((𝒻1 f) y)) z))))(mtgt M ((𝒻1 f) y)))!}
                               ]

-}
                              

{-


(subst (λ X → (X ≡ ((bref A ((𝒻0 f) x)) ∘ (m𝒪 M ((𝒻0 f)x))))) ? )

(flatpaste (cong (m𝓡 M) ((𝒻ref f) x)) (cong bref  ))
-- (cong-app (cong (m𝓡 M) ((𝒻ref f) x))(bref ⊤_ {Δ = Δ} ((𝒻0 f) x))  ))) ,
substTm : {Γ Δ : RG} → {A : Ty Δ} → Tm Δ A → (f : HomRG Γ Δ) → Tm Γ (substTy A f )
substTm M f = ∘FamRG⇒ (famifyHom f) M

-}
{-
substTm : {Γ Δ : RG} → {A : Ty Δ} → Tm Δ A → (f : HomRG Γ Δ) → Tm Γ (substTy A f ) 
substTm M f = FamRG⇒[_ , _ , _ , _ , _]
-}


-- comprehension

{-comment this out for the moment because of the holes
_•_ : (Γ : RG) → (A : Ty Γ) → RG
Γ • A = RG[ (Σ (o Γ) (𝒪 A)) ,
            (Σ (r Γ) (𝓡 A)) ,
            (λ x → (ref Γ (proj₁ x) , bref A (proj₁ x) (proj₂ x))) ,
            (λ y → (src Γ (proj₁ y) , bsrc A (proj₁ y) (proj₂ y))) ,
            (λ y → (tgt Γ (proj₁ y) , btgt A (proj₁ y) (proj₂ y))) ,
            {!(λ w → (Σ-≡ (ul Γ (proj₁ w )) (q)))
              where
                q = begin
                  subst (𝒪 A) (ul Γ (proj₁ w)) (bsrc A (ref Γ (proj₁ w)) ((bref A (proj₁ w)) (proj₂ w)))
                  ≡⟨cong ( subst (𝒪 A) (ul Γ (proj₁ w))) (cong (ptul A (proj₁ w)) (proj₂ w))⟩
                  subst (𝒪 A) (ul Γ (proj₁ w)) (subst 𝒪 A (sym (ul Γ proj₁ w)) (proj₂ w))
                  ≡⟨subst-sym-cancel ((ul Γ (proj₁ w))⟩
                  proj₂ w
              ∎ where open ≡-Reasoning!} ,
           {! (λ w → (Σ-≡ (ur Γ (proj₁ w )) ( trans (cong ( subst (𝒪 A) (ur Γ (proj₁ w))) (cong (ptur A (proj₁ w)) (proj₂ w)))(subst-sym-cancel ((ur Γ (proj₁ w)))  ))!}
            ]
            
-}
-- Pi-type for reflexive graphs

-- app probably can be found somewhere else. Need for 
-- (p : f ≡ g) → ( q : x ≡ y) 
-- subst₂ app p q : f x ≡ g y

app : {A B : Set} → (f : A → B) → (x : A) → B
app f x = f x

-- also no memoy of where to find that:

-- transpPths : {A A' B B' : Set} → (A → A') → (B → B') → (A ≡ B → A' ≡ B')


{-comment this out for the moment because of the holes
PI : {Γ : RG} → (A : Ty Γ) → (B : Ty (Γ • A)) → Ty Γ
PI {Γ = Γ} A B = FamRG[ 𝒪PIAB , 𝓡PIAB , RefPIAB , SrcPIAB , TgtPIAB , PtulPIAB , PturPIAB ]
  where

    𝒪PIAB : o Γ -> Set
    𝒪PIAB x = Σ[ f ∈ (( a₀ : 𝒪 A x) → (𝒪 B (x , a₀)))
                    × ((a₁ : 𝓡 A (ref Γ x)) → (𝓡 B (ref Γ x , a₁))) ]
               (let (f₀ , f₁) = f in
                (
                ((a₀ : 𝒪 A x) →
                       bref B (x , a₀) (f₀ a₀) ≡ f₁ (bref A x a₀)) ×
                  ((a₁ : 𝓡 A (ref Γ x)) →
                       (subst (𝒪 B) (Σ-≡ (ul Γ x) refl)
                              (bsrc B (ref Γ x , a₁) (f₁ a₁))
                          ≡ f₀ (subst (𝒪 A) (ul Γ x) (bsrc A (ref Γ x) a₁))))
                          ) ×
                -- ((a₁ : 𝓡 A (ref Γ x)) →
                --    (bsrc B (ref Γ x , a₁) (f₁ a₁) ≡ f₀ bsrc A (ref Γ x) a₁))
                  ((a₁ : 𝓡 A (ref Γ x)) →
                       (subst (𝒪 B) (Σ-≡ (ur Γ x) refl)
                              (btgt B (ref Γ x , a₁) (f₁ a₁))
                          ≡ f₀ (subst (𝒪 A) (ur Γ x) (btgt A (ref Γ x) a₁)))))
    pr₁𝓡PIAB : r Γ → Set
    pr₁𝓡PIAB y = ((𝒪PIAB (src Γ y)) × (𝒪PIAB (tgt Γ y))) × ((a₁ : 𝓡 A y) → (𝓡 B (y , a₁)))
    pr₂𝓡PIAB : (y : r Γ) → pr₁𝓡PIAB y → Set
    pr₂𝓡PIAB y F = (let ((Fsrc , Ftgt) , r) = F in
                          ((a₁ : 𝓡 A y) →
                            bsrc B (y , a₁) (r a₁)
                                ≡ (proj₁ (proj₁ Fsrc)) (bsrc A y a₁)) ×
                          ((a₁ : 𝓡 A y) →
                            btgt B (y , a₁) (r a₁)
                                ≡ (proj₁ (proj₁ Ftgt)) (btgt A y a₁)))
    𝓡PIAB : r Γ → Set
    𝓡PIAB y = (Σ[ F ∈ (pr₁𝓡PIAB y) ] (pr₂𝓡PIAB y F))
    RefPIAB : (x : o Γ) → (𝒪PIAB x) → 𝓡PIAB (ref Γ x)
    RefPIAB x ((f₀ , f₁) , ((e₁ , e₂) , e₃)) =
      let Eq₁ : (a : 𝓡 A (ref Γ x)) → bsrc B (ref Γ x , a) (f₁ a) ≡ proj₁ (proj₁ (subst 𝒪PIAB (sym (ul Γ x)) ((f₀ , f₁) , ((e₁ , e₂) , e₃)))) (bsrc A (ref Γ x) a)
          Eq₁ a = begin
                    bsrc B (ref Γ x , a) (f₁ a)
                  ≡⟨ subst-transpose (Σ-≡ (ul Γ x) refl) (e₂ a) ⟩
                    subst (𝒪 B) (sym (Σ-≡ (ul Γ x) refl)) (f₀ (subst (𝒪 A) (ul Γ x) (bsrc A (ref Γ x) a)))
                  ≡⟨ congSubst (sym-Σ-≡-snd-refl (ul Γ x)) ⟩
                    subst (𝒪 B) (Σ-≡ (sym (ul Γ x)) (subst-sym'-subst (ul Γ x))) (f₀ (subst (𝒪 A) (ul Γ x) (bsrc A (ref Γ x) a)))
                  ≡⟨ sym (happly (subst-domcod' (ul Γ x)) (bsrc A (ref Γ x) a)) ⟩
                    subst (λ z → (a₀ : 𝒪 A z) → 𝒪 B (z , a₀)) (sym (ul Γ x)) f₀ (bsrc A (ref Γ x) a)
                  ≡⟨ happly (subst-natural (proj₁ ∘ proj₁) (sym (ul Γ x))) (bsrc A (ref Γ x) a) ⟩
                    proj₁ (proj₁ (subst 𝒪PIAB (sym (ul Γ x)) ((f₀ , f₁) , ((e₁ , e₂) , e₃)))) (bsrc A (ref Γ x) a)
                  ∎

          Eq₂ : (a₁ : 𝓡 A (ref Γ x)) → btgt B (ref Γ x , a₁) (f₁ a₁) ≡ proj₁ (proj₁ (subst 𝒪PIAB (sym (ur Γ x)) ((f₀ , f₁) , (e₁ , e₂) , e₃))) (btgt A (ref Γ x) a₁)
          Eq₂ a = begin
                    btgt B (ref Γ x , a) (f₁ a)
                  ≡⟨ subst-transpose (Σ-≡ (ur Γ x) refl) (e₃ a) ⟩
                    subst (𝒪 B) (sym (Σ-≡ (ur Γ x) refl)) (f₀ (subst (𝒪 A) (ur Γ x) (btgt A (ref Γ x) a)))
                  ≡⟨ congSubst (sym-Σ-≡-snd-refl (ur Γ x)) ⟩
                    subst (𝒪 B) (Σ-≡ (sym (ur Γ x)) (subst-sym'-subst (ur Γ x))) (f₀ (subst (𝒪 A) (ur Γ x) (btgt A (ref Γ x) a)))
                  ≡⟨ sym (happly (subst-domcod' (ur Γ x)) (btgt A (ref Γ x) a)) ⟩
                    subst (λ z → (a₀ : 𝒪 A z) → 𝒪 B (z , a₀)) (sym (ur Γ x)) f₀ (btgt A (ref Γ x) a)
                  ≡⟨ happly (subst-natural (proj₁ ∘ proj₁) (sym (ur Γ x))) (btgt A (ref Γ x) a) ⟩
                    proj₁ (proj₁ (subst 𝒪PIAB (sym (ur Γ x)) ((f₀ , f₁) , ((e₁ , e₂) , e₃)))) (btgt A (ref Γ x) a)
                  ∎

      in ((subst 𝒪PIAB (sym (ul Γ x)) ((f₀ , f₁) , (e₁ , e₂) , e₃) ,
           subst 𝒪PIAB (sym (ur Γ x)) ((f₀ , f₁) , (e₁ , e₂) , e₃)) , f₁) ,
           (Eq₁ , Eq₂)

-}
           
{-comment this out for the moment because of the holes
    SrcPIAB : (y : r Γ) → (𝓡PIAB y) → 𝒪PIAB (src Γ y)
    SrcPIAB = {!!}
    TgtPIAB :  (y : r Γ) → (𝓡PIAB y) → 𝒪PIAB (tgt Γ y)
    TgtPIAB y = {!!}
    PtulPIAB : (x : o Γ) → (SrcPIAB (ref Γ x) ∘ (RefPIAB x) ≡ subst 𝒪PIAB (sym (ul Γ x)))
    PtulPIAB = {!!}
    PturPIAB : (x : o Γ) → (TgtPIAB (ref Γ x) ∘ (RefPIAB x) ≡ subst 𝒪PIAB (sym (ur Γ x)))
    PturPIAB x = {!!}
    -}

{- --here ul / ur substitution everywhere  ( λ x → (λ f → ( (proj₁ f , proj₁ f) , proj₂ (proj₁ f) ) , proj₁ (proj₂ f))) ,
 {! ( λ x → (λ f → (
                             (
                             ( ext ( λ c → (subst (𝒪 B (sym (ul (Γ • A) (x , ((proj₁ (proj₁ f)) (subst (𝒪 A) (ul Γ x))) c)))))) ( (proj₁ (proj₁ f)) ( subst (𝒪 A) (ul Γ x)) c)) ,--f₀
                             {!right verion of previous line!}--f₁
                             ) ,
                             ({copy of (f₀ , f₁)}) , 
                             ) , {proj₁ f}) , proj₂ (proj₁ f) ) ,
                                       proj₁ (proj₂ f))) !},

...

              
               {! (λ y → (λ w → (proj₁ w)))!} ,
               {! (λ y → (λ w → (proj₂ w)))!} ,
                {!ptulPIAB!} ,
                {!!}
-}

{-
a term of 𝓡 (PI A B) y
has the form
(((( ( f₀ˢ , f₁ˢ) , ( (e₁ˢ , e₂ˢ) , e₃ˢ) ) , ( f₀ᵗ , f₁ᵗ) , ( (e₁ᵗ , e₂ᵗ) , e₃ᵗ) ) , r ) , (Eq₁ , Eq₂))

since a term of (𝒪PIAB x)
has the form
( ( f₀ , f₁) , ( (e₁ , e₂) , e₃) )
-}

{-
ptulPIAB x = )bsrc PI A B (ref Γ x)) ∘ (bref PI A B)
≡⟨⟩
subst 𝒪 PI A B (sym  (ul Γ x))
-}

{-
((f₀ , fᵣ) , (f₀ , fᵣ) , fᵣ)
f₀ = ( (bref B (src Γ ref Γ x)) ∘ (proj₁ (proj₁ f)) ∘ (bsrc A (ref Γ x)) , bref ∘ (proj₂ (proj₁ f)))
-}

-- IR

{-for IR we need the following
RGRG = the collection of reflexive graphs as a (large) reflexive graph.
-}


-- cuts
{-(subst (𝒪 A) (sym (ul Γ x)) a₁)
transport-subst-cd : {A B B' : Set} → ( q : B ≡ B') → {x y : A → B} → (p : x ≡ y) → (subst-cd q x ≡ subst-cd q y)
transport-subst-cd q p = ap (subst-cd q) p
-}

{-
the problem with this notation is that mixfix operators do not take implicit arguments
_[_]Ty : {Γ Δ : RG} → Ty Δ → (HomRG Γ Δ ) → Ty Γ
A [ f ]Ty = FamRG[ (𝒪 A) ∘ (𝒻0 f) ,
(𝓡 A) ∘ (𝒻1 f) , (λ x → ((subst (𝓡 A) (sym ((𝒻ref f) x))) ∘ ((bref A) ((𝒻0 f) x)))) ,
(λ y → (subst (𝒪 A) ((𝒻src f)y)) ∘ (bsrc A)((𝒻1 f) y)) ,
(λ y → (subst (𝒪 A) ((𝒻tgt f) y)) ∘ (btgt A)((𝒻1 f) y)) ,
(λ x →  ap (subst-cd unsubst3 {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f}) (ptul A ((𝒻0 f) x))) ,
(λ x →  ap (subst-cd unsubst3r {Γ = Γ}{Δ = Δ}{A = A}{x = x}{f = f}) (ptur A ((𝒻0 f) x))) ]
-}

{-
we should have pointwise bref A [ f ] x ≡ bref A ((𝒻0 f) x) , but
for typing reasons we cannot take this as definition of the field
-}


{-
A = 𝒪 A (𝒻0 f x)
B = 𝒪 A src Δ (ref Δ ((𝒻0 f) x))
B' = 𝒪 A [ f ]Ty (src Γ (ref Γ x))
q = sym unsubst3
x =  ((brsc A (ref A ((𝒻0 f) x)) ∘ (bref A ((𝒻0 f) x))))
y = subst 𝒪 A (sym (ul Δ ((𝒻0 f) x ))))
p = ptul A ((𝒻0 f) x)
then q will be
transport-subst-cd A B B' q x y p = q = ptul (A [ f ]Ty x)
-}

{-
-- internally

mutual
   data rg : Set → Set → Set
     oo : rg
     or :rg
     si : or → oo
     ti : or → oo

   data : 
     
-}


