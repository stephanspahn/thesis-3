module HankMonadStuff where

------------------------------------------------------------------------
data One : Set where
  sole : One

Rec-One : (C : One -> Set)-> C sole -> (z : One)-> C z
Rec-One C c sole = c

It-One :  (C : Set)-> C -> One -> C
It-One C = Rec-One (λ _ → C)

It-One' :  (C : Set)-> C -> One -> C
It-One' C c _ = c

const : {C : Set}-> C -> One -> C
const {C} = It-One' C

-------------------------------------------------------------------------
data W (A : Set) (B : A -> Set) : Set where
  Wsup : (a : A) -> (B a -> W A B) -> W A B

Rec-W : (A : Set)->(B : A -> Set)->
        (C : W A B -> Set)->
        ( (a : A)->(f : B a -> W A B)->
          ((b : B a)-> C (f b))->
          C (Wsup a f))->
        (z : W A B)-> C z
Rec-W A B C c (Wsup a f) = c a f (λ b → Rec-W A B C c (f b))

It-W : (A : Set)->(B : A -> Set)->
        (C : Set)->
        ( (a : A)-> (B a -> C)-> C)->
        W A B -> C
It-W A B C c = Rec-W A B (λ _ → C) (λ a _ f → c a f)

-----------------------------------------------------------------
data Sig (A : Set) (B : A -> Set) : Set where
  pair : (a : A) -> B a -> Sig A B

curry : (A : Set)-> (B : A -> Set)->
           (C : Sig A B -> Set)->
           (f : (z : Sig A B)-> C z)->
           ((a : A)-> (b : B a)-> C (pair a b))
curry A B C f a b = f (pair a b)

Rec-Sig : (A : Set)-> (B : A -> Set)->
           (C : Sig A B -> Set)->
           ((a : A)-> (b : B a)-> C (pair a b))->
           (z : Sig A B)-> C z
Rec-Sig A B C c (pair a b) = c a b

uncurry : (A : Set)-> (B : A -> Set)->
           (C : Sig A B -> Set)->
           ((a : A)-> (b : B a)-> C (pair a b))->
           (z : Sig A B)-> C z
uncurry = Rec-Sig

It-Sig : (A : Set)-> (B : A -> Set)->
         (C : Set)->
         ((a : A)-> (b : B a)-> C)->
         Sig A B -> C
It-Sig A B C c  = Rec-Sig A B (λ _ → C) c
It-Sig'  : {A : Set}-> {B : A -> Set}->
         (C : Set)->
         ((a : A)-> (b : B a)-> C)->
         Sig A B -> C
It-Sig' {A} {B} = It-Sig A B

-- large versions of the above
REC-Sig : (A : Set)-> (B : A -> Set)->
           (C : Sig A B -> Set₁)->
           ((a : A)-> (b : B a)-> C (pair a b))->
           (z : Sig A B)-> C z
REC-Sig A B C c (pair a b) = c a b

IT-Sig : (A : Set)-> (B : A -> Set)->
         (C : Set₁)->
         ((a : A)-> (b : B a)-> C)->
         Sig A B -> C
IT-Sig A B C c  = REC-Sig A B (λ _ → C) c
IT-Sig'  : {A : Set}-> {B : A -> Set}->
         (C : Set₁)->
         ((a : A)-> (b : B a)-> C)->
         Sig A B -> C
IT-Sig' {A} {B} = IT-Sig A B

pi0 : {A : Set}-> {B : A -> Set}-> Sig A B -> A
pi0 {A} {B} = -- It-Sig A B A (λ a _ → a)
              It-Sig' A (λ a _ → a)

{-  aha! If B is not constant, requires full elimination.
pi1-wrong : {A : Set}-> {B : A -> Set}-> (p : Sig A B) -> B (pi0 p)
pi1-wrong {A} {B} p = It-Sig A B (B (pi0 p)) {!!} p
-}
pi1 : {A : Set}-> {B : A -> Set}-> (p : Sig A B) -> B (pi0 p)
pi1 {A} {B}  =  let f : Sig A B → Set
                    f =  λ z → B (pi0 z)
                in
                Rec-Sig A B f    -- (λ p → B (pi0 p))
                        (λ _ b → b)
                -- Rec-Sig A B (λ p → B (pi0 p)) (λ _ b → b)
                -- curry (B (pi0 p)) (λ _ b → b) {!!}
                -- curry {A} {λ _ → B (pi0 p)}  (B (pi0 p)) ( (λ _ b → b)) {!!}
------------------------------------------------------------
-- Some container shit

-- action on objects
_<|_ : (S : Set)-> (S -> Set) -> Set -> Set
(S <| P) X = Sig S (λ s → P s -> X)

-- action on morphisms
_<|m_ : (S : Set)-> (P : S -> Set) -> (X Y : Set)-> (X -> Y) -> (S <| P)X -> (S <| P)Y
(S <|m P) X Y m = It-Sig S (λ s → P s → X) (Sig S (λ s → P s → Y)) (λ s f → pair s (λ p → m (f p)))

-- composition of containers.
cnt-comp-shape : (A : Set) -> (A -> Set) -> (C : Set) -> (C -> Set) -> Set
cnt-comp-shape A B C _ = (A <| B) C
cnt-comp-posn : (A : Set) -> (B : A -> Set) -> (C : Set) -> (D : C -> Set) -> cnt-comp-shape A B C D -> Set
cnt-comp-posn A B C D (pair a f) = Sig (B a) (λ b → D (f b))


-- representation of container nt
cnt : (A : Set) -> (A -> Set) -> (C : Set) -> (C -> Set) -> Set
cnt A B C D = (a : A)-> (C <| D) (B a)

[_<|_]=> : (A : Set) -> (A -> Set) -> (Set -> Set) -> Set
[ A <| B ]=> F = (a : A)-> F (B a)

cnt' : (A : Set) -> (A -> Set) -> (C : Set) -> (C -> Set) -> Set
cnt' A B C D = [ A <| B ]=> (C <| D)

-- decode representation to nt
cnt2nt : (A : Set) -> (B : A -> Set) -> (C : Set) -> (D : C -> Set) ->
         cnt' A B C D -> (X : Set) -> (A <| B) X -> (C <| D) X
cnt2nt A B C D t X abx = let a : A
                             a = pi0 abx
                             b2x : B a → X
                             b2x = pi1 abx
                             c : C
                             c = pi0 (t a)
                             d2b : D c -> B a
                             d2b = pi1 (t a)
                         in pair  c ( λ d → b2x (d2b d))

cnt2nt-gen : (A : Set) -> (B : A -> Set) ->
          (F : Set -> Set) -> (Fm : (X Y : Set)->(X -> Y)-> F X -> F Y) ->
         ([ A <| B ]=> F) -> (X : Set) -> (A <| B) X -> F X
cnt2nt-gen A B F Fm t X
   = It-Sig A (λ s → B s → X)
             (F X)
             (λ a f  → Fm (B a) X f (t a) )

cnt'2nt : (A : Set) -> (B : A -> Set) -> (C : Set) -> (D : C -> Set) ->
         cnt' A B C D -> (X : Set) -> (A <| B) X -> (C <| D) X
cnt'2nt A B C D = cnt2nt-gen A B (C <| D) (C <|m D)

-- encode nt as representation. (Not used.)
nt2cnt : (A : Set) -> (B : A -> Set) -> (C : Set) -> (D : C -> Set) ->
        ( (X : Set) -> (A <| B) X -> (C <| D) X ) -> cnt A B C D
nt2cnt A B C D t a =  t (B a) (pair a (λ x → x))

------------------------------------------------------------------
module M1 (A : Set) (B : A -> Set) where    -- cf Dybjer-Setzer
  -- ordinary W type, except with distinguished leaf node.
  -- ie, W((1 <| 1) + (A <| B))
  -- I cannot be arsed to express that all in a low level way with W.
  data W1 : Set where -- trees grow with a new root
    leaf : W1
    node : (a : A)-> (B a -> W1) -> W1

  -- set of paths from the root to a leaf
  -- note: requires large iteration (into Set)
  -- this could be defined, by some masochist, using degenerate IR
  P1 : W1 -> Set
  P1 leaf       = One
  P1 (node a f) = Sig (B a) (λ b → P1 (f b))

  F1 : Set -> Set
  F1 = (W1 <| P1)

  F1^2 : Set -> Set
  F1^2 = ((W1 <| P1) W1) <|  (λ x → Sig (P1 (pi0 x)) (λ p → P1 (pi1 x p)))

  F1m : (X : Set) -> (Y : Set) ->
      (X -> Y) -> F1 X -> F1 Y
  F1m X Y m (pair w f) = pair w (λ p → m (f p))

  -- Investigate the monadicity of (W1 A B <| P1 A B)
  -- I am content to just define the eta and mu.  Laws are too much effort.
  eta1 : (X : Set)-> X -> F1 X
  eta1 X x = pair leaf (const x)

  -- warm up ...
  shape : (w : W1) -> (P1 w -> W1) -> W1
  shape leaf f       = f sole
  shape (node a x) f = node a (λ b → shape  (x b)
                                            (λ p → f (pair b p)) )
  -- ... for this:
  shape' : (X : Set)-> (w : W1) -> (P1 w -> F1 X) -> F1 X
  shape' X leaf f       = f sole
  shape' X (node a x) f = let v : B a -> F1 X
                              v b = shape' X     -- recurse
                                          (x b)
                                          (λ p → f (pair b p))
                              v0 : B a -> W1
                              v0 b = pi0 (v b)
                              v1 : (b : B a) -> P1 (v0 b) -> X
                              v1 b = pi1 (v b)
                              w : Sig (B a) (λ b → P1 (v0 b)) → X
                              w = It-Sig (B a) (λ b → P1 (v0 b)) X v1
                          in pair (node a v0) w

  -- ... of which the following is a bureaucratic rearrangement
  mu1 :  (X : Set)-> F1 (F1 X) -> F1 X
  mu1 X = It-Sig W1 (λ w → P1 w → F1 X)
                      (F1 X)
                      (shape' X)

  blah : (X : Set)-> F1^2 X -> F1 (F1 X)
  blah    X arg = let arg0 : (W1 <| P1) W1
                      arg0 = pi0 arg
                      w  : W1
                      w = pi0 arg0
                      f : P1 w → W1
                      f = pi1 arg0
                      arg1 : Sig (P1 w) (λ p → P1 (f p)) → X
                      arg1 = pi1 arg
                      arg1' : (p : P1 w)-> P1 (f p) -> X
                      arg1' p p' =  arg1 (pair p p')
                  in pair w (λ p → pair (f p) (arg1' p))

  halb : (X : Set)-> F1 (F1 X) -> F1^2 X
  halb X ff = let w : W1
                  w = pi0 ff
                  fw : P1 w -> F1 X
                  fw = pi1 ff
                  f : P1 w -> W1
                  f p = pi0 (fw p)
                  v : (p : P1 w)-> P1 (f p) -> X
                  v p = pi1 (fw p)
              in pair (pair w f) (It-Sig (P1 w) (λ p → P1 (f p)) X v)

  mu1-alt : (X : Set)-> F1^2 X -> F1 X
  mu1-alt X x = mu1 X (blah X x)

-- let's see how to represent this nt

  mu1' : (w : W1)-> (f : P1 w -> W1) -> (W1 <| P1)(Sig (P1 w) (λ p → P1 (f p)))
  mu1' leaf f = pair (f sole) (pair sole)
  mu1' (node a x) f = let
                          f' : (b : B a)-> P1 (x b) → W1
                          f' b p = f (pair b p)
                          P' : (b : B a) → Set
                          P' b =   Sig (P1 (x b)) (λ p → P1 (f' b p))
                          u : (b : B a) -> (W1 <| P1)(P' b)
                          u b = mu1' (x b) (f' b)
                          v0 : B a -> W1
                          v0 b = pi0 (u b)
                          v1 : (b : B a)-> P1 (v0 b) -> P' b
                          v1 b = pi1 (u b)
                          v10 : (b : B a)-> (p : P1 (v0 b)) -> P1 (x b)
                          v10 b p = pi0 (v1 b p)
                          v11 : (b : B a)-> (p : P1 (v0 b)) -> P1 (f (pair b (v10 b p)))
                          v11 b p = pi1 (v1 b p )
                          w : W1
                          w = node a v0
                          w' : P1 w  → Sig (P1 (node a x)) (λ p → P1 (f p))
                          w' = It-Sig (B a)
                                      (λ b → P1 (v0 b))
                                      (Sig (P1 (node a x)) (λ p → P1 (f p)))
                                      (λ b z → pair (pair b (v10 b z)) (v11 b z))
                       in pair w w'

  mu1'' : cnt ((W1 <| P1)W1) (λ x → Sig (P1 (pi0 x)) (λ p → P1 (pi1 x p)))
               W1 P1
  mu1'' x = mu1' (pi0 x) (pi1 x)

-- Strangely, I cannot define that using It-Sig.
-- So I defined a large version of it above, and tried again.
-- And failed...
{-
  mu1''' : cnt ((W1 <| P1)W1) (IT-Sig W1 (λ s → P1 s → W1) Set (λ p f → (W1 <| P1) (Sig (P1 p) (λ p' → P1 (f p')))))
               W1             P1
  mu1''' = {!!} -- Rec-Sig W1 (λ s → P1 s → W1) (IT-Sig W1 (λ s → P1 s -> W1) (Sig W1 (λ s → P1 s → W1) → Set) {!!} {!!})  {!!} x
-}

  mu1''' : (X : Set)-> F1^2 X -> F1 X
  mu1''' = cnt'2nt ((W1 <| P1) W1) (λ z → Sig (P1 (pi0 z)) (λ p → P1 (pi1 z p)))   -- F1^2
                   W1              P1                                              -- F1
                   mu1''                                                           -- rep of (F1^2 -.-> F1)

--------------------------------------------------------------

module M2 (A : Set) (B : A -> Set) where    -- cf Irish

  mutual
    data W2 : Set where  -- trees grow anywhere
      u : W2
      l : A -> W2
      m : (w : W2)-> (P2 w -> W2) -> W2

    P2 : W2 -> Set
    P2 u        = One
    P2 (l a)    = B a
    P2 (m w w') = Sig (P2 w) (λ p → P2 (w' p))

  F2 : Set -> Set
  F2 = (W2 <| P2)

  -- F2 is I presume a monad (up to natural equivalence, but I won't try to prove the laws).
  eta2 : (X : Set)-> X -> F2 X
  eta2 X x = pair u (const x)

  mu2 : (X : Set)-> F2 (F2 X) -> F2 X
  mu2 X (pair a x) = let x0 : P2 a -> W2
                         x0 pa = pi0 (x pa)
                         x1 : (pa : P2 a) -> P2 (x0 pa) → X
                         x1 pa = pi1 (x pa)
                         v : W2
                         v = m a x0
                         w : P2 v -> X
                         w  =  It-Sig (P2 a) (λ pa → P2 (x0 pa)) X x1
                     in pair v w

module M2' (A : Set) (B : A -> Set) where    -- cf Irish

  mutual
    data A* : Set where  -- trees grow anywhere
      one : A*                  -- at root
      eta : A -> A*             -- at leaves
      mu  : (A* <| B*) A* -> A* -- in the middle

    B* : A* -> Set
    B* one = One
    B* (eta a) = B a
    B* (mu (pair a x)) = Sig (B* a) (λ p → B* (x p))

--    B* (eta a)    = B a
--    B* (mu w w')  = Sig (B* w) (λ p → B* (w' p))

  FM : Set -> Set
  FM X = (A* <| B*) X

  -- F2 is I presume a monad (up to natural equivalence, but I won't try to prove the laws).
  eta2 : (X : Set)-> X -> FM X
  eta2 X x = pair one (const x)      -- had to put in "one" for this

  mu2 : (X : Set)-> FM (FM X) -> FM X
  mu2 X (pair a x) = let x0 : B* a -> A*
                         x0 pa = pi0 (x pa)
                         x1 : (pa : B* a) -> B* (x0 pa) → X
                         x1 pa = pi1 (x pa)
                         v : A*
                         v = mu (pair a x0)
                         w : B* v -> X
                         w  =  It-Sig (B* a) (λ pa → B* (x0 pa)) X x1
                     in pair v w

  -- we have two trivial natural transformations:

  nt-eta : [ A <| B ]=> FM
  nt-eta = λ a → pair (eta a) (λ z → z)

  nt-mu : -- [ FM A* <| IT-Sig A* (λ a → B* a → A*) Set (λ a b → Sig (B* a) (λ x → B* (b x)))  -- self-comp of A*<|B*
          [ FM A* <| IT-Sig' Set (λ a b → Sig (B* a) (λ x → B* (b x)))  -- self-comp of A*<|B*
          ]=> FM
--  nt-mu (pair a x) = pair (mu (pair a x)) (λ z → z)
  nt-mu w@(pair _ _) = pair (mu w) (λ z → z)

module translate-1-2 (A : Set) (B : A -> Set) where
  open M1 A B
  open M2 A B

  1-2 : W1 -> W2
  1-2 M1.leaf = u
  1-2 (M1.node a f) = m (l a) (λ b → 1-2 (f b))

  1-2' : (w1 : W1) -> P1 w1 -> P2 (1-2 w1)
  1-2' M1.leaf        = λ p → p
  1-2' (M1.node a f)  = let thing : (a₁ : B a) (b : P1 (f a₁)) →
                                       Sig (P2 (l a)) (λ p₁ → P2 (1-2 (f p₁)))
                            thing = λ b pfb → pair b (1-2' (f b) pfb)
                        in It-Sig (B a) (λ b → P1 (f b)) (Sig (B a) (λ b → P2 (1-2 (f b)))) thing

  -- essentially, define 1-2 and 1-2' at the same time.
  xua : (w1 : W1) -> F2 (P1 w1)  -- F1 --.--> F2   Quite easy.
  xua M1.leaf = pair u (const sole)
  xua (M1.node a x) = let r : B a -> W2
                          r b = pi0 (xua (x b))
                          s : (b : B a)-> P2 (r b) → P1 (x b)
                          s b = pi1 (xua (x b))
                          w2 : W2
                          w2 = m (l a) r
                          t : P2 w2 -> Sig (B a) (λ b → P1 (x b))  -- (b : B a)-> P2 (r b)
                          t = It-Sig (B a) (λ b → P2 (r b)) (Sig (B a) (λ v → P1 (x v)))
                                     (λ b y → pair b (s b y))
                      in pair w2 t

  -- like above, but in the opposite direction.
  aux : (w2 : W2) -> F1 (P2 w2)  -- F2 --.--> F1   Need to use that F1 (= (A<|B)^*) is a monad
  aux M2.u = pair leaf (λ x → x)
  aux (M2.l a) = pair (node a (λ _ → leaf)) pi0
  aux (M2.m w2 f2) = let r : W1
                         r = pi0 (aux w2)
                         s : P1 r → P2 w2
                         s = pi1 (aux w2)
                         t : P2 w2 -> W1
                         t p2 = pi0 (aux (f2 p2))
                         u : (p2 : P2 w2) -> P1 (t p2) → P2 (f2 p2)
                         u p2 = pi1 (aux (f2 p2))
                         t' : P1 r -> W1
                         t' p1 = t (s p1)
                         u' : (p1 : P1 r) -> P1 (t' p1) → P2 (f2 (s p1))
                         u' p1 = u (s p1)
                         P2P2 : Set
                         P2P2 = Sig (P2 w2) (λ p2 → P2 (f2 p2))
                         hm-wrong : F1 (F1 P2P2)
                         hm-wrong = pair r (λ x → pair (t' x) (λ y → pair (s x) (u' x y) )) -- (u' {!!} {!!})))
                         hm-right : F1^2 P2P2
                         hm-right = pair (pair r t') (λ x → pair (s (pi0 x)) (u' (pi0 x) (pi1 x)))
                         hm' : F1 P2P2
                         hm' = cnt'2nt ((W1 <| P1)W1) ((λ z → Sig (P1 (pi0 z)) (λ p → P1 (pi1 z p)))) -- F1^2
                                      W1             P1                                               -- F1
                                      mu1''   P2P2
                                      hm-right
                         hm'' = mu1''' P2P2 hm-right
                     in hm' -- could be hm''

{- So we have natural transformations between F1 and F2 and vice-versa.
   Two natural questions: (a) are these morphisms between monads?
   (b) are F1 and F2 naturally isomorphic? Or is aux a retraction and xua its section?
-}

  -- as xua is to 1-2 and 1-2', so aux is to 2-1 and 2-1'
  2-1 : W2 -> W1
  2-1 w2 = pi0 (aux w2)

  2-1' : (w2 : W2) -> P1 (pi0 (aux w2)) → P2 w2
  2-1' w2 = pi1 (aux w2)

module M3 (A : Set) (B : A -> Set) where   -- cf uniform IR

  mutual
    data W3 : Set where -- trees grow at the leaves
      u : W3
      ext : (w : W3)-> (P3 w -> A) -> W3

    P3 : W3 -> Set
    P3 u = One
    P3 (ext w a) = Sig (P3 w) (λ p → B (a p))

  example1 : A -> W3
  example1 a = ext u (const a)
  example2 : (a : A) -> (B a -> A) -> W3
  example2 a b2a = ext (example1 a)
                       (It-Sig (P3 u) (λ p → B (const a p)) A (const b2a))

  F3 : Set -> Set
  F3 = (W3 <| P3)

  eta3 : (X : Set)-> X -> F3 X
  eta3 X x = pair u (const x)

{-  Pretty obvious this doesn't exist.
  mu3 : (X : Set)-> F3 (F3 X) -> F3 X
  mu3 X ff = {!!}
-}



