{- Some inconclusive noodling around with DS IIR, or one of its variants. -}
{- Possibly the most fucked up piece of half-remembered Agda you will see in your life. -}
open import Prelude.Basic
open import Prelude.Container
open import Prelude.Fam

module Misc.HankDSIIR where 

  record ICont (I : Set₁) : Set₁ where  -- NB I is here *large*.
    constructor IC
    field
      C : Set
      R : C -> Set
      n : (c : C)-> R c -> I
  open ICont

  ICont2Quant : {I : Set₁}->
     (ic : ICont I)-> (P : I -> Set) -> Set
  ICont2Quant {I} ic P = Σ (ic .C) (λ c → (r : ic .R c )-> P (ic .n c r))

  ICont2Quant' : {I : Set₁}->    -- large version of foregoing. Must learn how to use levels...
     (ic : ICont I)-> (Q : I -> Set₁) -> Set₁
  ICont2Quant' {I} ic Q = Σ (ic .C) (λ c → (r : ic .R c )-> Q (ic .n c r))

  ICont2Quantm : {I : Set₁}->    -- mapping (small to large)
     (ic : ICont I)->
     (P : I -> Set) -> (Q : I -> Set₁) -> ((i : I)-> P i -> Q i) ->
     ICont2Quant ic P -> ICont2Quant' ic Q
  ICont2Quantm {I} ic P Q inc (c , f) = (c , (λ r → inc (ic .n c r) (f r)) )

  local : {I : Set₁}-> {D : I -> Set₁} -> 
     (ut : (i : I)-> Fam (D i)) -> (ic : ICont I)-> Fam (ICont2Quant' ic D)
  local {I} {D} ut ic
                  = let U : I -> Set
                        U i = (ut i).ind
                        T : (i : I)-> U i -> D i 
                        T i = (ut i).fib 
                    in
                    ( ICont2Quant {I} ic U 
                    , ICont2Quantm {I} ic U D T )

{- Let's try to define the free monad over (ICont I <| H |-> ICont2Quant {I} H D) -}

  data DSIIR-shape (I : Set₁) (D : I -> Set₁) : Set₁ where
    leaf : DSIIR-shape I D
    fork : (H : ICont I)->
           ((K : ICont2Quant' {I} H D)-> DSIIR-shape I D)->
           DSIIR-shape I D

  -- dammit, where is unit?
  record Unit : Set₁ where
    constructor Blah
  record unit : Set where
    constructor blah
    
  DSIIR-posn : (I : Set₁)-> (D : I -> Set₁)-> DSIIR-shape I D -> Set₁
  DSIIR-posn I D leaf = Unit
  DSIIR-posn I D (fork H K) = Σ (ICont2Quant' {I} H D) (λ h → DSIIR-posn I D (K h))


  -- the (repn of the nt underlying the) monad map from that free monad to Fam.
  local' : {I : Set₁}-> {D : I -> Set₁} -> 
     (ut : (i : I)-> Fam (D i)) ->
     (t : DSIIR-shape I D) ->
     Fam (DSIIR-posn I D t)
  local' {I} {D} ut leaf = (unit , const Blah)
  local' {I} {D} ut (fork H K)
     = let  thing : -- (y : Σ (H .C) (λ c → (r : H .R c) → D (H .n c r))) →
                    (y : ICont2Quant' H D) -> 
                      Fam (DSIIR-posn I D (K y))  
            thing y = local' {I} {D} ut (K y)
            foo : Fam (ICont2Quant' H D)
            foo = local {I} {D} ut H
            U : Set
            U = foo .ind
            T : U -> ICont2Quant' H D
            T = foo .fib
       in ( Σ U (λ u →  ( thing (T u) ).ind)
          , uncurry (λ u y → (T u , thing (T u) .fib y))
          )

{-
-- dammit. Have to simplify this. Do a warm up.
  module _ (A : Set) (B : A -> Set₁) where
    data W+ : Set₁ where
      lf : W+ 
      fk : (a : A) -> (B a -> W+) -> W+
    P+ : W+  -> Set₁
    P+ lf       = Unit
    P+ (fk a f) = Σ (B a) (λ b → P+ (f b))
    simp : (l : (a : A)-> Fam (B a)) -> (t : W+)-> Fam (P+ t)
    simp l lf = (unit , const Blah)
    simp l (fk a f) = let thing  : (b : B a) → Fam (P+ (f b)) 
                          thing b = simp l (f b)
                          foo : Fam (B a)
                          foo = l a
                          U : Set
                          U = foo .ind
                          T : U → B a
                          T = foo .fib
                      in ( Σ U (λ u → (thing (T u)).ind)
                         , uncurry (λ u y → (T u , thing (T u) .fib y))
                         )
-}

  record gDSIIR (I : Set₁) (D : I -> Set₁) (E : Set₁) : Set₁ where  -- some kind of container application...
    constructor mk
    field
      shape : DSIIR-shape I D
      content  : DSIIR-posn I D shape -> E
  open gDSIIR

  DSIIR : (I : Set₁)-> (D : I -> Set₁)->
          (J : Set₁)-> (E : J -> Set₁) -> Set₁
  DSIIR I D J E = (j : J) -> gDSIIR I D (E j)

  interp : (I : Set₁)-> (D : I -> Set₁)->
           (J : Set₁)-> (E : J -> Set₁)->
          DSIIR I D J E -> ((i : I)-> Fam(D i)) -> (j : J)-> Fam(E j)
  interp I D J E gamma ut j = let g0 : DSIIR-shape I D
                                  g0 = (gamma j).shape
                                  g1 : DSIIR-posn I D (gamma j .shape) → E j
                                  g1 = (gamma j).content
                                  thing : Fam (DSIIR-posn I D g0)
                                  thing = local' {I} {D} ut g0 
                              in (thing .ind) , (λ x → g1 (thing .fib x))
