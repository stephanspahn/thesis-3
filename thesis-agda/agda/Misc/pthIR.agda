module Misc.pthIR where

open import DS.IR
open import Prelude.Basic
open import Data.Unit
open import Data.Empty
open import Data.Bool hiding (T)
open import Data.Nat
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality
open import Prelude.Equality
open import Prelude.Fam

open ≡-Reasoning

mutual
   data pthIR0 (D : Set₁) : Set₁ where
     idPN : pthIR0 D
--     idPN' : (c : pthIR0 D) → (Dec c → pthIR0 D) -- special case ot Pth
     con : (A : Set) -> pthIR0 D
     sigma  : (S : pthIR0 D) → (Dec S → pthIR0 D) → pthIR0 D
     pi  : (A : Set) → (A → pthIR0 D) → pthIR0 D
     Pth : (c : pthIR0 D)(x y : Dec {D = D} c) → IntI D c x y → pthIR0 D

   Dec : {D : Set₁} -> pthIR0 D -> Set₁
   Dec {D} idPN = D
--   Dec {D} (idPN' c x) = x ≡ x
   Dec (con A) = Lift A
   Dec (sigma S T) = Σ[ x ∈ (Dec S) ] Dec (T x)
   Dec (pi A T) = (x : A) -> Dec (T x)
   Dec (Pth c x y p) = x ≡ y

   data IntI (D : Set₁) (c : pthIR0 D) : (Dec {D = D} c) →  (Dec {D = D} c)  →  Set where
     reflI : (x : Dec {D = D} c) → IntI D c x x

   DecIntI : (D : Set₁) → (c : pthIR0 D) → (x : Dec {D = D} c) → (y : Dec {D = D} c) → IntI D c x y → x ≡ y
   DecIntI D c x .x (reflI .x) = refl

   ⟦Pth⟧ :  {D : Set₁} → (c : pthIR0 D) → (i : ( x y : Dec c) → IntI D c x y) → (UT : Fam D) → (a b : ⟦ c ⟧0 UT) → a ≡ b
   ⟦Pth⟧ c (λ {x .x → reflI x) UT a .a  = refl


{-
''data'' circle : Set where
  base : circle
  loop : base ≡ base
  -}
  

--higher paths may depend on lower ones


pthIR : (D E : Set1) -> Set1
pthIR D E = Σ[ c ∈ pthIR0 D ] (Dec c -> E)

mutual
  ⟦_⟧0 :{D : Set₁} → pthIR0 D → Fam D → Set
  ⟦ idPN ⟧0 F = ind F
  ⟦ idPN' c x ⟧0 F = ind F
  ⟦ con A ⟧0 F = A
  ⟦ sigma S T ⟧0 F = Σ[ s ∈ ⟦ S ⟧0 F ] ⟦ (T (⟦ S ⟧1 F s)) ⟧0 F
  ⟦ pi A T ⟧0 F = (x : A) -> ⟦ (T x) ⟧0 F
  ⟦ Pth c x y i ⟧0 F ={! x ≡ y!}

  ⟦_⟧1 :  {D : Set₁} → (c :  pthIR0 D) → (UT : Fam D) → ⟦ c ⟧0 UT → Dec c
  ⟦ idPN ⟧1 F x = fib F x
  ⟦ (con A) ⟧1 F a = lift a
  ⟦ (sigma S T) ⟧1 F (s , x) = ⟦ S ⟧1 F s , ⟦ (T (⟦ S ⟧1 F s)) ⟧1 F x
  ⟦ (pi A T) ⟧1 F g = λ a → ⟦ (T a) ⟧1 F (g a)
  ⟦ Pth c x y i ⟧1 F = {!subst T!} 


⟦ _ ⟧I : {D E : Set₁} → (c :  pthIR D E) → ( x y : Dec {D = D} (proj₁ c)) → (i : IntI D (proj₁ c) x y) → (UT : Fam D) → (a b : ⟦ c ⟧0 UT) → a ≡ b
⟦ Pth c x y i ⟧I UT a b = 

{-
⟦⟧I : {D E : Set₁} → (c :  pthIR D E) → (UT : Fam D) → ( x y : Dec {D = D} (proj₁ c)) → (i : IntI D (proj₁ c) x y) → (a b : ⟦ c ⟧0 UT) → a ≡ b
⟦ ⟧I idPN UT  x y i a b  =
-}
--x .x (reflI .x) a .a =  refl -- Failed to infer the value of dotted pattern

{-
⟦ _ ⟧pth0 : (c : pthIR D E) → (UT : Fam D) → (a b : ⟦ c ⟧0 UT) → a ≡ b

⟦ _ ⟧pth1 : (c : pthIR D E) → (UT : Fam D) → (a b : ⟦ c ⟧0 UT) → (p : a ≡ b) → (⟦ c ⟧1 UT a ≡ ⟦ c ⟧0 UT b)
-}
