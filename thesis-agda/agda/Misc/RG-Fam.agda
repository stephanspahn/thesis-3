{-# OPTIONS --allow-unsolved-metas #-}
module Misc.RG-Fam where

open import Prelude.Basic
open import Data.Unit
open import Data.Empty
open import Data.Bool hiding (T)
open import Data.Nat
open import Data.Sum
open import Data.Product
open import Function
open import Relation.Binary.PropositionalEquality
open import Prelude.Equality

open ≡-Reasoning

-- reflexive graphs

record RG : Set₁ where
  constructor RG[_,_,_,_,_,_,_]
  field
    o : Set
    r : Set
    ref : o → r
    src : r → o
    tgt : r → o
    ul : (x : o) → src ( ref x) ≡ x
    ur : (x : o) → tgt ( ref x) ≡ x
open RG public

-- (notice the difference between 𝒻 (Mcf) and 𝓯 (MCf))
record HomRG (Γ Δ : RG) : Set₁ where
  constructor RG→[_,_,_,_,_,]--_,_]
  field
    𝒻0 : (o Γ) → (o Δ)
    𝒻1 : r Γ → r Δ
    𝒻ref : (x : o Γ) → 𝒻1 (ref Γ x) ≡ ref Δ (𝒻0 x)
    𝒻src : (y : r Γ) → src Δ ( 𝒻1 y) ≡ 𝒻0 (src Γ y)  
    𝒻tgt : (y : r Γ) → tgt Δ ( 𝒻1 y) ≡ 𝒻0 (tgt Γ y)
--    𝒻ul : (x : o Γ) → ul Γ x ≡ ul Δ (𝒻0 x)
--    𝒻ur : (x : o Γ) → ur Γ x ≡ ur Δ (𝒻0 x)
open HomRG public

-- we can derive the action of 𝒻 on the ul and ur fields
-- more precisely it should probably be part of the definition HomRG
-- that ul Γ x be sent to ul Δ 𝒻f x under this function

𝒻ul : {Γ Δ : RG} → {x : o Γ} → (f : HomRG Γ Δ) → (src Γ (ref Γ x) ≡ x) → (src Δ (ref Δ ((𝒻0 f) x)) ≡ ((𝒻0 f) x))
𝒻ul {Γ = Γ} {Δ = Δ} {x = x} f = (λ p → trans (trans (cong (src Δ) (sym ((𝒻ref f) x))) ((𝒻src f) (ref Γ x))) p) ∘ ( λ w → cong (𝒻0 f) w)

𝒻ur : {Γ Δ : RG} → {x : o Γ} → (f : HomRG Γ Δ) → (tgt Γ (ref Γ x) ≡ x) → (tgt Δ (ref Δ ((𝒻0 f) x)) ≡ ((𝒻0 f) x))
𝒻ur {Γ = Γ} {Δ = Δ} {x = x} f = (λ p → trans (trans (cong (tgt Δ) (sym ((𝒻ref f) x))) ((𝒻tgt f) (ref Γ x))) p) ∘ ( λ w → cong (𝒻0 f) w)

-- families of reflexive graphs

record FamRG (Γ : RG) : Set₁ where
  constructor FamRG[_,_,_,_,_,_,_]
  field
    𝒪 : o Γ → Set
    𝓡 : r Γ → Set
    bref : (x : o Γ) → (𝒪 x → 𝓡 (ref Γ x))
    bsrc : (y : r Γ) → (𝓡 y → 𝒪  (src Γ y))
    btgt : (y : r Γ) → (𝓡 y → 𝒪  (tgt Γ y))
    ptul : (x : o Γ) → (bsrc (ref Γ x) ∘ (bref x) ≡ subst 𝒪 (sym (ul Γ x)))
    ptur : (x : o Γ) → (btgt (ref Γ x) ∘ (bref x) ≡ subst 𝒪 (sym ( ur Γ x)))
open FamRG public

{- intended "...≡ id", but x != src Γ (ref Γ x) of type o Γ
when checking that the expression id has type
(x₁ : 𝒪 x) → 𝒪 (src Γ (ref Γ x))
-}


record FamRG⇒ (Γ : RG) (A B : FamRG Γ) : Set₁ where
  constructor FamRG⇒[_,_,_,_,_]
  field
    m𝒪 : (x : o Γ) → (𝒪 A x → 𝒪 B x)
    m𝓡 : (y : r Γ) → (𝓡 A y →   𝓡 B y)
    mref : (x : o Γ) → (m𝓡 (ref Γ x)) ∘ (bref A x) ≡ (bref B x) ∘ (m𝒪 x)
    msrc : (y : r Γ) → (m𝒪 (src Γ y)) ∘ (bsrc A y) ≡ (bsrc B y) ∘ (m𝓡 y)
    mtgt : (y : r Γ) → (m𝒪 (tgt Γ y)) ∘ (btgt A y) ≡ (btgt B y) ∘ (m𝓡 y)
open FamRG⇒

paste : {A0 A1 B0 B1 C0 C1 : Set} →
        {fA : A0 → A1} → {fB : B0 → B1} → {fC : C0 → C1} →
        {d01 : A0 → B0} → {d02 : B0 → C0} → {d11 : A1 → B1} → {d12 : B1 → C1} →
        (p1 : d11 ∘ fA ≡ fB ∘ d01) → (p2 : d12 ∘ fB ≡ fC ∘ d02) →
        d12 ∘ d11 ∘ fA ≡ fC ∘ d02 ∘ d01
paste {d01 = d01} {d12 = d12} p1 p2 = trans (cong (λ z → d12 ∘ z) p1)
                                                (cong (λ z → z ∘ d01) p2)

flatpaste : {A B B' C : Set} → {f : A → B} → {f' : B → C} →
            {g : A → B} → {g' : B → C} →
            (q1 : f ≡ g) → (q2 : f' ≡ g') →
            f' ∘ f ≡ g' ∘ g
flatpaste {f' = f'} {g = g'} q1 q2 = trans (cong (λ z → f' ∘ z) q1) (cong (λ z → z ∘ g') q2)

ap : ∀{l}{ X Y : Set l} → {x x' : X} → (f : X → Y) → x ≡ x' → (f x) ≡ (f x')
ap f refl = refl

∘FamRG⇒ : {Γ : RG}{A B C : FamRG Γ} → FamRG⇒ Γ B C → FamRG⇒ Γ A B → FamRG⇒ Γ A C
∘FamRG⇒ {Γ = Γ} {A} {B} {C} g f
  = FamRG⇒[ (λ x → (((m𝒪 g) x) ∘ ((m𝒪 f) x))) ,
            ( λ y → ((m𝓡 g y) ∘ (m𝓡 f y))) ,
            (λ x → paste {fA = bref A x} {bref B x} {bref C x}
                         {m𝒪 f x} {m𝒪 g x}
                         {m𝓡 f (ref Γ x)} {m𝓡 g (ref Γ x)}
                         (mref f x) (mref g x)) ,
            (λ y → paste {fA = bsrc A y} {bsrc B y} {bsrc C y}
                         {m𝓡 f y} {m𝓡 g y}
                         {m𝒪 f (src Γ y)} {m𝒪 g (src Γ y)}
                         (msrc f y) (msrc g y)) ,
            (λ y → paste {fA = btgt A y} {btgt B y} {btgt C y}
                         {m𝓡 f y} {m𝓡 g y}
                         {m𝒪 f (tgt Γ y)} {m𝒪 g (tgt Γ y)}
                         (mtgt f y) (mtgt g y))]


-- homogenous family of reflexive graphs

record homgFamRG (Γ : RG) : Set₁ where
  constructor homgFamRG[_,_,_,_,_,_,_]
  field
    𝒪h : o Γ → Set
    𝓡h : r Γ → Set
    brefh : (x : o Γ) → (𝒪h x → 𝓡h (ref Γ x))
    bsrch : (y : r Γ) → (𝓡h y → 𝒪h  (src Γ y))
    btgth : (y : r Γ) → (𝓡h y → 𝒪h  (tgt Γ y))
    homgh : (y : r Γ) → (𝓡h y → (𝒪h  (src Γ y) ≡ 𝒪h (tgt Γ y)))
    hreflh : (y : r Γ) → (𝓡h y → (𝒪h  (src Γ y) ≡ 𝒪h (tgt Γ y))) → (𝒪h  (src Γ y) →  𝓡h (y))
