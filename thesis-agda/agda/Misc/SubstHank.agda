open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam
open import Prelude.Container

module Misc.SubstHank where

mutual
  data Abs (A : Set){B : A → Set} : Set₁ where
    eta* : A → Abs A
    mu* : ((Abs A) ◃ (Dex {A} B)) (Abs A {B}) → Abs A

  Dex : {A : Set} → (B : A → Set) → (Abs A {B}) -> Set₁
  Dex B (eta* a) = Lift (B a)
  Dex {A} B (mu* (a , f)) = Σ[ x ∈ (Dex {A} B a) ] (Dex {A} B (f x))

Subs : {A : Set} → (A → Set) → (E : Set₁) → Set₁
Subs {A} B E = Σ[ x ∈ Abs A {B} ] (Dex {A} B x → E)

bindSubs : {C E : Set₁} → {A : Set} → (B : A → Set) → Subs B C → (C → Subs B E) → Subs B E
bindSubs B (c , α) h
  = mu* (c , proj₁ ∘ h ∘ α) , (λ { (x , y ) → proj₂ (h (α x)) y })
