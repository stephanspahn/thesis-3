{-# OPTIONS --allow-unsolved-metas #-}
module Misc.Indexed.IIR where

open import Prelude.Basic
open import Prelude.Equality

open import Misc.Indexed.IFam

mutual
   data IIrish {I : Set}(D : I -> Set₁) : Set₁ where
     idi : I -> IIrish D
     con : (A : I -> Set) -> IIrish D
     sigma  : (S : IIrish D) → ((i : I) -> IInfo S i → IIrish D) → IIrish D
     pi  : (A : Set) → (A → IIrish D) → IIrish D

   IInfo : {I : Set}{D : I -> Set₁} -> IIrish D -> I -> Set₁
   IInfo {D = D} (idi o) i = D o
   IInfo (con A) i = Lift (A i)
   IInfo (sigma S T) i = Σ[ x ∈ (IInfo S i) ] IInfo (T i x) i
   IInfo (pi A T) i = (x : A) -> IInfo (T x) i

IIR : {I J : Set}(D : I -> Set1)(E : J -> Set1) -> Set1
IIR {I} {J} D E = Σ[ c ∈ IIrish D ] Σ[ α ∈ (J -> I) ] ((j : J) -> IInfo c (α j) -> E j)



module _ {I : Set}{D : I -> Set1} where

  mutual

    Node : (T : IIrish D) -> IFam D -> I -> Set
    Node (idi o) (U , T) i = U o
    Node (con A) Z i = A i
    Node (sigma S T) Z i = Σ[ s ∈ Node S Z i ] Node (T i (info S Z i s)) Z i
    Node (pi A S) Z i = (a : A) -> Node (S a) Z i

    info : (T : IIrish D) -> (F : IFam D) -> (i : I) -> Node T F i -> IInfo T i
    info (idi o) (U , T) i x = T o x
    info (con A) Z i x = lift x
    info (sigma S T) Z i (s , x) = (info S Z i s) , info (T i (info S Z i s)) Z i x
    info (pi A S) Z i g = λ a → info (S a) Z i (g a)

irish : {I J : Set}{D : I -> Set1}{E : J -> Set1} -> IIR D E -> IFam D -> IFam E
irish (c , α , β) F = IFam-map α β (Node c F , info c F)

irish₀ : {I J : Set}{D : I -> Set1}{E : J -> Set1} -> IIR D E -> IFam D -> J -> Set
irish₀ c Z = ind (irish c Z)

irish₁ : {I J : Set}{D : I -> Set1}{E : J -> Set1} -> (c : IIR D E) -> (Z : IFam D) -> (j : J) -> irish₀ c Z j -> E j
irish₁ c Z = fib (irish c Z)

-- IIR D is functorial
IIR-map : {I J : Set}{D : I -> Set₁}{E E' : J -> Set₁} -> ((j : J) -> E j -> E' j) -> IIR D E  -> IIR D E'
IIR-map f (c , α , β) = (c , α , (λ j → f j ∘ (β j)))

-- formation and introduction rules

module _ {I : Set}{D : I -> Set1}{c : IIR D D} where

  mutual

    {-# NO_POSITIVITY_CHECK #-}
    data U  : I -> Set where
      intro : (i : I) -> irish₀ c (U , T) i -> U i

    {-# TERMINATING #-}
    T : (i : I) -> U i -> D i
    T i (intro .i x) = irish₁ c (U , T) i x

open import Prelude.Basic

mutual
  data UU : ℕ -> Set where
    zero : UU 0
    suc : (n : ℕ) -> (x : UU n) -> (TT n x -> UU (suc n)) -> UU n

  TT : (n : ℕ) -> UU n -> Set
  TT .0 zero = ⊤
  TT n (suc .n x xbar) = Σ[ s ∈ TT n x ] TT (suc n) (xbar s)

II : Set
II = ℕ

DD : II -> Set₁
DD _ = Set

insist-on : {I : Set}{D : I -> Set1} -> I -> IIrish D
insist-on o = con (λ i → i ≡ o)

code : IIrish DD
code = sigma (con (λ _ → Bool))
             (λ _ → λ { (lift false) → insist-on 0
                      ; (lift true) → sigma (con (λ _ → ℕ))
                                            (λ _ n → sigma (insist-on (lower n)) (λ _ _ → sigma (idi (lower n))
                                                        (λ i TTn → pi TTn (λ _ → (idi (suc (lower n))))))) })

decode : (j : II) → IInfo code j → DD j
decode j (lift false , y) = ⊤
decode j (lift true , (lift n) , _ , TTn , f) = Σ[ x ∈ TTn ] (f x)

all : IIR DD DD
all = code , id , decode

z : U {c = all} 0
z = intro 0 (false , refl)

s : (n : ℕ) -> (x : U {c = all} n) -> (T {c = all} n x -> U {c = all} (suc n)) -> U {c = all} n
s n x f = intro n (true , n , {!!} , x , f)


{-
-- monadic

Irish-μ : {I J : Set}{D : I -> Set1}{E : J -> Set₁} ->
          (c : IIrish D) -> (α : J -> I) (β : (i : I) -> IInfo c i -> IIR D E) ->
          IIrish D
Irish-μ c α β = sigma c (λ i x → proj₁ (β i x))

{-
Info-μ : {D E : Set1} -> (c : Irish D) -> (α : Info c -> IR D E) ->
         Info (Irish-μ c α) -> E
Info-μ c α (x , y) = proj₂ (α x) y

-}

IR-μ : {I J : Set}{D : I -> Set₁}{E : J -> Set₁} -> IIR D (λ j → IIR D E) -> IIR D E
IR-μ (c , α , β) = Irish-μ c α {!!} , {!!}
-}

--ss-- indexed DS

data IDS {I : Set}(D : I -> Set₁) : Set₁ where
  iiota : (i : I) -> (D i) -> IDS D
  isigma : (A : Set) -> (A -> IDS D) -> IDS D
  idelta : (A : Set)(i : A -> I) -> (((a : A) -> D (i a)) -> IDS D) -> IDS D

