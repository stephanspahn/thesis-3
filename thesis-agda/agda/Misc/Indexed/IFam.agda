module Misc.Indexed.IFam where

-- Indexed families

open import Prelude.Basic
open import Prelude.Equality

record IFam {I : Set}(D : I -> Set₁) : Set₁ where
  constructor _,_
  field
    ind : I -> Set
    fib : (i : I) -> ind i -> D i
open IFam public

IFam-map : ∀ {I J D D'} -> (g : J -> I) -> ((j : J) -> D (g j) -> D' j) -> IFam D -> IFam D'
IFam-map g h (A , f) = A ∘ g , (λ j → h j ∘ (f (g j)))

