module Misc.free-category where

open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam

{-Weber, FAMILIAL 2-FUNCTORS AND PARAMETRIC RIGHT ADJOINTS, p. 671, gives an example of a parametric right adjoint functor which is not a polynomial functor: the functor assigning to a graph the free category on it. This functor should be definable by inductio-recursion.

A graph is a family (E, P) : Fam(V × V). The free category FC(E,P) is given by a degenerate inductive-recursive definition.

-}
module _ {V : Set₁}{X : Fam (V × V)} where

  mutual
    data E' : Set₁ where
      ids : V → E'
      edg  : (ind X) → E'
      paths : (e : E') → ( e' : E') → (proj₁ (P' e) ≡ proj₂ (P' e)) → E'

    P' : E' → (V × V)
    P' (ids v) = (v , v)
    P'( edg e) = (fib X) e
    P'(paths p q r) = (proj₁ (P' p) , proj₂ (P' q))


{-
This functor has the coproduct $\DS\; V\;V\times V$ code with summands

\[
  \sigma V \lambda v \to \iota (v,v) 
  \]
  \[
    \sigma E \lambda e \to \iota (P e)
    \]
\[
  \delta 1 \lambda X \to \delta 1 \lambda Y \to \sigma (\proj_1 Y(*) \equiv \proj_2 X(*))) \lambda z \to \iota (\proj_1 X(*) , \proj_2 Y(*))
\]

-}
