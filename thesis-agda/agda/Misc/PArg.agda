module Misc.PArg where
open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Equivalences
open import Prelude.Fam
open import Prelude.Container

-- a two-level version of DS extended by a path constructor

mutual
  data PCont (D : Set₁) : Set₁ where
    iot  : (X : Cont{lzero}) → PCont D
    ct : LCFam D (PCont D) → PCont D
    ptho : (K : PCont D) → Ptho {D = D} K → PCont D

  data Ptho {D : Set₁}(K : PCont D) : Set₁ where --gives an internal path-space object / identity type
    ho : PPos K → Ptho K
    inh : PPos K → PPos K → Ptho K
    cano : Ptho K

  ptho' : {D : Set₁} → (K : PCont D) → Ptho {D = D} K → PCont D
  ptho' = ptho

  PPos : {D : Set₁} → (PCont D) → Set₁
  PPos {D = D} (iot X) = cont{lzero}{lzero} X D
  PPos {D = D} (ct (Q , h)) = Σ[ x ∈ cont Q D ] (PPos (h x))
  PPos {D = D} (ptho K (ho x)) = x ≡ x
  PPos {D = D} (ptho K (inh x y)) = x ≡ y
  PPos {D = D} (ptho K cano) = {!S^1!} -- here should go something with a canonical element

  data PCont₁ {D : Set₁}(K : PCont D) : Set₁ where
    ko : PPos K → PCont₁ K
    pth* : PPos K → PPos K → PCont₁ K --constructor adding codes for a new free path, i.e. base case for inductive paths
    pth :(x y : PPos K) → x ≡ y → PCont₁ K --constructor adding codes for known path

  PPos₁ : {D : Set₁}{K : PCont D} → (PCont₁ {D = D} K) → (w : Ptho {D = D} K) → PPos {D = D} (ptho' K w) --decodes codes for paths to actual paths
  PPos₁ {D = D} {K = K} (ko x) w = {!refl!}
  PPos₁ {D = D} {K = K} (pth* x y) p = {!!} -- would need a canonical term here, to define probably by induction on K
  PPos₁ {D = D} {K = K} (pth x y q) p = {!p!}
 
    
{-first attempt
  PPos₁ : {D : Set₁}{K : PCont D} → (PCont₁ {D = D} K) → (x y : PPos {D = D} K) → x ≡ y
  PPos₁ {D = D} {K = K} (ko x) a b = refl
  PPos₁ {D = D} {K = K} (pth x y) p = p
  -}
  
{-
mutual
  data PCont (D : Set₁) : Set₁ where
    iot  : (X : Cont{lzero}) → PCont D
    ct : LCFam D (PCont D) → PCont D
  PPos : {D : Set₁} → (PCont D) → Set₁
  PPos {D = D} (iot X) = cont{lzero}{lzero} X D
  PPos {D = D} (ct (Q , h)) = Σ[ x ∈ cont Q D ] (PPos (h x))
-- up to size issues, Stephan's pth can be encoded
pth : {D : Set₁} -> (K : PCont D) → PPos K → PPos K → PCont D
pth K x y = ct (((x ≡ y) , (λ _ → ⊥)) , (λ _ → iot K1-Cont))
-- it has the right decoding
-- PPos {D = D} (pth K x y) = x ≡ y
-- (abusing Fam 1 = Set to use existing equivalence machinery)
pth-decoding : ∀ {D : Set₁} {K} {x} {y} -> _≃_ {D = ⊤} ((PPos {D = D} (pth K x y), _)) (((x ≡ y) , _))
pth-decoding  = record { left = ((λ { ((p , _) , _) → p }) , (λ _ → refl))
                       ; right = ((λ p → (p , (λ ())) , _ , (λ ())) , (λ _ → refl))
                       ; leftright = refl
                       ; rightleft = ⇒-≡ p (ext (λ x → happly (subst-const p) x)) }
  where p = (ext (λ x → Σ-≡ (Σ-≡ refl (ext (λ ()))) (Σ-≡ refl (ext (λ ())))))
PathDS : (D E : Set₁) → Set₁
PathDS D E = Σ[ Q ∈ PCont D ] (PPos Q -> E)
-}
