open import Prelude.Basic
open import Prelude.Container
open import Prelude.Fam
-- open import Data.Unit

module Misc.HankIrish (D : Set₁) where    -- don't want to carry D around

{- Unneeded.
-- need this for some size issues.
data Σ' (S : Set)(T : S -> Set₁) : Set₁ where
  inΣ' : (s : S) -> (t : T s) -> Σ' S T
-}
record ⊤' : Set₁ where
  constructor tt'

mutual
    data A*  : Set₁ where
      eta  : Cont{lzero} -> A*                        -- contains containers at the leaves
      mu   : (a : A*)  -> (f : B* a -> A*)-> A*     -- codes of (autonomous) Sigma type
      pi   : (I : Set) -> (f : I -> A*) -> A*      -- from Conor: codes of products of set-indexed families
      sig  : (I : Set) -> (f : I -> A*) -> A*      -- experimental: codes of coproducts of set-indexed families
      exp  : (I : Set) -> A* -> A*                -- experimental "non-dependent" pi
      tag  : (I : Set) -> A* -> A*                -- experimental "non-dependent" sigma
      _times_ : A* -> A* -> A*
      _plus_  : A* -> A* -> A*
      one*    : A*
      zero*   : A*

    B* : A* -> Set₁
    B* (eta C)   =  cont C D
    B* (mu a f)  =  Σ (B* a) (λ x → B* (f x))
    B* (pi I f)  =  ( i : I) → B* (f i)         -- Pi for a set-indexed family of large things
    B* (sig I f) =  Σ I (λ i → B* (f i)) -- (B* ∘ f) -- (λ i → B* (f i))       -- similar Sigma . Σ' is for a Set indexed family of Set₁s.
    B* (exp I a) =  I → B* a
    B* (tag I a) =  I × B* a
    B* (a times a') = B* a × B* a'
    B* (a plus a') = B* a ⊎ B* a'
    B* one*        = ⊤'
    B* zero*       = ⊥

-- infix 4 _local_

_local_ : Fam D -> (a : A*)-> Fam (B* a)
(U , T) local (eta C) = (cont C U , cont-map C T ) -- the heart of IR
X local z@(mu a f) =
                    let phi : (x : B* a)-> Fam (B* (f x))
                        phi x = X local (f x)
                        lxa : Fam (B* a)
                        lxa = X local a
                        I : Set
                        I = ind lxa
                        t : I -> B* a
                        t = fib lxa
--                        U : Set
--                        U = Σ I (λ i → ind (phit i))
--                        zz : ( i : I)-> ind (phit i) -> B* z   -- Σ (B* a) (λ x → B* (f x)) = B* z = B* (mu a f)
--                        zz i y = (t i , fib (phit i) y)
                    in ( Σ I (ind ∘ phi ∘ t)
                       , uncurry ( λ i y → (t i , (fib ∘ phi ∘ t) i y) ))

X local z@(pi I f) =
                   let phi : (i : I) -> Fam (B* (f i))
                       phi i = X local f i
                       Iphi : I -> Set
                       Iphi i = ind (phi i)
                       dphi : (i : I) -> Iphi i → B* (f i)
                       dphi i = fib (phi i)
--                       U : Set
--                       U = (i : I)-> Iphi i
--                       t : U -> B* z                            -- (i : I) -> B* (f i) = B* z = B* (pi I f)
--                       t u = λ i → dphi i (u i)                 -- S dphi u
                   in  ( ((i : I)-> Iphi i)
--                       , λ u i → dphi i (u i) )
                       , (_ˢ_ dphi )  )           -- there's actually a symbol in Function



-- a non-dependent version of pi. Probably the same as pi with a singleton family.
X local z@(exp I a) =
                   let phi  : Fam (B* a)
                       phi  = X local a
                       Iphi : Set
                       Iphi = ind phi
                       dphi : Iphi  → B* a
                       dphi = fib phi
--                       U : Set
--                       U = I -> Iphi
--                       t : U -> B* z                            --  I -> B* a = B* z = B* (exp I a)
--                       t u = λ i → dphi (u i)
                   in  ( (I -> Iphi) , (λ u → dphi ∘ u) )  -- That's actually (S dphiᴷ u) ) --(  λ u i → dphi (u i) )

X local z@(sig I f) =
                   let phi : (i : I) -> Fam (B* (f i))
                       phi i  = X local (f i)
                       Iphi : I -> Set
                       Iphi i = ind (phi i)
                       dphi : (i : I) -> Iphi i → B* (f i)
                       dphi i = fib (phi i)
--                       U : Set
--                       U = Σ I Iphi
--                       dphi' : (u : U) → B* (f (u .proj₁))
--                       dphi' = uncurry dphi
--                       t : U -> B* z                               -- Σ' I (λ i → B* (f i)) = B z =  B* (sig I f)
--                       t u = inΣ' (u .proj₁) (dphi' u)
                   in  ( (Σ I Iphi)
                       , uncurry (λ i y → (i , ((fib ∘ phi) i y)))
                       )      -- uncurry (λ i y → inΣ' i (dphi i y)) )

-- a non-dependent version of sig. Probably the same as sig with a singleton family
X local z@(tag I a) =
                   let phi : Fam (B* a)
                       phi = X local a
                       Iphi : Set
                       Iphi = ind phi
                       dphi : Iphi → B* a
                       dphi = fib phi
--                       U : Set
--                       U = I × Iphi
--                       t : U -> B* z                               -- I × B* a = B* z =  B* (tag I a)
--                       t u = (u .proj₁) , dphi (proj₂ u)
                   in  ( (I × Iphi)
                       , uncurry ( λ i y → (i , fib phi y) )
                       )
-- a binary version of pi. Probably the same as pi with index 2.
X local z@(a times a') =
        let ta : Fam (B* a)
            ta = X local a
            ta' : Fam (B* a')
            ta' = X local a'
            U : Set
            U = ta .ind × ta' .ind
            t : U -> B* z
            t u =  (ta .fib (u .proj₁)) , (ta' .fib (u .proj₂ ))
        in U , t

-- a binary version of sig. Probably the same as sig with index 2.
X local z@(a plus a') =
        let ta : Fam (B* a)
            ta = X local a
            ta' : Fam (B* a')
            ta' = X local a'
            U : Set
            U = ta .ind ⊎ ta' .ind
            t : U -> B* z
            t =  [ (inj₁ ∘ (ta .fib)) , inj₂ ∘ (ta' .fib) ]
        in U , t

X local z@(one*) =  ( ⊤ , const tt' )
X local z@(zero*) = ( ⊥ , ⊥-elim )


-- decode the representation of the (large) container morphism (A* D <| B*)-.->Fam
-- to a polymorphic map. (Is there some large container module to import?)
decode : {E : Set₁}-> ((a : A*)-> Fam(B* a)) -> (a : A*)->  (f : B* a ->  E) -> Fam E
decode m a f = ((m a) .ind) , (λ x → f (m a .fib x))

-- semantics of this kind of IR.
interp : {E : Set₁}-> (a : A*)-> (f : B* a -> E)-> Fam D -> Fam E
interp a f X = decode (_local_ X) a f


record Program (X : Set₁) : Set₁ where
  constructor _,_
  field  code : A*
         content : B* code -> X

-- the interpreter in uncurried form.
interp' : {E : Set₁}-> Program E -> Fam D -> Fam E
interp' (code , content) = interp code content


-- (aleph,beth,gimel,daleth)=ℵℶℷℸ


