module Misc.Hank where
-- try to define "generalised church numerals"


open import Prelude.Basic
open import Prelude.Fam
open import Prelude.Equality

open import DS.IR
open import DS.Initial

-- see pp. 23-26 of
--   https://www.dropbox.com/s/5wi9dp73lgilrgw/ir2-2014-11-30.pdf?dl=0
-- One unusual feature: there are two initial algebras: one to define
-- a family of IR codes; then another, using a code manufactured from
-- that family.

OP : Set₁ -> Set₁
OP D = DS D D

mytype : Set₁
mytype = Set -> OP Set

_+OP_ : {D : Set1} -> OP D -> OP D -> OP D
c₀ +OP c₁ = DS.IR.σ Bool (λ b → if b then c₀ else c₁)

mycode₀ : OP mytype  -- this is a "degenerate"  code. ("T isn't used in defining U".). Just large iteration.
mycode₀ = (ι DS.IR.ι) +OP (δ ⊤ (λ Φ → ι (λ A → DS.IR.δ A (λ X → Φ tt (Σ A X))) ))

thingU : Set   -- should be something like Nat
thingU = DS.Initial.U {mytype} {mycode₀}

thingT : thingU -> mytype
thingT = DS.Initial.T {mytype} {mycode₀}

thingy : thingU -> OP Set
thingy u = thingT u ⊤

mycode₁ : OP Set      -- has "rank" ω possibly..  so maybe not uniform-izable)
mycode₁ = σ thingU thingy

-- Rewritten directly ----------------------------
Branch : Set -> ℕ -> DS Set Set
Branch A zero = ι A
Branch A (suc n) = δ A (λ X → Branch (Σ A X) n)

HankCode : DS Set Set
HankCode = σ ℕ (Branch ⊤)
--------------------------------------------------

-- Sanity check for rewritten code

toNat : thingU -> ℕ
toNat (intro (true , _)) = zero
toNat (intro (false , n , _)) = suc (toNat (n tt))

toBranch : (x : thingU) -> thingy x ≡ Branch ⊤ (toNat x)
toBranch x = toBranch' x ⊤
  where toBranch' : (x : thingU) -> (A : Set) -> thingT x A ≡ Branch A (toNat x)
        toBranch' (intro (true , _)) A = refl
        toBranch' (intro (false , n , _)) A
          = cong (δ A) (ext (λ X → toBranch' (n tt) (Σ A X)))

myix : Set
myix = DS.Initial.U {Set} {mycode₁}
mydx : myix → Set
mydx = DS.Initial.T {Set} {mycode₁}
myfam : Fam Set
myfam = myix , mydx

-- just renaming
HankIx : Set
HankIx = DS.Initial.U {Set} {HankCode}
HankDx : HankIx → Set
HankDx = DS.Initial.T {Set} {HankCode}
HankFam : Fam Set
HankFam = HankIx , HankDx

-- Now I need to check this is what I intended.
-- My Agda is so rusty I'm not sure how to do it ...

{- ThingU should be, essentially, Nat.
   thing n should be a code for the n'th functor in the series
   (U,T) |-> (Unit <| Unit)
   (U,T) |-> (U,T)
   (U,T) |-> (U,T) . (U,T)
   (U,T) |-> (U,T) . (U,T) . (U,T)
   ...
   of functors (which are containers) from Set to Set.
   (.) is container composition.

-}

-- this is not so easy to prove, as the proof is not obviously inductive
-- I've convinced myself of the equivalence for n <= 3 by hand

open import Prelude.Container

-- composition of containers
comp : ∀ {a b} → Cont {a} {b} -> Cont {a} {b} -> Cont {a ⊔ b} {b}
comp (S , P) (S' , P')
  = (Σ[ s ∈ S ] (P s -> S')) , (λ { (s , f) → Σ[ x ∈ (P s) ] P' (f x) })


iterComp : ℕ -> Fam Set -> Cont
iterComp zero SP = ⊤ , (λ _ → ⊤)
iterComp (suc n) (S , P) = comp (S , P) (iterComp n (S , P))

{-
-- failed attempt
Branch-map₀ : {A A' : Set} (f : A -> A') -> (n : ℕ) ->
             {Z : Fam Set} -> ⟦ Branch A' n ⟧₀ Z -> ⟦ Branch A n ⟧₀ Z
Branch-map₀ f zero x = x
Branch-map₀ f (suc n) (g , x) = g ∘ f , Branch-map₀ (map f id) n x

mutual

  thingProp₀ : (n : ℕ) -> (UT : Fam Set) -> ⟦ Branch ⊤ n ⟧₀ UT -> proj₁ (iterComp n UT)
  thingProp₀ zero (U , T) x = x
  thingProp₀ (suc n) (U , T) (u , x) = u _ , (λ t → thingProp₀ n (U , T) (Branch-map₀ (λ _ → _ , t) n x))


{-

Branch-map₁ : {A A' : Set} (f : A -> A') -> (n : ℕ) ->
             {Z : Fam Set} -> (x : ⟦ Branch A' n ⟧₀ Z) ->
             ⟦ Branch A n ⟧₁ Z (Branch-map₀ f n x) ≡ A
Branch-map₁ f zero x = refl
Branch-map₁ f (suc n) (g , x) = trans (Branch-map₁ (map f id) n x) {!!}

thingProp₀ : (n : ℕ) -> (UT : Fam Set) -> ⟦ Branch ⊤ n ⟧₀ UT -> ind (iterComp n UT)
thingProp₀ zero (U , T) x = x
thingProp₀ (suc n) (U , T) (u , x) = u _ , (λ t → thingProp₀ n (U , T) (Branch-map₀ (λ _ → (_ , t)) n x))

thingProp₁ : (n : ℕ) -> (UT : Fam Set) -> (x : ⟦ Branch ⊤ n ⟧₀ UT) -> ⟦ Branch ⊤ n ⟧₁ UT x ≡ fib (iterComp n UT) (thingProp₀ n UT x)
thingProp₁ zero UT x = refl
thingProp₁ (suc n) UT (u , x) = trans {!!} (trans (thingProp₁ n UT (Branch-map₀ {!!} n x)) {!!})
-}
-}


-- a "spuriously rank-omega" code

padOnes : DS Set Set -> ℕ -> DS Set Set
padOnes c zero = c
padOnes c (suc n) = σ ⊤ (λ _ → padOnes c n)

padded : DS Set Set -> DS Set Set
padded c = σ ℕ (padOnes c)

padEq₀ : {c : DS Set Set} -> (n : ℕ) -> {Z : Fam Set} ->
         ⟦ padOnes c n ⟧₀ Z -> ⟦ c ⟧₀ Z
padEq₀ zero x = x
padEq₀ (suc n) (_ , x) = padEq₀ n x

padEq₁ : {c : DS Set Set} -> (n : ℕ) -> {Z : Fam Set} ->
         (x : ⟦ padOnes c n ⟧₀ Z) ->
         ⟦ padOnes c n ⟧₁ Z x ≡ ⟦ c ⟧₁ Z (padEq₀ n x)
padEq₁ zero x = refl
padEq₁ (suc n) (_ , x) = padEq₁ n x
