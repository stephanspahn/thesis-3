{-# OPTIONS --type-in-type #-} -- need large FamRG; hack for now
{-# OPTIONS --no-termination-check #-}

open import Misc.RG-Fam

module Misc.DSRG {Γ : RG} where

open import Prelude.Equality
open import Prelude.Basic


Γ₀ = RG.o Γ
Γ₁ = RG.r Γ
Γr = RG.ref Γ
Γs = RG.src Γ
Γt = RG.tgt Γ


_⇒_ : (A B : FamRG Γ) -> FamRG Γ
_⇒_ FamRG[ A₀ , A₁ , Ar , As , At , Asr , Atr ]
     FamRG[ B₀ , B₁ , Br , Bs , Bt , Bsr , Btr ]
     = FamRG[ A⇒B₀
            , A⇒B₁
            , (λ  γ₀ f₀₁ → let (f₀ , f₁ , p , q , _) = f₀₁ in
                             subst A⇒B₀ (sym (ul Γ γ₀)) f₀₁ ,
                             subst A⇒B₀ (sym (ur Γ γ₀)) f₀₁ ,
                             f₁ ,
                             (λ y → trans
                                      (p y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ul Γ γ₀)))
                                          (subst-natural proj₁ (sym (ul Γ γ₀))))
                                        (As (ref Γ γ₀) y))) ,
                             (λ y → trans
                                      (q y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ur Γ γ₀)))
                                          (subst-natural proj₁ (sym (ur Γ γ₀))))
                                        (At (ref Γ γ₀) y))))
            , (λ { γ₁ (fs , ft , R , _)  → fs })
            , (λ { γ₁ (fs , ft , R , _)  → ft })
            , (λ γ₀ → refl)
            , (λ γ₀ → refl)
            ]
  where A⇒B₀ : Γ₀ -> Set
        A⇒B₀ γ₀
         = Σ[ f₀ ∈ (A₀ γ₀ -> B₀ γ₀) ]
           Σ[ f₁ ∈ (A₁ (Γr γ₀) -> B₁ (Γr γ₀)) ]
           (∀ y →
              Bs (Γr γ₀) (f₁ y)
                ≡ subst B₀ (sym (ul Γ γ₀))
                           (f₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y) ))) ×
           (∀ y →
              Bt (Γr γ₀) (f₁ y)
                ≡ subst B₀ (sym (ur Γ γ₀))
                           (f₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y) ))) ×
           (∀ x → Br γ₀ (f₀ x) ≡ f₁ (Ar γ₀ x))
        A⇒B₁ : Γ₁ -> Set
        A⇒B₁ γ₁ = Σ[ f01src ∈ A⇒B₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                   Σ[ f01tgt ∈ A⇒B₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                   Σ[ R ∈ (A₁ γ₁ -> B₁ γ₁) ]
                   (∀ y → Bs γ₁ (R y) ≡ f₀s (As γ₁ y)) ×
                   (∀ y → Bt γ₁ (R y) ≡ f₀t (At γ₁ y))))

module _ (D : FamRG Γ) where

  D₀ =     FamRG.𝒪 D
  D₁ = FamRG.𝓡 D
  Dr = FamRG.bref D
  Ds = FamRG.bsrc D
  Dt = FamRG.btgt D

  mutual

    DS : FamRG Γ
    DS = FamRG[ DS₀ , DS₁ , DSr , DSs , DSt ,
                (λ γ₀ → ext (DSptul γ₀)) , (λ γ₀ → ext (DSptur γ₀)) ]

    data DS₀ (γ₀ : Γ₀) : Set1 where
      ι₀ : D₀ γ₀ -> DS₀ γ₀

      σ₀ : (A : FamRG Γ) ->
           (let FamRG[ A₀ , A₁ , Ar , As , At , Asr , Atr ] = A in
           (f₀ : A₀ γ₀ -> DS₀ γ₀) ->
           (f₁ : A₁ (Γr γ₀) -> DS₁ (Γr γ₀)) ->
           (p : ∀ y →
                      DSs (Γr γ₀) (f₁ y)
                                   ≡ subst DS₀ (sym (ul Γ γ₀))
                                                   (f₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y)))) ->
           (q : ∀ y →
                                                      DSt (Γr γ₀) (f₁ y)
                                   ≡ subst DS₀ (sym (ur Γ γ₀))
                                                   (f₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y)))) ->
           (r : ∀ x → DSr γ₀ (f₀ x) ≡ f₁ (Ar γ₀ x)) -> DS₀ γ₀)
      δ₀ : (A : FamRG Γ) -> --FamRG.𝒪 ((A ⇒ D) ⇒ DS) γ₀ -> DS₀ γ₀
           (f₀ : (FamRG.𝒪 (A ⇒ D)) γ₀ -> DS₀ γ₀) ->
           (f₁ : (FamRG.𝓡 (A ⇒ D)) (Γr γ₀) -> DS₁ (Γr γ₀)) ->
           (p : ∀ y →
                                       DSs (Γr γ₀) (f₁ y)
                                         ≡ subst DS₀ (sym (ul Γ γ₀))
                                                     (f₀ (subst (FamRG.𝒪 (A ⇒ D)) (ul Γ γ₀) ((FamRG.bsrc (A ⇒ D)) (Γr γ₀) y) ))) ->
           (q : ∀ y →
                                       DSt (Γr γ₀) (f₁ y)
                                         ≡ subst DS₀ (sym (ur Γ γ₀))
                                                     (f₀ (subst (FamRG.𝒪 (A ⇒ D)) (ur Γ γ₀) ((FamRG.btgt (A ⇒ D)) (Γr γ₀) y) ))) ->
           (r : ∀ x → DSr γ₀ (f₀ x) ≡ f₁ ((FamRG.bref (A ⇒ D)) γ₀ x)) -> DS₀ γ₀


    data DS₁ (γ₁ : Γ₁) : Set1 where
      ι₁ : D₁ γ₁ -> DS₁ γ₁
      σ₁ : (A : FamRG Γ) -> --  FamRG.𝓡 (A ⇒ DS) γ₁ -> DS₁ γ₁
                          let  FamRG[ A₀ , A₁ , Ar , As , At , Asr , Atr ] = A
                               A⇒DS₀ : Γ₀ -> Set
                               A⇒DS₀ γ₀
                                    = Σ[ f₀ ∈ (A₀ γ₀ -> DS₀ γ₀) ]
                                    Σ[ f₁ ∈ (A₁ (Γr γ₀) -> DS₁ (Γr γ₀)) ]
                                    (∀ y →
                                    DSs (Γr γ₀) (f₁ y)
                                    ≡ subst DS₀ (sym (ul Γ γ₀))
                                    (f₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y) ))) ×
                                    (∀ y →
                                    DSt (Γr γ₀) (f₁ y)
                                    ≡ subst DS₀ (sym (ur Γ γ₀))
                                    (f₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y) ))) ×
                                    (∀ x → DSr γ₀ (f₀ x) ≡ f₁ (Ar γ₀ x))
                           in -- A⇒DS₁ γ₁
                              (
                                  Σ[ f01src ∈ A⇒DS₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                                  Σ[ f01tgt ∈ A⇒DS₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                                  Σ[ R ∈ (A₁ γ₁ -> DS₁ γ₁) ]
                                  (∀ y → DSs γ₁ (R y) ≡ f₀s (As γ₁ y)) ×
                                  (∀ y → DSt γ₁ (R y) ≡ f₀t (At γ₁ y))))
                                  )
                            -> DS₁ γ₁
      δ₁ : (A : FamRG Γ) -> -- FamRG.𝓡 ((A ⇒ D) ⇒ DS) γ₁ -> DS₁ γ₁
                            let  FamRG[ A₀ , A₁ , Ar , As , At , Asr , Atr ] = A
                                 A⇒D₀ : Γ₀ -> Set
                                 A⇒D₀ γ₀
                                  = Σ[ f₀ ∈ (A₀ γ₀ -> D₀ γ₀) ]
                                   Σ[ f₁ ∈ (A₁ (Γr γ₀) -> D₁ (Γr γ₀)) ]
                                   (∀ y →
                                    Ds (Γr γ₀) (f₁ y)
                                    ≡ subst D₀ (sym (ul Γ γ₀))
                                    (f₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y) ))) ×
                                     (∀ y →
                                    Dt (Γr γ₀) (f₁ y)
                                     ≡ subst D₀ (sym (ur Γ γ₀))
                                     (f₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y) ))) ×
                                     (∀ x → Dr γ₀ (f₀ x) ≡ f₁ (Ar γ₀ x))
                                 A⇒D₁ : Γ₁ -> Set
                                 A⇒D₁ γ₁ = Σ[ f01src ∈ A⇒D₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                                  Σ[ f01tgt ∈ A⇒D₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                                  Σ[ R ∈ (A₁ γ₁ -> D₁ γ₁) ]
                                  (∀ y → Ds γ₁ (R y) ≡ f₀s (As γ₁ y)) ×
                                  (∀ y → Dt γ₁ (R y) ≡ f₀t (At γ₁ y))))
                                 A⇒Dr =
                                   (λ  γ₀ f₀₁ → (let (f₀ , f₁ , p , q , _) = f₀₁ in (
                                    ( subst A⇒D₀ (sym (ul Γ γ₀)) f₀₁ ,
                                    subst A⇒D₀ (sym (ur Γ γ₀)) f₀₁ ,
                                    f₁ ,
                                    (λ y → trans
                                      (p y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ul Γ γ₀)))
                                          (subst-natural proj₁ (sym (ul Γ γ₀))))
                                        (As (ref Γ γ₀) y))) ,
                                    (λ y → trans
                                      (q y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ur Γ γ₀)))
                                          (subst-natural proj₁ (sym (ur Γ γ₀))))
                                        (At (ref Γ γ₀) y)))))))
                                 A⇒D⇒DS₀ : Γ₀ -> Set
                                 A⇒D⇒DS₀ γ₀
                                  = Σ[ f₀ ∈ ( A⇒D₀ γ₀ -> DS₀ γ₀) ]
                                  Σ[ f₁ ∈ ( A⇒D₁ (Γr γ₀) -> DS₁ (Γr γ₀)) ]
                                  (∀ y →
                                  DSs (Γr γ₀) (f₁ y)
                                  ≡ subst DS₀ (sym (ul Γ γ₀))
                                  (f₀ (subst  A⇒D₀ (ul Γ γ₀) (proj₁ y) ))) ×
                                   (∀ y →
                                  DSt (Γr γ₀) (f₁ y)
                                  ≡ subst DS₀ (sym (ur Γ γ₀))
                                   (f₀ (subst  A⇒D₀ (ur Γ γ₀) (proj₁ (proj₂ y)) ))) ×
                                   (∀ x → DSr γ₀ (f₀ x) ≡ f₁ ( A⇒Dr γ₀ x))
                            in
                                     (
                                     Σ[ f01src ∈  A⇒D⇒DS₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                                      Σ[ f01tgt ∈  A⇒D⇒DS₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                                      Σ[ R ∈ ( A⇒D₁ γ₁ -> DS₁ γ₁) ]
                                       (∀ y → DSs γ₁ (R y) ≡ f₀s ( (proj₁ y)) ×
                                          (∀ y → DSt γ₁ (R y) ≡ f₀t (proj₁ (proj₂ y))))))
                                      )
                                     -> DS₁ γ₁


    DSr : (γ₀ : Γ₀) -> DS₀ γ₀ -> DS₁ (Γr γ₀)
    DSr γ₀ (ι₀ d) = ι₁ (Dr γ₀ d)
    --DSr γ₀ (σ₀ A@(FamRG[ A₀ , A₁ , Ar , As , At , _ , _ ]) f₀ f₁ p q r) = σ₁ A (--FamRG.bref (A ⇒ DS) γ₀ f
    DSr γ₀ (σ₀ (FamRG[ A₀ , A₁ , Ar , As , At , _ , _ ]) f₀ f₁ p q r) = σ₁ (FamRG[ A₀ , A₁ , Ar , As , At , _ , _ ]) (--FamRG.bref (A ⇒ DS) γ₀ f
                             let f = f₀ , f₁ , p , q , r
                                 A⇒DS₀ : Γ₀ -> Set
                                 A⇒DS₀ γ₀ =
                                   Σ[ f₀ ∈ (A₀ γ₀ -> DS₀ γ₀) ]
                                   Σ[ f₁ ∈ (A₁ (Γr γ₀) -> DS₁ (Γr γ₀)) ]
                                   ((∀ y →
                                         DSs (Γr γ₀) (f₁ y)
                                   ≡ subst DS₀ (sym (ul Γ γ₀))
                                                   (f₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y)))) ×
                                                    (∀ y →
                                                      DSt (Γr γ₀) (f₁ y)
                                   ≡ subst DS₀ (sym (ur Γ γ₀))
                                                   (f₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y)))) ×
                                                    (∀ x → DSr γ₀ (f₀ x) ≡ f₁ (Ar γ₀ x)))
                                 A⇒DS₁ : Γ₁ -> Set
                                 A⇒DS₁ γ₁ = Σ[ f01src ∈ A⇒DS₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                                   Σ[ f01tgt ∈ A⇒DS₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                                   Σ[ R ∈ (A₁ γ₁ -> DS₁ γ₁) ]
                                   (∀ y → DSs γ₁ (R y) ≡ f₀s (As γ₁ y)) ×
                                   (∀ y → DSt γ₁ (R y) ≡ f₀t (At γ₁ y))))
                             in
                               (
                                subst A⇒DS₀ (sym (ul Γ γ₀)) f ,
                                subst A⇒DS₀ (sym (ur Γ γ₀)) f ,
                                f₁ ,
                                (λ y → trans
                                      (p y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ul Γ γ₀)))
                                          (subst-natural proj₁ (sym (ul Γ γ₀))))
                                        (As (ref Γ γ₀) y))) ,
                                (λ y → trans
                                      (q y)
                                      (happly
                                        (trans
                                          (sym
                                           (subst-domcod-simple-sym (ur Γ γ₀)))
                                          (subst-natural proj₁ (sym (ur Γ γ₀))))
                                        (At (ref Γ γ₀) y)))
                                    ))
    DSr γ₀ (δ₀ A f₀ f₁ p q r) = δ₁ A (-- FamRG.bref ((A ⇒ D) ⇒ DS) γ₀ F =
                           let 
                               FamRG[ A₀ , A₁ , Ar , As , At , _ , _ ] = A
                               FamRG[ D₀ , D₁ , Dr , Ds , Dt , _ , _ ] = D
                               lA⇒Dl⇒DS₀ : Γ₀ -> Set
                               lA⇒Dl⇒DS₀ γ₀
                                     = Σ[ f₀ ∈ ((Σ[ h₀ ∈ (A₀ γ₀ -> D₀ γ₀) ]
                                       Σ[ h₁ ∈ (A₁ (Γr γ₀) -> D₁ (Γr γ₀)) ]
                                          (∀ y →
                                           Ds (Γr γ₀) (h₁ y)
                                          ≡ subst D₀ (sym (ul Γ γ₀))
                                           (h₀ (subst A₀ (ul Γ γ₀) (As (Γr γ₀) y) ))) ×
                                            (∀ y →
                                            Dt (Γr γ₀) (h₁ y)
                                             ≡ subst D₀ (sym (ur Γ γ₀))
                                              (h₀ (subst A₀ (ur Γ γ₀) (At (Γr γ₀) y) ))) ×
                                               (∀ x → Dr γ₀ (h₀ x) ≡ h₁ (Ar γ₀ x))) -> DS₀ γ₀) ]
                                        Σ[ f₁ ∈ ((FamRG.𝓡 (A ⇒ D)) (Γr γ₀) -> DS₁ (Γr γ₀)) ]
                                        (∀ y →
                                        DSs (Γr γ₀) (f₁ y)
                                         ≡ subst DS₀ (sym (ul Γ γ₀))
                                                     (f₀ (subst (FamRG.𝒪 (A ⇒ D)) (ul Γ γ₀) ((FamRG.bsrc (A ⇒ D)) (Γr γ₀) y) ))) ×
                                       (∀ y →
                                       DSt (Γr γ₀) (f₁ y)
                                         ≡ subst DS₀ (sym (ur Γ γ₀))
                                                     (f₀ (subst (FamRG.𝒪 (A ⇒ D)) (ur Γ γ₀) ((FamRG.btgt (A ⇒ D)) (Γr γ₀) y) ))) ×
                                       (∀ x → DSr γ₀ (f₀ x) ≡ f₁ ((FamRG.bref (A ⇒ D)) γ₀ x))
                               lA⇒Dl⇒DS₁ : Γ₁ -> Set
                               lA⇒Dl⇒DS₁ γ₁ = Σ[ f01src ∈ lA⇒Dl⇒DS₀ (Γs γ₁) ] (let f₀s = proj₁ f01src in
                                                    Σ[ f01tgt ∈ lA⇒Dl⇒DS₀ (Γt γ₁) ] (let f₀t = proj₁ f01tgt in
                                                    Σ[ R ∈ ((FamRG.𝓡 (A ⇒ D)) γ₁ -> DS₁ γ₁) ]
                                                    (∀ y → DSs γ₁ (R y) ≡ f₀s ((FamRG.bsrc (A ⇒ D)) γ₁ y)) ×
                                                    (∀ y → DSt γ₁ (R y) ≡ f₀t ((FamRG.btgt (A ⇒ D)) γ₁ y))))
                                       in
                                          subst lA⇒Dl⇒DS₀ (sym (ul Γ γ₀)) (f₀ , f₁ , p , q , r) ,
                                          subst lA⇒Dl⇒DS₀ (sym (ur Γ γ₀)) (f₀ , f₁ , p , q , r) ,
                                          f₁ ,
                                          (λ y →
                                            (trans (p y) (happly (trans (sym (subst-domcod-simple-sym (ul Γ γ₀)))
                                                                        (subst-natural proj₁ (sym (ul Γ γ₀))))
                                                                 ((FamRG.bsrc (A ⇒ D)) (ref Γ γ₀) y))) ,
                                            (λ y₁ →
                                              trans (q y₁) ((happly (trans (sym (subst-domcod-simple-sym (ur Γ γ₀)))
                                                                           (subst-natural proj₁ (sym (ur Γ γ₀))))
                                                                    ((FamRG.btgt (A ⇒ D)) (ref Γ γ₀) y₁))))))

    DSs : (γ₁ : Γ₁) -> DS₁ γ₁ -> DS₀ (Γs γ₁)
    DSs γ₁ (ι₁ d) = ι₀ (Ds γ₁ d)
    DSs γ₁ (σ₁ A ((f₀ , f₁ , (p , q , r)) , _ , _ , _)) = σ₀ A f₀ f₁ p q r
    DSs γ₁ (δ₁ A ((F₀ , F₁ , (p , q , r)) , _ , _ , _)) = δ₀ A F₀ F₁ p q r

    DSt : (γ₁ : Γ₁) -> DS₁ γ₁ -> DS₀ (Γt γ₁)
    DSt γ₁ (ι₁ d) = ι₀ (Dt γ₁ d)
    DSt γ₁ (σ₁ A (_ , (f₀ , f₁ , (p , q , r)) , _ , _ , _)) = σ₀ A f₀ f₁ p q r
    DSt γ₁ (δ₁ A (_ , (F₀ , F₁ , (p , q , r)) , _ , _)) =  δ₀ A F₀ F₁ p q r

    DSptul : (γ₀ : Γ₀) -> (x : DS₀ γ₀) ->
             DSs (Γr γ₀) (DSr γ₀ x) ≡ subst DS₀ (sym (ul Γ γ₀)) x
    DSptul γ (ι₀ d) = trans (cong ι₀ (happly (FamRG.ptul D γ) d))
                            (sym (subst-natural ι₀ (sym (ul Γ γ))))
    DSptul γ (σ₀ A f₀ f₁ p q r) = sym (subst-natural (λ { (f₀ , f₁ , (p , q , r)) → σ₀ A f₀ f₁ p q r}) (sym (ul Γ γ)))
    DSptul γ (δ₀ A f₀ f₁ p q r) = sym (subst-natural (λ { (F₀ , F₁ , (p , q , r)) → δ₀ A F₀ F₁ p q r}) (sym (ul Γ γ)))

    DSptur : (γ₀ : Γ₀) -> (x : DS₀ γ₀) ->
             DSt (Γr γ₀) (DSr γ₀ x) ≡ subst DS₀ (sym (ur Γ γ₀)) x
    DSptur γ (ι₀ d) = trans (cong ι₀ (happly (FamRG.ptur D γ) d))
                             (sym (subst-natural ι₀ (sym (ur Γ γ))))
    DSptur γ (σ₀ A f₀ f₁ p q r) = sym (subst-natural (λ { (f₀ , f₁ , (p , q , r)) → σ₀ A f₀ f₁ p q r}) (sym (ur Γ γ)))
    DSptur γ (δ₀ A f₀ f₁ p q r) = sym (subst-natural (λ { (F₀ , F₁ , (p , q , r)) → δ₀ A F₀ F₁ p q r}) (sym (ur Γ γ)))




