module Misc.universes where

open import Prelude.Basic
open import Prelude.Equality
open import Prelude.Fam

-- universe operator

data UO (X : Fam Set) : Set₁ where
  o : UO X
  l : ind X → UO X

TOₒ : {X : Fam Set} → UO X → Set
TOₒ {X = X} o = ind X
TOₒ {X = X} (l x) = ind X

TO₁ : {X : Fam Set} → (c :  UO X) → (y : TOₒ c) → Set
TO₁ {X = X} o x = ind X
TO₁ {X = X} (l x) y = (fib X) y
