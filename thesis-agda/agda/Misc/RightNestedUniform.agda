module Misc.RightNestedUniform where

open import Prelude.Basic
open import Prelude.Fam

open import Uniform.IR as UIR

-- We think of uniform codes and their info as contexts and associated
-- environments
Cxt = Uni
Env = Info

data IR (D E : Set1) (Γ : Cxt D) : Set1 where
  ι : (Env Γ -> E) -> IR D E Γ
  σ : (A : Env Γ -> Set) -> IR D E (UIR.σ Γ A) -> IR D E Γ
  δ : (A : Env Γ -> Set) -> IR D E (UIR.δ Γ A) -> IR D E Γ

decode : {D E : Set1}{Γ : Cxt D} -> Env Γ -> IR D E Γ -> Fam D -> Fam E
decode e (ι α) UT = (⊤ , (λ _ → α e))
decode e (σ A c) UT = ⋃ (A e) (λ a → decode (e , a) c UT)
decode e (δ A c) (U , T) = ⋃ (A e -> U) (λ g → decode (e , T ∘ g) c (U , T))

_[_] : {D E : Set1}{Γ Γ' : Cxt D} ->
       IR D E Γ -> (Env Γ' -> Env Γ) -> IR D E Γ'
(ι e) [ f ] = ι (e ∘ f)
(σ A c) [ f ] = σ (A ∘ f) (c [ map f id ])
(δ A c) [ f ] = δ (A ∘ f) (c [ map f id ])

