{-# OPTIONS --no-positivity-check #-}
{-# OPTIONS --no-termination-check #-}
{-# OPTIONS --universe-polymorphism #-}


{- open import Function -}
open import Function
open import Data.Empty
open import Data.Unit
open import Data.Product
open import Level

module RefCont3 where

∏ : (S : Set)(T : S -> Set1) -> Set1
∏ S T = (s : S) -> T s

{- 1. Fam, its functoriality and monadic combinators and a π-operator -}

Fam : Set₁ -> Set₁
Fam D = Σ Set (λ A -> (A → D))

Fam₁ : {D D' : Set₁} -> (D -> D') -> Fam D -> Fam D'
Fam₁ g (A , f) = (A , g ∘ f)

η : {I : Set₁} -> I -> Fam I
η i = (⊤ , const i)

μ : {D : Set₁} -> Fam (Fam D) → Fam D
μ {D} (A , B) = (Σ A (proj₁ ∘ B) , g)
                where g : Σ A (proj₁ ∘ B) -> D
                      g (a , k) = proj₂ (B a) k

_>>=_ : {X Y : Set₁} -> Fam X → (X → Fam Y) -> Fam Y
P >>= f = μ (Fam₁ f P)

_>>==_ : {D : Set₁} -> {E : D -> Set₁} -> Fam D -> ((d : D) -> Fam (E d)) -> Fam (Σ D E)
P >>== f = P >>= (λ x → Fam₁ (λ p → (x , p)) (f x)) 

πFam : (S : Set){D : S -> Set1} -> (∏ S (Fam ∘ D)) -> Fam (∏ S D)
πFam S T = ((s : S) -> proj₁ (T s)) , \ f s -> proj₂ (T s) (f s)

{- 2. Containers, their semantics and their functorialacton-}
{- Repeat for Large Containers -}
{- Would be nice to define Cont a = Σ (Set a) (λ S → Set a) -}
{- Define two specific large containers: one will give DS codes one will give IR codes-}
{- Note Fam arises as a large contianer ... (Set, El) : LCont and Fam = ⟦(Set , El)⟧L -}

{- 2.1 containers, using universe polymorphism -} 
Cont : ∀ {a} -> Set (suc a)
Cont {a} = Σ (Set a) (λ S -> (S → Set a))

⟦_⟧ : ∀ {a b} → Cont {a} -> Set b → Set  (a ⊔ b)
⟦ S , P ⟧ D = Σ S (λ s -> (P s → D))


⟦_⟧₁ : ∀ {a} -> (H : Cont {a}) → ∀ {b c} -> {X : Set b} -> {Y : Set c} -> 
      (X -> Y) -> ⟦ H ⟧ X → ⟦ H ⟧ Y
⟦ S , P ⟧₁ f (s , g) = (s , f ∘ g)

{- 2.2 The large container LC of containers which 
can be used to define Dybjer-Setzer codes-}

LC : {D : Set₁} -> Cont
LC {D} = (Cont {zero} , λ H → ⟦ H ⟧ D)

LCFam : Set₁ -> Set₁ -> Set₁
LCFam D E = ⟦ LC {D} ⟧ E

LCFam₁ : {I X Y : Set₁} -> (X -> Y) -> LCFam I X -> LCFam I Y 
LCFam₁ f P = ⟦ LC ⟧₁ f P

{- 2.3 A large container which we can define IR codes-}

mutual 
   data IRCont (D : Set₁) : Set₁ where 
        eta : Cont {zero} -> IRCont D
        mu  : (R : IRCont D) ->  (IRPos R -> IRCont D) -> IRCont D
        π   : (A : Set) → (A → IRCont D) → IRCont D 
 
   IRPos : {D : Set₁} -> IRCont D -> Set₁
   IRPos {D} (eta R)  = ⟦ R ⟧ D
   IRPos     (mu R f) = Σ (IRPos R) (IRPos ∘ f)
   IRPos     (π A f) = ∏ A (IRPos ∘ f)

ContIR : {D : Set₁} → Cont
ContIR {D} = (IRCont D , IRPos)


{- 3. IR Codes, their monadicity, a π-operator and their semantics via localisation -}
{- loc P is a container morphism -}
{- loc₁ P is loc P as a nat trans -}

IR : ∀ {a} → Set₁ -> Set a -> Set (a ⊔ suc (zero))
IR D E = ⟦ ContIR {D} ⟧ E 

IR₁ : ∀ {b c} → {C : Set₁} → {D : Set b} → {E : Set c} -> (D -> E) -> IR C D  -> IR C E 
IR₁ f P = ⟦ ContIR ⟧₁ f P

con : {D : Set₁} → Set → IRCont D
con A = eta (A , λ a → ⊥) 

ηIR : ∀ {a} → {D : Set₁} → {E : Set a} -> E -> IR D E 
ηIR e = (con ⊤ , const e)

μIR : ∀ {a} → {D : Set₁} -> {E : Set a} → IR D (IR D E) → IR D E
μIR {a} {D} {E} (R , f) = (Q , α)
                          where Q : IRCont D  
                                Q = mu R (proj₁ ∘ f) 
                                α : IRPos Q → E  
                                α (x , q) = proj₂ (f x) q                

_IRbind_ : ∀ {b c} → {I : Set₁} → {X : Set b} → {Y : Set c} -> IR I X → (X →  IR I Y) -> IR I Y
P IRbind f = μIR (IR₁ f P)

_IRDbind_ : ∀ {a b} → {I : Set₁} -> {D : Set a} → {E : D -> Set b} -> 
            IR I D -> ((d : D) -> IR I (E d)) -> IR I (Σ D E)
P IRDbind f = P IRbind (λ x → IR₁ (λ p → (x , p)) (f x)) 


πIR : {D : Set1} → (S : Set){E : S -> Set₁} -> (∏ S (IR D ∘ E)) -> IR D (∏ S E)
πIR S f = (π S (proj₁ ∘ f) , \ g s -> proj₂ (f s) (g s))

loc : {D : Set₁} -> Fam D -> (X : IRCont D) -> Fam (IRPos X)
loc (U , T) (eta R)   = (⟦ R ⟧ U , ⟦ R ⟧₁ T) 
loc P       (mu R f)  = (loc P R) >>== (λ p → (loc P (f p))) 
loc P       (π A f)   = πFam A (loc P ∘ f)

loc₁ : {D E : Set₁} -> Fam D → IR D E -> Fam E
loc₁ P (X , f) = Fam₁ f (loc P X) 

{- 4. Standard IR using (using Container IR) embedds -}
{- We use IR D is a monad and DS D is the free monad on LCFam D 
to induce an embedding of Dybjer Setzer codes to IR codes-}

data DS (D E : Set₁) : Set₁ where
  ι  : E                → DS D E 
  σδ : LCFam D (DS D E) → DS D E

iter : {D E X : Set₁} -> 
       (E -> X) -> (LCFam D X -> X) -> DS D E -> X
iter α β (ι o)  = α o
iter α β (σδ F) = β (LCFam₁ (iter α β) F)

τ : {D E : Set₁} → LCFam D E → IR D E
τ (Q , f) = (eta Q , f)

σδ' : {D E : Set₁} -> LCFam D (IR D E) -> IR D E
σδ' = μIR ∘ τ

DStoIR : {D E : Set₁} -> DS D E -> IR D E
DStoIR = iter ηIR σδ'

{-5. Finally, the composition operator -}

_/_ : {D E : Set₁}(M : IRCont E) -> IR D E -> IR D (IRPos M)
(eta (S , P)) / F  = (con S , proj₁) IRDbind (λ s -> πIR (P s) (const F))
(mu R f) / F = (R / F) IRDbind (λ p -> (f p) / F) 
(π A f) / F  = πIR A (λ a → f a / F)


_○_ : {C D E : Set1} -> IR D E -> IR C D -> IR C E
(Q , α) ○ G = IR₁ α (Q / G)




